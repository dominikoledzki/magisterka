Czasy:
NARF

initialize: 0.004
depthToWorld: 0.002
calculateTypical3DDistance: 0.238
calculateBorderScore: 0.006
smoothBorderScore: 0.006
firstClassification: 0.005
secondClassification: 0.006
cumulateBorderType: 0.002
calculateLocalSurfaceParameters: 0
calculateLocalSurfaceParameters: 0.185
calculateBorderDirections: 0.004
calculateAverageBorderDirections: 0.004
calculateSurfaceChanges: 0.021
calculateInterestImage: 1.39
calculateInterestPoints: 0.095
sortAndFilterInterestPoints: 0.008

Time of NARF_CUDA: 2.012

initialize: 0.003
depthToWorld: 0.004
calculateTypical3DDistance: 0.288
calculateBorderScore: 0.078
smoothBorderScore: 0.129
firstClassification: 0.014
secondClassification: 0.033
cumulateBorderType: 0.01
calculateLocalSurfaceParameters: 1.714
calculateBorderDirections: 0.004
calculateAverageBorderDirections: 0.008
calculateSurfaceChanges: 0.617
calculateInterestImage: 5.257
calculateInterestPoints: 0.092
sortAndFilterInterestPoints: 0.004

Time of NARF_CPU: 8.291

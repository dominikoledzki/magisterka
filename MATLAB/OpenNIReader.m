classdef OpenNIReader
    %OPENNIREADER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Resolution
        Factors
        Frames
    end
    
    methods
        function obj = OpenNIReader(filename)
            m1 = memmapfile(filename,...
                'Format', {...
                'int32', [2,1], 'resolution';...
                'single', [2,1], 'factors'},...
                'Repeat', 1);
            obj.Resolution = m1.Data.resolution;
            obj.Factors = m1.Data.factors;
            res = double(obj.Resolution');
            m2 = memmapfile(filename,...
                'Offset', 16,...
                'Format', {...
                'uint16', res, 'frame'});
            for i=1:size(m2.Data,1)
                obj.Frames(i,:,:) = m2.Data(i).frame';
            end
        end
        
        function frame = getFrame(obj, index)
            frame = squeeze(obj.Frames(index,:,:));
        end
    end
    
end


reader = OpenNIReader('record.dat');
conv = depthToWorldConverter(reader.Resolution, reader.Factors);
world = cell(size(reader.Frames,1),1);
for n=1:size(reader.Frames,1)
    world{n} = conv.depthToWorldFrame(squeeze(reader.Frames(n,:,:)));
end

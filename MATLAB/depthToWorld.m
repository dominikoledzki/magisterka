function [ worldPoint ] = depthToWorld( resolution, factors, depthPoint )
%DEPTHTOWORLD Summary of this function goes here
%   Detailed explanation goes here

normalizedX = depthPoint(1)/resolution(1) - 0.5
normalizedY = 0.5 - depthPoint(2)/resolution(2)

worldPoint(1,1) = normalizedX * depthPoint(3) * factors(1);
worldPoint(2,1) = normalizedY * depthPoint(3) * factors(2);
worldPoint(3,1) = double(depthPoint(3));

end


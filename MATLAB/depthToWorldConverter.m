classdef depthToWorldConverter
    %DEPTHTOWORLDCONVERTER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Resolution
        Factors
    end
    
    methods
        function obj = depthToWorldConverter(resolution, factors)
            obj.Resolution = double(resolution);
            obj.Factors = double(factors);
        end
        
        function obj = depthToWorldConverterFromReader(reader)
            obj = depthToWorldConverter(double(reader.Resolution), double(reader.Factors));
        end
        
        function [ worldPoint ] = depthToWorld(this, depthPoint )
            %DEPTHTOWORLD Summary of this function goes here
            %   Detailed explanation goes here

            normalizedX = depthPoint(1)/this.Resolution(1) - 0.5;
            normalizedY = 0.5 - depthPoint(2)/this.Resolution(2);

            worldPoint(1,1) = normalizedX * depthPoint(3) * this.Factors(1);
            worldPoint(2,1) = double(depthPoint(3));
            worldPoint(3,1) = normalizedY * depthPoint(3) * this.Factors(2);
        end
        
        function [ worldPoints ] = depthToWorldFrame( this, depthFrame, starty, endy, startx, endx)
            
            if (nargin < 6)
                endy = size(depthFrame,1);
            end
            
            if (nargin < 5)
                endx = size(depthFrame, 2);
            end
            
            if (nargin < 4)
                starty = 1;
            end
            
            if (nargin < 3)
                startx = 1;
            end
            
            subframe = depthFrame(starty:endy, startx:endx);
            worldPoints = zeros(3, sum(subframe (:) > 0));
            
            index = 1;
            for y = starty:endy
                normalizedY = 0.5 - y / this.Resolution(2);
                for x = startx:endx
                    normalizedX = x / this.Resolution(1) - 0.5;
                    depth = depthFrame(y,x);
                    if depth > 0
                        depth = double(depth);
                        pointX = normalizedX * depth * this.Factors(1);
                        pointY = normalizedY * depth * this.Factors(2);
                        worldPoints(:,index) = [pointX; depth; pointY];
                        index = index + 1;
                    end                                           
                end
            end
        end
    end
    
end


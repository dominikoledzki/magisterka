function [image] = loadFile( filename, type )
    if (nargin < 2)
        type = 'single';
    end

    fd = fopen(filename,'r');
    image = zeros(480,640);
    for i=1:480
        image(i,:) = fread(fd,640,type);
    end
    fclose(fd);
    return;
end


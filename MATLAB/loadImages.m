bd = loadFile('imgs/borderDown.dat','uint32');
bu = loadFile('imgs/borderUp.dat','uint32');
bl = loadFile('imgs/borderLeft.dat','uint32');
br = loadFile('imgs/borderRight.dat','uint32');

bsd = loadFile('imgs/borderScoreDown.dat');
bsu = loadFile('imgs/borderScoreUp.dat');
bsl = loadFile('imgs/borderScoreLeft.dat');
bsr = loadFile('imgs/borderScoreRight.dat');

pbd = loadFile('imgs/potentialBorderDown.dat', 'uint32');
pbu = loadFile('imgs/potentialBorderUp.dat', 'uint32');
pbl = loadFile('imgs/potentialBorderLeft.dat', 'uint32');
pbr = loadFile('imgs/potentialBorderRight.dat', 'uint32');

typ = loadFile('imgs/typicalDistance.dat');
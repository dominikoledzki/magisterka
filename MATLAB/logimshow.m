function [] = logimshow( img )
r = valuerange(img);
logrange = log(r);
logimg = log(img);

imshow(logimg,logrange);

end


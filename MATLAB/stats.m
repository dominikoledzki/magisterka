function [ stats ] = stats( M )
%STATS Summary of this function goes here
%   Detailed explanation goes here
M2 = M(:);
stats.min = nanmin(M2);
stats.max = nanmax(M2);
stats.mean = nanmean(M2);
stats.std = nanstd(M2);

end


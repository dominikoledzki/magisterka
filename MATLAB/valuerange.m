function result = valuerange(frame)
%VALUERANGE Summary of this function goes here
%   Detailed explanation goes here
    f = frame(:);
    mask = f > 0;
    f = f(mask);
    result = [min(f), max(f)];
end


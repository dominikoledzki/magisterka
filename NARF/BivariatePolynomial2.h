#pragma once
#include "DataTypes.h"
#include <vector>
#include "Utilities.h"
#include "device_vector.h"

namespace NARF
{

	const int bp2ParamsCount = 6;

	class BivariatePolynomial2	// polynomial of form ax^2 + bxy + cx + dy^2 + ey + f
	{
	public:
		/*static const int parametersCount = 6;*/
		real parameters[bp2ParamsCount];

		CUDA_CALLABLE BivariatePolynomial2(real a = 0.0, real b = 0.0, real c = 0.0, real d = 0.0, real e = 0.0, real f = 0.0)
		{
			parameters[0] = a;
			parameters[1] = b;
			parameters[2] = c;
			parameters[3] = d;
			parameters[4] = e;
			parameters[5] = f;
		}

		CUDA_CALLABLE BivariatePolynomial2(const real *params) {
			memcpy(parameters, params, sizeof(parameters));
			//memcpy_s(&parameters, sizeof(parameters), params, sizeof(parameters));
		}

		CUDA_CALLABLE real valueAt(real x, real y) {
			return
				parameters[0] * x * x +
				parameters[1] * x * y +
				parameters[2] * x +
				parameters[3] * y * y +
				parameters[4] * y +
				parameters[5];

		}

		CUDA_CALLABLE bool isInvalid() const {
			for (int i = 0; i < bp2ParamsCount; ++i) {
				if (isnan(parameters[i]))
					return true;
			}
			return false;
		}

		CUDA_CALLABLE bool criticalPoint(real &x, real &y, int &type) {
			x = (real(2)*parameters[2] * parameters[3] - parameters[1] * parameters[4]) /
				(parameters[1] * parameters[1] - real(4)*parameters[0] * parameters[3]);

			y = (real(-2)*parameters[0] * x - parameters[2]) / parameters[1];

			if (!isfinite(x) || !isfinite(y))
				return false;

			type = 2;
			real det_H = real(4)*parameters[0] * parameters[3] - parameters[1] * parameters[1];

			if (det_H > real(0))  // Check Hessian determinant
			{
				if (parameters[0] + parameters[3] < real(0))  // Check Hessian trace
					type = 0;
				else
					type = 1;
			}

			return true;
		}

		CUDA_CALLABLE static const BivariatePolynomial2 invalid() {
#ifdef __CUDA_ARCH__
			return BivariatePolynomial2(
				CUDART_NAN_F,
				CUDART_NAN_F,
				CUDART_NAN_F,
				CUDART_NAN_F,
				CUDART_NAN_F,
				CUDART_NAN_F
				);
#else
			return BivariatePolynomial2(
				std::numeric_limits<real>::quiet_NaN(),
				std::numeric_limits<real>::quiet_NaN(),
				std::numeric_limits<real>::quiet_NaN(),
				std::numeric_limits<real>::quiet_NaN(),
				std::numeric_limits<real>::quiet_NaN(),
				std::numeric_limits<real>::quiet_NaN()
				);
#endif
		}
	};
}

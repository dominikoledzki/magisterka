#include "BivariatePolynomialApprox.h"

namespace NARF
{

	BivariatePolynomial2 bivariatePolynomialApproximation(const std::vector<Point3r> &samplePoints)
	{
		const int paramsCount = bp2ParamsCount;
		const int degree = 2;

		if (samplePoints.size() < paramsCount)
			return BivariatePolynomial2::invalid();

		Matrix<real, paramsCount, paramsCount> A;
		Matrix<real, paramsCount, 1> b;
		real currentX, currentY, currentZ;
		real tmpX, tmpY;
		real tmpC[paramsCount];

		for (auto it = samplePoints.begin(); it != samplePoints.end(); ++it) {
			currentX = it->x;
			currentY = it->y;
			currentZ = it->z;

			int tmpIdx = paramsCount - 1;

			tmpX = 1.0f;
			for (int xDegree = 0; xDegree <= degree; ++xDegree) {
				tmpY = 1.0f;
				for (int yDegree = 0; yDegree <= degree - xDegree; ++yDegree) {
					tmpC[tmpIdx--] = tmpX * tmpY;
					tmpY *= currentY;
				}
				tmpX *= currentX;
			}

			int aIdx = 0;
			int bIdx = 0;
			int tmpCIdx1 = 0;

			for (int i = 0; i < paramsCount; ++i) {
				b[bIdx++] += currentZ * tmpC[tmpCIdx1];

				int tmpCIdx2 = 0;
				for (int j = 0; j < paramsCount; ++j) {
					A[aIdx++] += tmpC[tmpCIdx1] * tmpC[tmpCIdx2++];

				}

				++tmpCIdx1;
			}

		}

		Matrix<real, paramsCount, 1> parameters = A.inverted() * b;
		if (!parameters.isValid())
			return BivariatePolynomial2::invalid();

		// check result
		real check = (A * parameters - b).vectorNorm();
		if (check > 1e-5)
			return BivariatePolynomial2::invalid();

		return BivariatePolynomial2(parameters.data);
	}

	CUDA_DEVICE BivariatePolynomial2 bivariatePolynomialApproximation(const device_vector<Point3r> &samplePoints)
	{
		const int paramsCount = bp2ParamsCount;
		const int degree = 2;

		if (samplePoints.size() < paramsCount)
			return BivariatePolynomial2::invalid();

		Matrix<real, paramsCount, paramsCount> A;
		Matrix<real, paramsCount, 1> b;
		real currentX, currentY, currentZ;
		real tmpX, tmpY;
		real tmpC[paramsCount];

		for (size_t idx = 0; idx < samplePoints.size(); ++idx) {
			const Point3r &p = samplePoints[idx];
			currentX = p.x;
			currentY = p.y;
			currentZ = p.z;

			int tmpIdx = paramsCount - 1;

			tmpX = 1.0f;
			for (int xDegree = 0; xDegree <= degree; ++xDegree) {
				tmpY = 1.0f;
				for (int yDegree = 0; yDegree <= degree - xDegree; ++yDegree) {
					tmpC[tmpIdx--] = tmpX * tmpY;
					tmpY *= currentY;
				}
				tmpX *= currentX;
			}

			int aIdx = 0;
			int bIdx = 0;
			int tmpCIdx1 = 0;

			for (int i = 0; i < paramsCount; ++i) {
				b[bIdx++] += currentZ * tmpC[tmpCIdx1];

				int tmpCIdx2 = 0;
				for (int j = 0; j < paramsCount; ++j) {
					A[aIdx++] += tmpC[tmpCIdx1] * tmpC[tmpCIdx2++];

				}

				++tmpCIdx1;
			}

		}

		Matrix<real, paramsCount, 1> parameters = A.inverted() * b;
		if (!parameters.isValid())
			return BivariatePolynomial2::invalid();

		// check result
		real check = (A * parameters - b).vectorNorm();
		if (check > 1e-5)
			return BivariatePolynomial2::invalid();

		return BivariatePolynomial2(parameters.data);
	}

}

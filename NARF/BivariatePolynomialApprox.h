#pragma once
#include "BivariatePolynomial2.h"
#include <vector>
#include "device_vector.h"

namespace NARF
{
	BivariatePolynomial2 bivariatePolynomialApproximation(const std::vector<Point3r> &samplePoints);
	CUDA_DEVICE BivariatePolynomial2 bivariatePolynomialApproximation(const device_vector<Point3r> &samplePoints);

}

#include "BorderExtractor.h"
#include <limits>
#include <vector>
#include <algorithm>
#include <map>
#include <queue>
#include "DataTypes.h"
#include "PointAverage.h"

namespace NARF {
	BorderExtractor::BorderExtractor(const Frame &frame, int supportingSize, int pointsUsedForAverage, int smoothingRadius, int reduceBorderSearchingDistance, real borderScoreThreshold) :
		frame(frame),
		supportingSize(supportingSize),
		pointsUsedForAverage(pointsUsedForAverage),
		smoothingRadius(smoothingRadius),
		reduceBorderSearchingDistance(reduceBorderSearchingDistance),
		borderScoreThreshold(borderScoreThreshold)
	{
		width = frame.getWidth();
		height = frame.getHeight();
	}

	BorderExtractor::~BorderExtractor()
	{
		if (distance3D)
			delete[] distance3D;

		for (int i = 0; i < 4; ++i) {
			if (borderScore[i])
				delete[] borderScore[i];
		}

		if (borderTypeWithDirection)
			delete[] borderTypeWithDirection;
	}

	void BorderExtractor::calculateTypical3DDistance()
	{
		LOG_FUNCTION_CALL

		real nan = numeric_limits<real>::quiet_NaN();
		int M = (supportingSize + 1)*(supportingSize + 1) / 4;

		distance3D = new real[height * width];

		int s = supportingSize;

		for (int y = 0; y < s / 2; ++y) {
			for (int x = 0; x < width; ++x) {
				distance3D[y*width + x] = nan;
			}
		}

		for (int y = height - s + s / 2; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				distance3D[y*width + x] = nan;
			}
		}

		for (int y = s / 2; y < height - s + s / 2; ++y) {
			for (int x = 0; x < s / 2; ++x) {
				distance3D[y*width + x] = nan;
			}
			for (int x = width - s + s / 2; x < width; ++x) {
				distance3D[y*width + x] = nan;
			}
		}

		real *distances = new real[s*s];
		int distances_size = 0;

		for (int y = s / 2; y < height - s + s / 2; ++y) {
			for (int x = s / 2; x < width - s + s / 2; ++x) {

				bool invalidValue = false;

				Point3r p = frame.pointAt(x, y);
				if (isValid(p)) {
					for (int j = -s / 2; j < s - s / 2; ++j) {
						for (int i = -s / 2; i < s - s / 2; ++i) {
							int py = y + j;
							int px = x + i;

							Point3r n = frame.pointAt(px, py);
							if (isValid(n)) {
								real d = sumOfSquares(n - p);
								distances[distances_size++] = d;
							}
							else {
								invalidValue = true;
							}
						}

						if (invalidValue) {
							break;
						}
					}
				}
				else {
					invalidValue = true;
				}

				if (!invalidValue) {
					
					// small sort
					real minVal, val;
					int minIdx, i1, i2;
					for (i1 = 0; i1 < M; ++i1) {
						minVal = distances[i1];
						minIdx = i1;

						for (i2 = i1 + 1; i2 < s*s; ++i2) {
							val = distances[i2];
							if (val < minVal) {
								minVal = val;
								minIdx = i2;
							}
						}
						distances[minIdx] = distances[i1];
						distances[i1] = minVal;						
					}

					real dist = std::sqrt(distances[M-1]);
					distance3D[y*width + x] = dist;
				}
				else {
					distance3D[y*width + x] = nan;
				}

				distances_size = 0;
			}
		}

		delete[] distances;
	}

	void BorderExtractor::calculateBorderScore() {
		LOG_FUNCTION_CALL

		real nan = numeric_limits<real>::quiet_NaN();
		int mp = pointsUsedForAverage;

		std::vector<Direction> directions = { Direction::Up, Direction::Right, Direction::Down, Direction::Left };
		std::map<Direction, std::pair<int, int> > directionPoints = {
				{ Direction::Up,    std::make_pair( 0,  1) },
				{ Direction::Right, std::make_pair( 1,  0) },
				{ Direction::Down,  std::make_pair( 0, -1) },
				{ Direction::Left,  std::make_pair(-1,  0) }
		};

		for (auto it = directions.begin(); it != directions.end(); ++it) {
			Direction dir = *it;
			int dirIdx = static_cast<int>(dir);

			int xOffset = directionPoints[dir].first;
			int yOffset = directionPoints[dir].second;

			borderScore[dirIdx] = new real[width * height];
			real *dirBorderScore = borderScore[dirIdx];

			avgNeighborRange[dirIdx] = new real[width * height];
			real *dirAvgNeighborRange = avgNeighborRange[dirIdx];

			//for each point
			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; ++x) {

					if (x + mp * xOffset < 0 ||
						x + mp * xOffset >= width ||
						y + mp * yOffset < 0 ||
						y + mp * yOffset >= height) {
						dirBorderScore[y*width + x] = nan;
						continue;
					}

					Point3r p = frame.pointAt(x, y);
					Point3r avgNeighborPosition;
					for (int i = 1; i <= mp; ++i) {
						Point3r neighbour = frame.pointAt(x + i * xOffset, y + i * yOffset);
						avgNeighborPosition = avgNeighborPosition + neighbour;
					}
					avgNeighborPosition = avgNeighborPosition * (1 / (real)mp);
					real avgNeighborRange = avgNeighborPosition.z;
					dirAvgNeighborRange[y * width + x] = avgNeighborRange;
					
					real avgDistance = std::sqrt(sumOfSquares(p - avgNeighborPosition));
					real typical3DDistance = distance3D[y * width + x];
					real score = std::max(0.0f, 1 - typical3DDistance / avgDistance);
					dirBorderScore[y * width + x] = score;
				}
			}
		}

	}

	void BorderExtractor::smoothBorderScore() {
		LOG_FUNCTION_CALL

		real nan = numeric_limits<real>::quiet_NaN();
		int radius = smoothingRadius;

		for (int dir = 0; dir < 4; ++dir) {
			real *tempBorderScore = new real[width * height];
			real *dirBorderScore = borderScore[dir];
			memcpy(tempBorderScore, dirBorderScore, sizeof(real) * width * height);

			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; ++x) {

					real sum = 0.0f;
					int pointsCount = 0;

					for (int yOffset = -radius; yOffset <= radius; ++yOffset) {
						for (int xOffset = -radius; xOffset <= radius; ++xOffset) {

							int py = y + yOffset;
							int px = x + xOffset;

							if (px < 0 || px >= width || py < 0 || py >= height)
								continue;

							real pval = tempBorderScore[py * width + px];
							if (isnan(pval))
								continue;

							sum += pval;
							++pointsCount;
						}
					}

					if (pointsCount > 0) {
						real avg = sum / pointsCount;
						dirBorderScore[y * width + x] = avg;
					}
				}
			}

			delete[] tempBorderScore;			
		}
	}

	void BorderExtractor::firstClassification()	{
		LOG_FUNCTION_CALL
		for (int dir = 0; dir < 4; ++dir) {
			potentialBorderType[dir] = new PotentialBorderType[width * height];
			PotentialBorderType *dirBorderType = potentialBorderType[dir];

			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; ++x) {
					real pRange = frame.depthAt(x, y);
					real pNeighRange = avgNeighborRange[dir][y * width + x];

					if (isnan(pRange + pNeighRange))
						dirBorderType[y * width + x] = PotentialBorderType::Unknown;
					else
						dirBorderType[y * width + x] = pRange < pNeighRange ?
						PotentialBorderType::ObstacleBorder :
						PotentialBorderType::ShadowBorder;
				}
			}
		}
	}

	void BorderExtractor::secondClassification() {
		LOG_FUNCTION_CALL
		real nan = numeric_limits<real>::quiet_NaN();
		int searchingDistance = reduceBorderSearchingDistance;

		std::vector<Direction> directions = { Direction::Up, Direction::Right, Direction::Down, Direction::Left };
		std::map<Direction, std::pair<int, int> > directionPoints = {
				{ Direction::Up, std::make_pair(0, 1) },
				{ Direction::Right, std::make_pair(1, 0) },
				{ Direction::Down, std::make_pair(0, -1) },
				{ Direction::Left, std::make_pair(-1, 0) }
		};

		for (auto it = directions.begin(); it != directions.end(); ++it) {
			Direction dir = *it;
			int dirIdx = static_cast<int>(dir);

			int xOffset = directionPoints[dir].first;
			int yOffset = directionPoints[dir].second;

			PotentialBorderType *dirPotentialBorderType = potentialBorderType[dirIdx];
			real *dirBorderScore = borderScore[dirIdx];

			real *tempBorderScore = new real[width * height];
			memcpy(tempBorderScore, dirBorderScore, sizeof(real) * width * height);

			borderType[dirIdx] = new BorderType[width * height];
			BorderType *dirBorderType = borderType[dirIdx];

			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; ++x) {
					dirBorderType[y * width + x] = BorderType::NoBorder;

					PotentialBorderType type = dirPotentialBorderType[y * width + x];
					if (type != PotentialBorderType::ObstacleBorder)
						continue;

					real thisScore = tempBorderScore[y * width + x];

					// looking for shadow border on the direction
					real maxScore = nan;
					int maxIndex = 0;
					for (int i = 1; i <= searchingDistance; ++i) {
						int py = y + i * yOffset;
						int px = x + i * xOffset;

						if (py < 0 || py >= height || px < 0 || px >= width)
							continue;

						real pScore = tempBorderScore[py * width + px];
						if (isnan(maxScore) || (!isnan(pScore) && pScore > maxScore)) {
							maxScore = pScore;
							maxIndex = i;
						}
					}

					if (!isnan(maxScore)) {
						real oneMinusSSCubed = (1 - maxScore)*(1 - maxScore)*(1 - maxScore);
						real newScore = max((real)0.9, 1 - oneMinusSSCubed) * thisScore;
						dirBorderScore[y * width + x] = newScore;

						if (newScore > borderScoreThreshold) {
							bool isMax = true;

							int x1 = x - xOffset;
							int y1 = y - yOffset;
							int x2 = x + xOffset;
							int y2 = y + yOffset;

							if (x1 >= 0 && x1 < width && y1 >= 0 && y1 < height && tempBorderScore[y1 * width + x1] > newScore)
								isMax = false;

							if (x2 >= 0 && x2 < width && y2 >= 0 && y2 < height && tempBorderScore[y2 * width + x2] > newScore)
								isMax = false;
							
							if (isMax) {
								dirBorderType[y * width + x] = BorderType::ObstacleBorder;
								dirBorderType[(y + maxIndex * yOffset) * width + x + maxIndex * xOffset] = BorderType::ShadowBorder;

								for (int i = 1; i < maxIndex; ++i) {
									dirBorderType[(y + i * yOffset) * width + x + i * xOffset] = BorderType::VeilPoint;
								}
							}
						}
					}
				}
			}

			delete[] tempBorderScore;
		}

		// cumulate results
		borderTypeWithDirection = new unsigned int[width * height];

		BorderType *upBorderType	= borderType[(int)Direction::Up];
		BorderType *rightBorderType	= borderType[(int)Direction::Right];
		BorderType *downBorderType	= borderType[(int)Direction::Down];
		BorderType *leftBorderType	= borderType[(int)Direction::Left];

		for (int idx = 0; idx < width * height; ++idx) {
			unsigned int &pixel = borderTypeWithDirection[idx];

			pixel = (unsigned int)BorderTypeWithDirection::NoBorder;

			if (upBorderType[idx] == BorderType::ObstacleBorder)
				pixel |= (unsigned int)BorderTypeWithDirection::ObstacleBorderUp;

			if (rightBorderType[idx] == BorderType::ObstacleBorder)
				pixel |= (unsigned int)BorderTypeWithDirection::ObstacleBorderRight;

			if (downBorderType[idx] == BorderType::ObstacleBorder)
				pixel |= (unsigned int)BorderTypeWithDirection::ObstacleBorderDown;

			if (leftBorderType[idx] == BorderType::ObstacleBorder)
				pixel |= (unsigned int)BorderTypeWithDirection::ObstacleBorderLeft;


			if (upBorderType[idx] == BorderType::ShadowBorder)
				pixel |= (unsigned int)BorderTypeWithDirection::ShadowBorderUp;

			if (rightBorderType[idx] == BorderType::ShadowBorder)
				pixel |= (unsigned int)BorderTypeWithDirection::ShadowBorderRight;

			if (downBorderType[idx] == BorderType::ShadowBorder)
				pixel |= (unsigned int)BorderTypeWithDirection::ShadowBorderDown;

			if (leftBorderType[idx] == BorderType::ShadowBorder)
				pixel |= (unsigned int)BorderTypeWithDirection::ShadowBorderLeft;


			if (upBorderType[idx] == BorderType::VeilPoint)
				pixel |= (unsigned int)BorderTypeWithDirection::VeilPointUp;

			if (rightBorderType[idx] == BorderType::VeilPoint)
				pixel |= (unsigned int)BorderTypeWithDirection::VeilPointRight;

			if (downBorderType[idx] == BorderType::VeilPoint)
				pixel |= (unsigned int)BorderTypeWithDirection::VeilPointDown;

			if (leftBorderType[idx] == BorderType::VeilPoint)
				pixel |= (unsigned int)BorderTypeWithDirection::VeilPointLeft;
		}
	}

	void BorderExtractor::calculateLocalSurfaceParameters()
	{
		LOG_FUNCTION_CALL
		localSurfaceParameters = new LocalSurface*[width * height];

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int index = y * width + x;
				localSurfaceParameters[index] = nullptr;

				if (frame.depthAt(x, y)) {
					LocalSurface *localSurface = new LocalSurface();

					if (!calculateLocalSurfaceParametersAtPoint(x, y, 2, 6, *localSurface)) {
						delete localSurface;
						localSurface = nullptr;
						
					} else {
						localSurfaceParameters[index] = localSurface;
					}
				}

				
				
			}
		}
	}

	bool BorderExtractor::calculateLocalSurfaceParametersAtPoint(int x, int y, int radius, int noOfClosestNeighbors, LocalSurface &localSurface)
	{
		
		localSurface.normal = Vector3r();
		localSurface.neighborhoodMean = Vector3r();
		localSurface.eigenValues = Vector3r();
		localSurface.normalNoJumps = Vector3r();
		localSurface.nighborhoodMeanNoJumps = Vector3r();
		localSurface.eigenValuesNoJumps = Vector3r();
		localSurface.maxNeighborDistanceSquared = 0.0f;

		Point3r point = frame.pointAt(x, y);

		int blockSize = (2 * radius + 1) * (2 * radius + 1);

		int neighborCount = 0;
		NeighborWithDistance *neighbors = new NeighborWithDistance[blockSize];

		for (int y2 = y - radius; y2 <= y + radius; ++y2) {
			for (int x2 = x - radius; x2 <= x + radius; ++x2) {
				if (x2 < 0 || y2 < 0 || x2 >= width || y2 >= height || frame.depthAt(x2, y2) == 0)
					continue;

				NeighborWithDistance &neighbor = neighbors[neighborCount];
				neighbor.point = frame.pointAt(x2, y2);
				neighbor.distance = squaredDistance(point, neighbor.point);
				++neighborCount;
			}
		}

		noOfClosestNeighbors = std::min(neighborCount, noOfClosestNeighbors);
		std::sort(neighbors, neighbors + neighborCount);

		localSurface.maxNeighborDistanceSquared = neighbors[noOfClosestNeighbors - 1].distance;

		real maxDistanceSquared = 4.0f * localSurface.maxNeighborDistanceSquared;

		PointAverage pointAverage;

		int neighborIdx;
		for (neighborIdx = 0; neighborIdx < neighborCount; ++neighborIdx) {
			if (neighbors[neighborIdx].distance > maxDistanceSquared)
				break;

			pointAverage.addSample(neighbors[neighborIdx].point);
		}

		if (pointAverage.getNoOfSamples() < 3)
			return false;

		Vector3r eigenVector2, eigenVector3;
		pointAverage.pca(localSurface.eigenValues, localSurface.normal, eigenVector2, eigenVector3);

		Vector3r viewingDirection(-point.x, -point.y, -point.z);
		if (localSurface.normal.dotProduct(viewingDirection) < 0.0f)
			localSurface.normal = localSurface.normal * (-1.0f);

		localSurface.neighborhoodMean = pointAverage.getMean();

		// add left neightbors
		for (; neighborIdx < neighborCount; ++neighborIdx) {
			pointAverage.addSample(neighbors[neighborIdx].point);
		}

		pointAverage.pca(localSurface.eigenValuesNoJumps, localSurface.normalNoJumps, eigenVector2, eigenVector3);
		if (localSurface.normalNoJumps.dotProduct(viewingDirection) < 0.0f)
			localSurface.normalNoJumps = localSurface.normalNoJumps * (-1.0f);
		localSurface.nighborhoodMeanNoJumps = pointAverage.getMean();

		delete[] neighbors;

		return true;
	}

	const Frame &BorderExtractor::getFrame() const {
		return frame;
	}

	BorderExtractor::BorderType BorderExtractor::getBorderType(int x, int y, NARF::Direction dir) const {
		int index = y * width + x;
		return borderType[static_cast<int>(dir)][index];
	}
}
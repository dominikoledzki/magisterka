#pragma once
#include "Frame.h"

namespace NARF {

	struct LocalSurface
	{
		bool valid;
		Vector3r normal;
		Vector3r neighborhoodMean;
		Matrix<real, 3, 1> eigenValues;
		Vector3r normalNoJumps;
		Vector3r nighborhoodMeanNoJumps;
		Matrix<real, 3, 1> eigenValuesNoJumps;
		real maxNeighborDistanceSquared;

		bool operator==(const LocalSurface &other)
		{
			return
				valid == other.valid &&
				normal == other.normal &&
				neighborhoodMean == other.neighborhoodMean &&
				eigenValues == other.eigenValues &&
				normalNoJumps == other.normalNoJumps &&
				nighborhoodMeanNoJumps == other.nighborhoodMeanNoJumps &&
				eigenValuesNoJumps == other.eigenValuesNoJumps &&
				maxNeighborDistanceSquared == other.maxNeighborDistanceSquared;
		}

		real maxRelativeDiff(const LocalSurface &other) 
		{
			real maxNeighborDistanceSquaredDiff = maxNeighborDistanceSquared - other.maxNeighborDistanceSquared;
			real maxNeighborDistanceSquaredDiffValue = abs(maxNeighborDistanceSquaredDiff / maxNeighborDistanceSquared);

			Vector3r normalDiff = normal - other.normal;
			real normalDiffValue = normalDiff.norm() / normal.norm();

			Vector3r neighborhoodMeanDiff = neighborhoodMean - other.neighborhoodMean;
			real neighborhoodMeanDiffValue = neighborhoodMeanDiff.norm() / neighborhoodMean.norm();

			Vector3r normalNoJumpsDiff = normalNoJumps - other.normalNoJumps;
			real normalNoJumpsDiffValue = normalNoJumpsDiff.norm() / normalNoJumps.norm();

			Vector3r nighborhoodMeanNoJumpsDiff = nighborhoodMeanNoJumps - other.nighborhoodMeanNoJumps;
			real nighborhoodMeanNoJumpsDiffValue = nighborhoodMeanNoJumpsDiff.norm() / nighborhoodMeanNoJumps.norm();

			Vector3r eigenValuesDiff = eigenValues - other.eigenValues;
			real eigenValuesDiffValue = eigenValuesDiff.norm() / Vector3r(eigenValues).norm();

			Vector3r eigenValuesNoJumpsDiff = eigenValuesNoJumps - other.eigenValuesNoJumps;
			real eigenValuesNoJumpsDiffValue = eigenValuesNoJumpsDiff.norm() / Vector3r(eigenValuesNoJumps).norm();

			return max({ 
				maxNeighborDistanceSquaredDiffValue,
				normalDiffValue,
				neighborhoodMeanDiffValue,
				normalNoJumpsDiffValue,
				nighborhoodMeanNoJumpsDiffValue,
				eigenValuesDiffValue,
				eigenValuesNoJumpsDiffValue
			});
		}
	};

	class BorderExtractor
	{
	public:
		enum class PotentialBorderType : unsigned char
		{
			Unknown,
			ObstacleBorder,
			ShadowBorder
		};

		enum class BorderType : unsigned int
		{
			NoBorder,
			ObstacleBorder,
			ShadowBorder,
			VeilPoint
		};

		enum class BorderTypeWithDirection : unsigned int
		{
			NoBorder = 0,

			ObstacleBorderUp = 1 << 0,
			ObstacleBorderRight = 1 << 2,
			ObstacleBorderDown = 1 << 3,
			ObstacleBorderLeft = 1 << 4,
			ObstacleBorder = ObstacleBorderUp | ObstacleBorderRight | ObstacleBorderDown | ObstacleBorderLeft,

			ShadowBorderUp = 1 << 5,
			ShadowBorderRight = 1 << 6,
			ShadowBorderDown = 1 << 7,
			ShadowBorderLeft = 1 << 8,
			ShadowBorder = ShadowBorderUp | ShadowBorderRight | ShadowBorderDown | ShadowBorderLeft,

			VeilPointUp = 1 << 9,
			VeilPointRight = 1 << 10,
			VeilPointDown = 1 << 11,
			VeilPointLeft = 1 << 12,
			VeilPoint = VeilPointUp | VeilPointRight | VeilPointDown | VeilPointLeft,

			VeilPointOrShadowBorder = ShadowBorder | VeilPoint,
		};

		BorderExtractor(const Frame &frame, int supportingSize = 5, int pointsUsedForAverage = 3, int smoothingRadius = 1, int reduceBorderSearchingDistance = 3, real borderScoreThreshold = 0.8);
		~BorderExtractor();

		void calculateTypical3DDistance();
		void calculateBorderScore();
		void smoothBorderScore();
		void firstClassification();
		void secondClassification();

		void calculateLocalSurfaceParameters();
		bool calculateLocalSurfaceParametersAtPoint(int x, int y, int radius, int noOfClosestNeighbors, LocalSurface &localSurface);

		const Frame &getFrame() const;
		BorderType getBorderType(int x, int y, Direction dir) const;

		

	public:
		int width;
		int height;

		const Frame &frame;
		int supportingSize;
		int pointsUsedForAverage;
		int smoothingRadius;
		int reduceBorderSearchingDistance;
		real borderScoreThreshold;

		real *distance3D;	
		real *avgNeighborRange[4];
		real *borderScore[4];
		PotentialBorderType *potentialBorderType[4];
		BorderType *borderType[4];
		unsigned int *borderTypeWithDirection;

		LocalSurface **localSurfaceParameters;
	};

}
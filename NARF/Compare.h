#pragma once
#include <cassert>
#include <cmath>

namespace NARF {
	template<typename T>
	void compare(T *array1, T *array2, int width, int height, double tolerance = 0.0)
	{
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int index = y * width + x;

				T val1 = array1[index];
				T val2 = array2[index];

				assert(isnan(val1) == isnan(val2));
				if (!isnan(val1)) {
					assert(abs(val1 - val2) <= tolerance);
				}
			}
		}
	}

	template<>
	void compare<Vector3r>(Vector3r *array1, Vector3r *array2, int width, int height, double tolerance)
	{
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int index = y * width + x;

				const Vector3r &val1 = array1[index];
				const Vector3r &val2 = array2[index];

				assert(isValid(val1) == isValid(val2));
				if (isValid(val1)) {
					Vector3r diff = val1 - val2;
					assert(abs(diff.x) <= tolerance);
					assert(abs(diff.y) <= tolerance);
					assert(abs(diff.z) <= tolerance);
				}
			}
		}
	}

	template<typename T>
	void compareExact(T *array1, T *array2, int width, int height)
	{
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int index = y * width + x;

				T val1 = array1[index];
				T val2 = array2[index];

				assert(val1 == val2);
			}
		}
	}

	void compareLocalSurface(LocalSurface *array1, LocalSurface *array2, int width, int height, NARF::real tolerance = 0.0)
	{
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int index = y * width + x;

				LocalSurface val1 = array1[index];
				LocalSurface val2 = array2[index];

				assert(val1.valid == val2.valid);

				if (val1.valid) {
					real maxDiff1 = val1.maxRelativeDiff(val2);
					real maxDiff2 = val2.maxRelativeDiff(val1);

					assert(maxDiff1 < tolerance);
					assert(maxDiff2 < tolerance);
				}
			}
		}
	}

}



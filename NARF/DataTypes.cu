#include "DataTypes.h"
#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"


namespace NARF
{
	template<>
	template<>
	CUDA_CALLABLE Matrix33r Matrix33r::operator*(const Matrix33r &other) {
		Matrix33r result;

		result[0 * 3 + 0] =
			data[0 * 3 + 0] * other.data[0 * 3 + 0] +
			data[0 * 3 + 1] * other.data[1 * 3 + 0] +
			data[0 * 3 + 2] * other.data[2 * 3 + 0];
		result[0 * 3 + 1] =
			data[0 * 3 + 0] * other.data[0 * 3 + 1] +
			data[0 * 3 + 1] * other.data[1 * 3 + 1] +
			data[0 * 3 + 2] * other.data[2 * 3 + 1];
		result[0 * 3 + 2] =
			data[0 * 3 + 0] * other.data[0 * 3 + 2] +
			data[0 * 3 + 1] * other.data[1 * 3 + 2] +
			data[0 * 3 + 2] * other.data[2 * 3 + 2];

		result[1 * 3 + 0] =
			data[1 * 3 + 0] * other.data[0 * 3 + 0] +
			data[1 * 3 + 1] * other.data[1 * 3 + 0] +
			data[1 * 3 + 2] * other.data[2 * 3 + 0];
		result[1 * 3 + 1] =
			data[1 * 3 + 0] * other.data[0 * 3 + 1] +
			data[1 * 3 + 1] * other.data[1 * 3 + 1] +
			data[1 * 3 + 2] * other.data[2 * 3 + 1];
		result[1 * 3 + 2] =
			data[1 * 3 + 0] * other.data[0 * 3 + 2] +
			data[1 * 3 + 1] * other.data[1 * 3 + 2] +
			data[1 * 3 + 2] * other.data[2 * 3 + 2];

		result[2 * 3 + 0] =
			data[2 * 3 + 0] * other.data[0 * 3 + 0] +
			data[2 * 3 + 1] * other.data[1 * 3 + 0] +
			data[2 * 3 + 2] * other.data[2 * 3 + 0];
		result[2 * 3 + 1] =
			data[2 * 3 + 0] * other.data[0 * 3 + 1] +
			data[2 * 3 + 1] * other.data[1 * 3 + 1] +
			data[2 * 3 + 2] * other.data[2 * 3 + 1];
		result[2 * 3 + 2] =
			data[2 * 3 + 0] * other.data[0 * 3 + 2] +
			data[2 * 3 + 1] * other.data[1 * 3 + 2] +
			data[2 * 3 + 2] * other.data[2 * 3 + 2];


		return result;
	}


	CUDA_CALLABLE Matrix44r getTransformFromUnitVectorsZY(const Vector3r &zAxis, const Vector3r &yDirection) {
		Vector3r tmp0 = yDirection.crossProduct(zAxis).normalized();
		Vector3r tmp1 = zAxis.crossProduct(tmp0).normalized();
		Vector3r tmp2 = zAxis.normalized();

		Matrix44r transform;
		transform(0, 0) = tmp0.x;	transform(0, 1) = tmp0.y;	transform(0, 2) = tmp0.z;	transform(0, 3) = 0.0f;
		transform(1, 0) = tmp1.x;	transform(1, 1) = tmp1.y;	transform(1, 2) = tmp1.z;	transform(1, 3) = 0.0f;
		transform(2, 0) = tmp2.x;	transform(2, 1) = tmp2.y;	transform(2, 2) = tmp2.z;	transform(2, 3) = 0.0f;
		transform(3, 0) = 0.0f;		transform(3, 1) = 0.0f;		transform(3, 2) = 0.0f;		transform(3, 3) = 1.0f;

		return transform;
	}

	CUDA_CALLABLE Matrix44r getTransformFromTwoUnitVectors(const Vector3r &yDirection, const Vector3r &zAxis) {
		return getTransformFromUnitVectorsZY(zAxis, yDirection);
	}


	template<>
	CUDA_CALLABLE Matrix<real, 4, 1> matMul<real, 4, 4, 1>(const Matrix<real, 4, 4> &mat1, const Matrix<real, 4, 1> &mat2) {
		const real *m1 = mat1.data;
		const real *m2 = mat2.data;

		real data[4] = {
			m1[0 + 0] * m2[0] + m1[0 + 1] * m2[1] + m1[0 + 2] * m2[2] + m1[0 + 3] * m2[3],
			m1[4 + 0] * m2[0] + m1[4 + 1] * m2[1] + m1[4 + 2] * m2[2] + m1[4 + 3] * m2[3],
			m1[8 + 0] * m2[0] + m1[8 + 1] * m2[1] + m1[8 + 2] * m2[2] + m1[8 + 3] * m2[3],
			m1[12 + 0] * m2[0] + m1[12 + 1] * m2[1] + m1[12 + 2] * m2[2] + m1[12 + 3] * m2[3]
		};

		Matrix < real, 4, 1 > mat;
		memcpy(mat.data, data, 4 * sizeof(real));

		return mat;
	}

	CUDA_CALLABLE bool operator<(const InterestPoint &l, const InterestPoint &r) {
		return InterestPoint::isBetter(l, r);
	}

	CUDA_CALLABLE bool InterestPoint::isBetter(const InterestPoint &ip1, const InterestPoint &ip2)
	{
		return ip1.interestValue > ip2.interestValue;
	}
}

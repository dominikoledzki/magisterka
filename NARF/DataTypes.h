#pragma once
#include <limits>
#include <cmath>
#include <cstdarg>
#include <initializer_list>
#include <type_traits>
#include <opencv2\opencv.hpp>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_cblas.h>

#include "Utilities.h"
#include <math_constants.h>
#include "matinv.h"

namespace NARF
{

	typedef float real;
	typedef unsigned short depth_t;

	template<typename T, int rows, int cols>
	class Matrix;

	template<typename T>
	struct Point3
	{
		typedef Point3<T> ThisClass;

		CUDA_CALLABLE Point3(const cv::Vec<T, 3> &vec) : x(vec[0]), y(vec[1]), z(vec[2]) {}
		CUDA_CALLABLE Point3(T _x = 0, T _y = 0, T _z = 0) : x(_x), y(_y), z(_z) {}
		CUDA_CALLABLE Point3(const Matrix<T, 3, 1> &matrix);

		T x;
		T y;
		T z;

		CUDA_CALLABLE static ThisClass invalidPoint()
		{
		#ifdef __CUDA_ARCH__
			return Point3<T>(CUDART_NAN_F, CUDART_NAN_F, CUDART_NAN_F);
		#else
			return Point3<T>(std::numeric_limits<T>::quiet_NaN(), std::numeric_limits<T>::quiet_NaN(), std::numeric_limits<T>::quiet_NaN());
		#endif
		}

		CUDA_CALLABLE inline T squaredNorm() const
		{
			return x*x + y*y + z*z;
		}

		CUDA_CALLABLE inline T norm() const
		{
			return sqrt(squaredNorm());
		}

		CUDA_CALLABLE inline void normalize()
		{
			T anorm = norm();
			x /= anorm;
			y /= anorm;
			z /= anorm;
		}

		CUDA_CALLABLE inline ThisClass normalized() const
		{
			T anorm = norm();
			return ThisClass(x / anorm, y / anorm, z / anorm);
		}

		CUDA_CALLABLE T dotProduct(const ThisClass &other) const {
			return x * other.x + y * other.y + z * other.z;
		}

		CUDA_CALLABLE ThisClass crossProduct(const ThisClass &v) const {
			const ThisClass &u = *this;
			// http://mathworld.wolfram.com/CrossProduct.html

			ThisClass result;
			result.x = u.y * v.z - u.z * v.y;
			result.y = u.z * v.x - u.x * v.z;
			result.z = u.x * v.y - u.y * v.x;

			return result;
		}

		CUDA_CALLABLE bool isZero()
		{
			return x == 0 && y == 0 && z == 0;
		}

		cv::Vec<T, 3> toCvVec()
		{
			return cv::Vec<T, 3>(x, y, z);
		}

		CUDA_CALLABLE ThisClass operator-() const {
			return ThisClass(-x, -y, -z);
		}

		CUDA_CALLABLE ThisClass &operator+=(const ThisClass &other) {
			x += other.x;
			y += other.y;
			z += other.z;

			return *this;
		}

		CUDA_CALLABLE ThisClass &operator-=(const ThisClass &other) {
			x -= other.x;
			y -= other.y;
			z -= other.z;

			return *this;
		}

		template<typename V>
		CUDA_CALLABLE ThisClass &operator*=(V factor) {
			x *= factor;
			y *= factor;
			z *= factor;
			return *this;
		}

		template<typename V>
		CUDA_CALLABLE ThisClass &operator/=(V factor) {
			x /= factor;
			y /= factor;
			z /= factor;
			return *this;
		}

		string toString() {
			stringstream ss;
			ss << "(" << x << "; " << y << "; " << z << ")";
			return ss.str();
		}
	};

	template<typename T>
	struct Rotation3
	{
		T alpha;
		T beta;
		T gamma;
	};

	template<typename T>
	struct Vector2
	{
		CUDA_CALLABLE Vector2(T x = 0.0, T y = 0.0) : x(x), y(y) {}

		T x;
		T y;

		CUDA_CALLABLE void normalize() {
			T aNorm = norm();
			x /= aNorm;
			y /= aNorm;
		}

		CUDA_CALLABLE inline T norm() const {
			return sqrt(x*x + y*y);
		}
	};

	template<typename T>
	CUDA_CALLABLE bool operator==(const Vector2<T> &lhs, const Vector2<T> &rhs) {
		return
			lhs.x == rhs.x &&
			lhs.y == rhs.y;
	}

	typedef Point3<float> Point3f;
	typedef Rotation3<float> Rotation3f;
	typedef Vector2<float> Vector2f;
	typedef Vector2<int> Size2i;
	typedef Vector2<real> Vector2r;
	typedef Vector2<int> Point2i;

	typedef Point3<real> Point3r;
	typedef Point3r Vector3r;

	//typedef Point3<real> PointWithRange;

	template<typename T>
	CUDA_CALLABLE bool isValid(const Point3<T> &p) {
		return !isnan(p.x) && !isnan(p.y) && !isnan(p.z);
	}

	template<typename T>
	CUDA_CALLABLE Point3<T> operator+(const Point3<T> &lhs, const Point3<T> &rhs) {
		return Point3<T>(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
	}

	template<typename T>
	CUDA_CALLABLE Point3<T> operator-(const Point3<T> &lhs, const Point3<T> &rhs) {
		return Point3<T>(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
	}

	template<typename T, typename Numeric>
	CUDA_CALLABLE Point3<T> operator*(const Numeric &lhs, const Point3<T> &rhs) {
		return Point3<T>(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z);
	}

	template<typename T, typename Numeric>
	CUDA_CALLABLE Point3<T> operator*(const Point3<T> &lhs, const Numeric &rhs) {
		return Point3<T>(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs);
	}

	template<typename T, typename Numeric>
	CUDA_CALLABLE Point3<T> operator/(const Numeric &lhs, const Point3<T> &rhs) {
		return Point3<T>(lhs / rhs.x, lhs / rhs.y, lhs / rhs.z);
	}

	template<typename T, typename Numeric>
	CUDA_CALLABLE Point3<T> operator/(const Point3<T> &lhs, const Numeric &rhs) {
		return Point3<T>(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs);
	}

	template<typename T>
	CUDA_CALLABLE bool operator==(const Point3<T> &lhs, const Point3<T> &rhs) {
		return
			lhs.x == rhs.x &&
			lhs.y == rhs.y &&
			lhs.z == rhs.z;
	}

	template<typename T>
	CUDA_CALLABLE bool operator!=(const Point3<T> &lhs, const Point3<T> &rhs) {
		return
			lhs.x != rhs.x ||
			lhs.y != rhs.y ||
			lhs.z != rhs.z;
	}

	template<typename T>
	CUDA_CALLABLE T sumOfSquares(const Point3<T> &point) {
		return point.x * point.x + point.y * point.y + point.z * point.z;
	}

	template<typename T>
	CUDA_CALLABLE T squaredDistance(const Point3<T> &p, const Point3<T> &q) {
		return sumOfSquares(p - q);
	}

	template<typename T, int rows, int cols>
	class Matrix {
		typedef Matrix<T, rows, cols> ThisClass;

	public:

		T data[rows*cols];

		CUDA_CALLABLE Matrix(const Point3<T> &point, T rest = 0.0) {
			data[0] = point.x;
			data[1] = point.y;
			data[2] = point.z;
			for (int i = 3; i < rows * cols; ++i)
				data[i] = rest;
		}

		CUDA_CALLABLE Matrix(T val = 0.0) {
			for (unsigned int i = 0; i < rows * cols; ++i) {
				data[i] = val;
			}
		}

		CUDA_CALLABLE Matrix(const T *arr) {
			for (int i = 0; i < rows * cols; ++i) {
				data[i] = arr[i];
			}
		}

		Matrix(std::initializer_list<T> l) {
			T *dataPtr = data;
			for (auto it = l.begin(); it != l.end(); ++it) {
				*dataPtr = *it;
				dataPtr++;
			}
		}
		
		Matrix(int n, ...) {
			va_list args;
			va_start(args, n);

			T *dataPtr = data;
			T *dataEnd = data + n;

			do {
				
				*dataPtr = va_arg(args, T);
				dataPtr++;

			} while (dataPtr != dataEnd);

			va_end(args);
		}

		CUDA_CALLABLE const T &operator[](int idx) const
		{
			return data[idx];
		}

		CUDA_CALLABLE T &operator[](int idx)
		{
			return data[idx];
		}

		CUDA_CALLABLE const T &operator()(int row, int col) const
		{
			return data[row * cols + col];
		}

		CUDA_CALLABLE T &operator()(int row, int col)
		{
			return data[row * cols + col];
		}

		CUDA_CALLABLE bool operator==(const ThisClass &other)
		{
			for (int i = 0; i < rows * cols; ++i) {
				if (data[i] != other.data[i])
					return false;
			}
			return true;
		}

		CUDA_CALLABLE Matrix<T, cols, rows> transposed() const
		{
			Matrix<T, cols, rows> transposed;

			for (unsigned int r = 0; r < rows; ++r) {
				for (unsigned int c = 0; c < cols; ++c) {
					unsigned int index1 = r * cols + c;
					unsigned int index2 = c * rows + r;

					transposed[index2] = data[index1];
				}
			}

			return transposed;
		}

		CUDA_CALLABLE bool isValid() const
		{
			for (int i = 0; i < rows * cols; ++i) {
				if (isnan(data[i]))
					return false;
			}
			return true;
		}

		template<int cols2>
		CUDA_CALLABLE Matrix<T, rows, cols2> multiply(const Matrix<T, cols, cols2> &other) {
			Matrix<T, rows, cols2> result;
			
			for (int r = 0; r < rows; ++r) {
				for (int c = 0; c < cols2; ++c) {

					T sum = 0.0;
					for (int i = 0; i < cols; ++i) {
						sum += data[r * cols + i] * other.data[i * cols2 + c];
					}

					result[r * cols2 + c] = sum;
				}
			}

			return result;
		}

		CUDA_CALLABLE Matrix<T, rows, rows> multiplyByTransposition() {
			Matrix<T, rows, rows> result;

			for (int i = 0; i < rows; ++i) {
				for (int j = i; j < rows; ++j) {

					T sum = 0.0;
					for (int r = 0; r < cols; ++r) {
						sum += data[i * cols + r] * data[j * cols + r];
					}
					result.data[i * rows + j] = result.data[j * rows + i] = sum;
				}
			}

			return result;
		}

		template<int cols2>
		CUDA_CALLABLE Matrix<T, rows, cols2> operator*(const Matrix<T, cols, cols2> &other) {
			return multiply(other);
		}

		template<typename V>
		CUDA_CALLABLE ThisClass scale(V factor) const {
			ThisClass result;
			for (int i = 0; i < rows * cols; ++i) {
				result.data[i] = data[i] * factor;
			}
			return result;
		}

		template<typename V>
		CUDA_CALLABLE ThisClass operator*(V factor) const {
			return scale(factor);
		}

		template<typename V>
		CUDA_CALLABLE ThisClass operator/(V factor) const {
			return scale(1.0 / factor);
		}

		CUDA_CALLABLE ThisClass add(const ThisClass &other) const {
			ThisClass result;
			for (int i = 0; i < rows*cols; ++i) {
				result.data[i] = data[i] + other.data[i];
			}
			return result;
		}

		CUDA_CALLABLE ThisClass subtract(const ThisClass &other) const {
			ThisClass result;
			for (int i = 0; i < rows*cols; ++i) {
				result.data[i] = data[i] - other.data[i];
			}
			return result;
		}

		CUDA_CALLABLE ThisClass operator+(const ThisClass &other) const {
			return add(other);
		}

		CUDA_CALLABLE ThisClass operator-(const ThisClass &other) const {
			return subtract(other);
		}

		CUDA_CALLABLE ThisClass inverted() const {
#ifndef __CUDA_ARCH__
			if (rows != cols)
				return ThisClass::invalid;

			int n = rows;
			int s;

			gsl_matrix *m = gsl_matrix_alloc(n, n);
			gsl_matrix *inverse = gsl_matrix_alloc(n, n);
			gsl_permutation *perm = gsl_permutation_alloc(n);

			for (int i = 0; i < rows * cols; ++i)
				m->data[i] = double(data[i]);

			int gslRes = 0;

			gslRes = gsl_linalg_LU_decomp(m, perm, &s);
			if (gslRes != 0) {
				gsl_permutation_free(perm);
				gsl_matrix_free(inverse);
				gsl_matrix_free(m);
				return ThisClass::invalid;
			}

			gslRes = gsl_linalg_LU_invert(m, perm, inverse);
			if (gslRes != 0) {
				gsl_permutation_free(perm);
				gsl_matrix_free(inverse);
				gsl_matrix_free(m);
				return ThisClass::invalid;
			}

			ThisClass result;

			for (int i = 0; i < rows * cols; ++i)
				result.data[i] = T(inverse->data[i]);

			gsl_permutation_free(perm);
			gsl_matrix_free(inverse);
			gsl_matrix_free(m);

			return result;

#else
			ThisClass result;
			cuGSL::matInv(data, rows, result.data);
			return result;
#endif
		}

		CUDA_CALLABLE T vectorNorm() const {
			if (rows > 1 && cols > 1) {
			#ifdef __CUDA_ARCH__
				return CUDART_NAN_F;
			#else
				return std::numeric_limits<T>::quiet_NaN();
			#endif
			}

			T sum = T(0.0);
			for (int i = 0; i < rows * cols; ++i) {
				sum += data[i] * data[i];
			}
			return sqrt(sum);
		}

		std::string toMatlab() const {
			stringstream ss;
			ss << "[";

			for (int r = 0; r < rows; ++r) {
				for (int c = 0; c < cols; ++c) {
					ss << data[r * cols + c];
					if (c == cols - 1)
						ss << ";";
					else
						ss << ",";
				}
			}

			ss << "]";
			return ss.str();			
		}

		std::string toString() const {
			stringstream ss;
			
			ss << "[";
			for (int r = 0; r < rows; ++r) {
				ss << "[";
				for (int c = 0; c < cols; ++c) {
					ss << data[r * cols + c];
					if (c < cols - 1)
						ss << ", ";
				}
				ss << "]";
			}
			ss << "]";

			return ss.str();
		}

		static const ThisClass zero;
		static const ThisClass invalid;
	};

	template<typename T, int rows, int cols>
	const Matrix<T, rows, cols> Matrix<T, rows, cols>::zero = Matrix<T, rows, cols>();

	template<typename T, int rows, int cols>
	const Matrix<T, rows, cols> Matrix<T, rows, cols>::invalid = Matrix<T, rows, cols>(std::numeric_limits<T>::quiet_NaN());

	typedef Matrix<real, 3, 3> Matrix33r;
	typedef Matrix<real, 4, 4> Matrix44r;
	typedef Matrix<real, 4, 1> Matrix41r;

	template<>
	template<>
	CUDA_CALLABLE Matrix33r Matrix33r::operator*(const Matrix33r &other);

	// Matrix multiply A_NxM x B_MxP = C_NxP
	template<typename T, int n, int m, int p>
	CUDA_CALLABLE Matrix<T, n, p> matMul(const Matrix<T, n, m> &mat1, const Matrix<T, m, p> &mat2) {
		Matrix<T, n, p> result;
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < p; ++j) {
				for (int k = 0; k < m; ++k) {
					result(i, j) += mat1(i, k) * mat2(k, j);
				}
			}
		}
		return result;
	}

	template<>
	CUDA_CALLABLE Matrix<real, 4, 1> matMul<real, 4, 4, 1>(const Matrix<real, 4, 4> &mat1, const Matrix<real, 4, 1> &mat2);

	CUDA_CALLABLE Matrix44r getTransformFromUnitVectorsZY(const Vector3r &zAxis, const Vector3r &yDirection);
	CUDA_CALLABLE Matrix44r getTransformFromTwoUnitVectors(const Vector3r &yDirection, const Vector3r &zAxis);

	template<typename T>
	CUDA_CALLABLE Point3<T>::Point3(const Matrix<T, 3, 1> &matrix) {
		x = matrix[0];
		y = matrix[1];
		z = matrix[2];
	}

	enum class Direction : int {
		Up,
		Right,
		Down,
		Left
	};

	
	struct MY_ALIGN(16) NeighborWithDistance
	{
		real distance;
		Vector3r point;
		CUDA_CALLABLE bool operator < (const NeighborWithDistance& other) const {
			return distance<other.distance;
		}
	};

	struct NARFParameters
	{
		// border extraction parameters
		int supportingSize;
		int pointsUsedForAverage;
		int smoothingRadius;
		int reduceBorderSearchingDistance;
		real borderScoreThreshold;
		int localSurfaceRadius;
		int localSurfaceNumerOfClosestNeighbors;

		// interest points parameters
		int pixelRadiusBorderDirections;
		real minimumBorderProbability;
		int minimumWeight;
		int mainPrincipalCurvatureRadius;
		real minimumInterestValue;
		real searchRadius;
		real optimalDistanceToHighSurfaceChange;
		bool addPointsOnStraightEdges;
		bool doNonMaximumSuppression;
		int numberOfPolynomialApproximations;
		int maxNumberOfInterestPoints;
		real minDistanceBetweenInterestPoints;
	};


	struct PrincipalCurvature
	{
		CUDA_CALLABLE PrincipalCurvature(real magnitude = 0, const Vector3r &direction = Vector3r()) : magnitude(magnitude), direction(direction) {}

		real magnitude;
		Vector3r direction;

		CUDA_CALLABLE static PrincipalCurvature invalid()
		{
		#ifdef __CUDA_ARCH__
			return PrincipalCurvature(CUDART_NAN_F, Vector3r::invalidPoint());
		#else
			return PrincipalCurvature(std::numeric_limits<real>::quiet_NaN(), Vector3r::invalidPoint());
		#endif
		}
	};

	struct InterestPoint
	{
		Point2i point2d;
		Point3r point3d;
		real interestValue;

		CUDA_CALLABLE static bool isBetter(const InterestPoint &ip1, const InterestPoint &ip2);
	};

	CUDA_CALLABLE bool operator<(const InterestPoint &l, const InterestPoint &r);

	struct Quaternion
	{
		float w;
		float x;
		float y;
		float z;

		Quaternion(float w, float x, float y, float z) : x(x), y(y), z(z), w(w) {}

		Quaternion(Vector3r vector, float angle) {
			vector.normalize();

			float s = sinf(angle/2);
			float c = cosf(angle/2);

			w = c;
			x = vector.x * s;
			y = vector.y * s;
			z = vector.z * s;
		}

		Quaternion(const Vector3r &vector) : w(0.0f), x(vector.x), y(vector.y), z(vector.z) {}

		Quaternion conj() {
			return Quaternion(w, -x, -y, -z);
		}

		Quaternion operator*(const Quaternion &other) {
			return Quaternion(
				w*other.w - x*other.x - y*other.y - z*other.z,
				w*other.x + x*other.w + y*other.z - z*other.y,
				w*other.y - x*other.z + y*other.w + z*other.x,
				w*other.z + x*other.y - y*other.x + z*other.w
				);
		}

		Quaternion operator*(const Vector3r &vec) {
			return (*this) * Quaternion(vec);
		}

		Quaternion normalized() {
			float magnitude = sqrtf(w*w + x*x + y*y + z*z);
			return Quaternion(w/magnitude, x/magnitude, y/magnitude, z/magnitude);
		}

		Vector3r toVector() {
			return Vector3r(x, y, z);
		}
	};

};

template<>
struct std::is_arithmetic < NARF::Matrix<NARF::real, 3, 3> > {
	using value = false_type;
};

template<typename T, int size>
struct array_wrapper
{
	CUDA_CALLABLE array_wrapper(const T obj[size]) {
		memcpy(data, obj, sizeof(T) * size);
	}

	T data[size];

	CUDA_CALLABLE T &operator[](const size_t idx) {
		return data[idx];
	}
};

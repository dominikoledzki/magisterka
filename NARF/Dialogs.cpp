#include "Dialogs.h"

GtkDialog *load_dialog(const char *filename, gpointer user_data)
{
	GtkBuilder *builder = gtk_builder_new();
	gtk_builder_add_from_file(builder, filename, NULL);
	gtk_builder_connect_signals(builder, user_data);

	GSList *objects = gtk_builder_get_objects(builder);
	GSList *node = objects;
	GtkDialog *dialog = NULL;

	while (node != NULL && !GTK_IS_DIALOG(node->data)) {
		node = node->next;
	}

	if (node != NULL) {
		dialog = GTK_DIALOG(node->data);
	}

	g_slist_free(objects);

	return dialog;
}
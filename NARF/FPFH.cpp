#include "FPFH.h"
#include <deque>
#include "PointAverage.h"

namespace FPFH
{
	void calculateNormals(FPFHContext &context)
	{
		LOG_FUNCTION_CALL

		const int width = context.width;
		const int height = context.height;
		const float radius = context.normalsRadius;
		const float squaredRadius = radius * radius;

		const Point3r *world = context.world;

		bool *wasTouched = new bool[width * height];

		for (int i = 0; i < width * height; ++i)
			wasTouched[i] = false;

		std::vector<Point2i> pointsToCheck;
		std::vector<Point2i> touchedPoints;

		int *pointsToCheckCount = new int[width * height];

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {

				pointsToCheckCount[y * width + x] = -1;

				context.normals[y * width + x] = Vector3r::invalidPoint();

				Point3r p = world[y * width + x];
				if (!isValid(p))
					continue;

				pointsToCheck.clear();
				touchedPoints.clear();

				pointsToCheck.push_back(Point2i(x, y));
				wasTouched[y * width + x] = true;
				touchedPoints.push_back(Point2i(x, y));
				
				for (int i = 0; i < pointsToCheck.size(); ++i) {

					Point2i c = pointsToCheck[i];

					for (int y2 = c.y - 1; y2 <= c.y + 1; ++y2) {
						for (int x2 = c.x - 1; x2 <= c.x + 1; ++x2) {
						
							if (x2 == c.x && y2 == c.y)
								continue;

							if (x2 < 0 || x2 >= width || y2 < 0 || y2 >= height)
								continue;

							if (wasTouched[y2 * width + x2])
								continue;

							wasTouched[y2 * width + x2] = true;
							touchedPoints.push_back(Point2i(x2, y2));

							if (!isValid(world[y2 * width + x2]))
								continue;

							Point3r p2 = world[y2 * width + x2];
							float dist = (p2 - p).squaredNorm();
							if (dist > squaredRadius)
								continue;

							pointsToCheck.push_back(Point2i(x2, y2));
						}
					}
				}

				pointsToCheckCount[y * width + x] = pointsToCheck.size();

				for (auto it = touchedPoints.begin(); it != touchedPoints.end(); ++it) {
					const Point2i &i = *it;
					wasTouched[i.y * width + i.x] = false;
				}

				//cout << "Points to check: " << pointsToCheck.size() << "\n";

				if (pointsToCheck.size() >= 3) {
					PointAverage average;
					for (auto it = pointsToCheck.begin(); it != pointsToCheck.end(); ++it) {
						const Point2i &i = *it;
						const Point3r &p = world[i.y * width + i.x];
						average.addSample(p);
					}

					Vector3r normal, eigenVector2, eigenVector3;
					Matrix<float, 3, 1> eigenValues;
					average.pca(eigenValues, normal, eigenVector2, eigenVector3);

					Vector3r viewingDirection = -p;
					if (normal.dotProduct(viewingDirection) < 0.0f)
						normal = -normal;

					context.normals[y * width + x] = normal;
				}
			}
		}


		/*int min = INT_MAX;
		int max = INT_MIN;

		for (int i = 0; i < width * height; ++i) {
			if (pointsToCheckCount[i] != -1 && pointsToCheckCount[i] < min) {
				min = pointsToCheckCount[i];
			}
			if (pointsToCheckCount[i] != -1 && pointsToCheckCount[i] > max) {
				max = pointsToCheckCount[i];
			}
		}

		double m = mean(pointsToCheckCount, 0, width * height);

		for (int i = 0; i < width * height; ++i) {
			int v = pointsToCheckCount[i];
			pointsToCheckCount[i] = (v - m) * (v - m);
		}

		double stddev = mean(pointsToCheckCount, 0, width * height);
		stddev = sqrt(stddev);

		cout << "min: " << min << " max: " << max << " mean: " << m << " stddev: " << stddev << endl;*/
	}

	Point2i point2dAt(const Point3f &p, int width, int height, float xzFactor, float yzFactor)
	{
		float normalizedX = p.x / (p.z * xzFactor);
		float normalizedY = p.y / (p.z * yzFactor);

		Point2i d;
		d.x = static_cast<int>(width * (normalizedX + 0.5f));
		d.y = static_cast<int>(height * (0.5f - normalizedY));
		return d;
	}

	void calculateNormals2(FPFHContext &context)
	{
		LOG_FUNCTION_CALL

		const int width = context.width;
		const int height = context.height;
		const float radius = context.normalsRadius;
		const float squaredRadius = radius * radius;

		const Point3r *world = context.world;

		// find max 2d radius to check

		int *minY = new int[width * height];
		int *maxY = new int[width * height];

		int *minX = new int[width * height];
		int *maxX = new int[width * height];

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {

				int index = y * width + x;

				const Point3r &p = world[index];

				if (!isValid(p)) {
					continue;
				}

				Point3r edgePoints[4];

				{
					float a1 = (p.z * p.x - radius * sqrtf(p.z * p.z + p.x * p.x - radius * radius)) / (p.z * p.z - radius * radius);
					float a2 = (p.z * p.x + radius * sqrtf(p.z * p.z + p.x * p.x - radius * radius)) / (p.z * p.z - radius * radius);

					float z1 = (p.z + a1*p.x) / (a1*a1 + 1);
					float z2 = (p.z + a2*p.x) / (a2*a2 + 1);

					float x1 = a1 * z1;
					float x2 = a2 * z2;

					edgePoints[0] = Point3r(x1, p.y, z1);
					edgePoints[1] = Point3r(x2, p.y, z2);
				}

				{
					float a1 = (p.z * p.y - radius * sqrtf(p.z * p.z + p.y * p.y - radius * radius)) / (p.z * p.z - radius * radius);
					float a2 = (p.z * p.y + radius * sqrtf(p.z * p.z + p.y * p.y - radius * radius)) / (p.z * p.z - radius * radius);

					float z1 = (p.z + a1*p.y) / (a1*a1 + 1);
					float z2 = (p.z + a2*p.y) / (a2*a2 + 1);

					float y1 = a1 * z1;
					float y2 = a2 * z2;

					edgePoints[2] = Point3r(p.x, y1, z1);
					edgePoints[3] = Point3r(p.x, y2, z2);
				}

				minY[index] = height;
				maxY[index] = 0;
				minX[index] = width;
				maxX[index] = 0;

				for (int i = 0; i < 4; ++i) {
					Point2i pd = point2dAt(edgePoints[i], width, height, context.xzFactor, context.yzFactor);
					minY[index] = MIN(minY[index], pd.y);
					maxY[index] = MAX(maxY[index], pd.y);

					minX[index] = MIN(minX[index], pd.x);
					maxX[index] = MAX(maxX[index], pd.x);
				}

				minY[index] = MAX(minY[index], 0);
				maxY[index] = MIN(maxY[index], height - 1);

				minX[index] = MAX(minX[index], 0);
				maxX[index] = MIN(maxX[index], width - 1);
			}
		}

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {

				int index = y * width + x;
				context.normals[index] = Vector3r::invalidPoint();

				Point3r p = world[index];
				if (!isValid(p))
					continue;

				PointAverage average;

				for (int y2 = minY[index]; y2 <= maxY[index]; ++y2) {
					for (int x2 = minX[index]; x2 <= maxX[index]; ++x2) {
						const Point3r &p2 = world[y2 * width + x2];

						if ((p - p2).squaredNorm() < squaredRadius)
							average.addSample(p2);
					}
				}

				if (average.getNoOfSamples() >= 3) {
					Vector3r normal, eigenVector2, eigenVector3;
					Matrix<float, 3, 1> eigenValues;
					average.pca(eigenValues, normal, eigenVector2, eigenVector3);

					Vector3r viewingDirection = -p;
					if (normal.dotProduct(viewingDirection) < 0.0f)
						normal = -normal;

					context.normals[y * width + x] = normal;
				}
			}
		}

		delete[] minY;
		delete[] maxY;
		delete[] minX;
		delete[] maxX;
	}

	//===============================================================================================================//

	Features computeFeatures(const Point3r &p, const Vector3r &pn, const Point3r &q, const Vector3r &qn)
	{
		Point3r pi = p;
		Vector3r ni = pn;
		Point3r pj = q;
		Vector3r nj = qn;

		Point3r ps, pt;
		Vector3r ns, nt;

		float distance = (pj - pi).norm();
		float anglei = acos(ni.dotProduct(pj-pi)/distance);
		float anglej = acos(nj.dotProduct(pi-pj)/distance);

		if (anglei < anglej) 
		{
			ps = pi;
			pt = pj;

			ns = ni;
			nt = nj;
		}
		else 
		{
			ps = pj;
			pt = pi;

			ns = nj;
			nt = ni;
		}


		// Darboux frame
		Vector3r u = ns;
		Vector3r v = (pt-ps).normalized().crossProduct(u);
		Vector3r w = u.crossProduct(v);

		Features features;
		features.alpha = v.dotProduct(nt);
		features.phi = u.dotProduct(pt - ps)/(pt-ps).norm();
		features.theta = atan2f(w.dotProduct(nt), u.dotProduct(nt));

		return features;
	}

	void calculateSPFHs(FPFHContext &context)
	{
		LOG_FUNCTION_CALL

		const int width = context.width;
		const int height = context.height;

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {

				const Point3r &p = context.world[y * width + x];
				if (!isValid(p))
					continue;

				const Vector3r &n = context.normals[y * width + x];
				if (!isValid(n))
					continue;

				for (int y2 = y - 1; y2 <= y + 1; ++y2) {
					if (y2 < 0 || y2 >= height)
						continue;

					for (int x2 = x - 1; x2 <= x + 1; ++x2) {
						if (x2 < 0 || x2 >= width)
							continue;

						if (x2 == x && y2 == y)
							continue;

						const Point3r &p2 = context.world[y2 * width + x2];
						if (!isValid(p2))
							continue;

						const Vector3r &n2 = context.normals[y2 * width + x2];
						if (!isValid(n2))
							continue;

						Features f = computeFeatures(p, n, p2, n2);
						context.spfh.addFeatures(f, x, y);
						context.spfh.setValid(true, x, y);
					}
				}
			}
		}
		//context.spfh.normalize();
	}

	//===============================================================================================================//

	void calculateFPFHs(FPFHContext &context)
	{
		LOG_FUNCTION_CALL

		const int width = context.width;
		const int height = context.height;
		const float radius = context.radius;
		const float squaredRadius = radius * radius;

		// copy spfh
		memcpy(context.fpfh.valid, context.spfh.valid, sizeof(bool) * width * height);
		for (int i = 0; i < context.spfh.histSize; ++i) {
			memcpy(context.fpfh.data[i], context.spfh.data[i], sizeof(float) * width * height);
		}

		const Point3r *world = context.world;

		bool *wasTouched = new bool[width * height];

		for (int i = 0; i < width * height; ++i)
			wasTouched[i] = false;

		std::vector<Point2i> pointsToCheck;
		std::vector<Point2i> touchedPoints;

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {

				Point3r p = world[y * width + x];
				if (!isValid(p))
					continue;

				pointsToCheck.clear();
				touchedPoints.clear();

				pointsToCheck.push_back(Point2i(x, y));
				wasTouched[y * width + x] = true;
				touchedPoints.push_back(Point2i(x, y));

				for (int i = 0; i < pointsToCheck.size(); ++i) {

					Point2i c = pointsToCheck[i];

					for (int y2 = c.y - 1; y2 <= c.y + 1; ++y2) {
						for (int x2 = c.x - 1; x2 <= c.x + 1; ++x2) {

							if (x2 == c.x && y2 == c.y)
								continue;

							if (x2 < 0 || x2 >= width || y2 < 0 || y2 >= height)
								continue;

							if (wasTouched[y2 * width + x2])
								continue;

							wasTouched[y2 * width + x2] = true;
							touchedPoints.push_back(Point2i(x2, y2));

							if (!isValid(world[y2 * width + x2]))
								continue;

							if (!context.spfh.isValid(x2, y2))
								continue;

							Point3r p2 = world[y2 * width + x2];
							if ((p2 - p).squaredNorm() > squaredRadius)
								continue;

							pointsToCheck.push_back(Point2i(x2, y2));
						}
					}
				}

				for (auto it = touchedPoints.begin(); it != touchedPoints.end(); ++it) {
					const Point2i &i = *it;
					wasTouched[i.y * width + i.x] = false;
				}

				float scale = 1.0f / pointsToCheck.size();
				for (auto it = pointsToCheck.begin(); it != pointsToCheck.end(); ++it) {
					Point2i p2i = *it;

					if (p2i.x == x && p2i.y == y)
						continue;

					const Point3r &p2 = world[p2i.y * width + p2i.x];
					float dist = (p-p2).norm();

					for (int i = 0; i < context.fpfh.histSize; ++i) {
						float k = scale / dist;
						context.fpfh.data[i][y * width + x] += context.spfh.data[i][p2i.y * width + p2i.x];
					}
				}
			}
		}

		context.fpfh.normalize();
	}
}

#pragma once
#include "DataTypes.h"
#include "FPFHContext.h"

using namespace NARF;

namespace FPFH
{
	template<typename T>
	T mean(T *arr, int minIdx, int maxIdx) {
		if (maxIdx - minIdx == 1) {
			if (arr[minIdx] == -1)
				return NAN;
			else
				return arr[minIdx];
		}

		int midIdx = (minIdx + maxIdx) / 2;
		T m1 = mean(arr, minIdx, midIdx);
		T m2 = mean(arr, midIdx, maxIdx);

		if (!isnan(m1)) {
			if (!isnan(m2))
				return (m1 + m2) * 0.5;
			else
				return m1;
		}
		else {
			if (!isnan(m2))
				return m2;
			else
				return NAN;
		}
	}

	void calculateNormals(FPFHContext &context);
	void calculateNormals2(FPFHContext &context);
	void calculateSPFHs(FPFHContext &context);
	void calculateFPFHs(FPFHContext &context);
}

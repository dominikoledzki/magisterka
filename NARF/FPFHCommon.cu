#include "FPFHCommon.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

namespace FPFH {

	CUDA_CALLABLE unsigned indexForFeature(float value, float minValue, float maxValue, unsigned div)
	{
		int idx = (int)((value - minValue) / (maxValue - minValue) * div);
		idx = MAX(0, MIN(div-1, idx));
		return (unsigned)idx;
	}

	CUDA_CALLABLE FeatureIndices Features::indicesForHist(unsigned div) const
	{
		unsigned idx1 = indexForFeature(alpha, -1.0f, 1.0f, div);
		unsigned idx2 = indexForFeature(phi, -1.0f, 1.0f, div);
		unsigned idx3 = indexForFeature(theta, -M_PI, M_PI, div);

		return FeatureIndices(idx1, idx2, idx3);
	}

	SPFH::SPFH(unsigned div, size_t width, size_t height, bool device) :
		width(width),
		height(height),
		div(div),
		histSize(3 * div),
		device(device)
	{

		if (!device) {
			data = new float*[histSize];
			hostData = new float*[histSize];
			for (int i = 0; i < histSize; ++i) {
				data[i] = new float[width * height];
				hostData[i] = data[i];
				for (int j = 0; j < width * height; ++j) {
					data[i][j] = 0.0f;
				}
			}

			valid = new bool[width * height];
			for (int i = 0; i < width * height; ++i)
				valid[i] = false;

		} else {

			hostData = new float*[histSize];
			CUDA_SAFE_CALL(cudaMalloc(&data, sizeof(float) * histSize));

			for (unsigned i = 0; i < histSize; i++)	{
				CUDA_SAFE_CALL(cudaMalloc(&(hostData[i]), sizeof(float) * width * height));
			}

			CUDA_SAFE_CALL(cudaMemcpy(data, hostData, sizeof(float *) * histSize, cudaMemcpyHostToDevice));
			CUDA_SAFE_CALL(cudaMalloc(&valid, sizeof(bool) * width * height));
		}
	}

	SPFH::SPFH(const SPFH &other, bool device) :
		SPFH(other.div, other.width, other.height, device)
	{
		float **dataHost = new float*[histSize];
		CUDA_SAFE_CALL(cudaMemcpy(dataHost, other.data, sizeof(float*) * histSize,
			other.device ? cudaMemcpyDeviceToHost : cudaMemcpyHostToHost));
		
		cudaMemcpyKind cpyKind;
		if (other.device && device) {
			cpyKind = cudaMemcpyDeviceToDevice;
		}
		else if (other.device && !device) {
			cpyKind = cudaMemcpyDeviceToHost;
		}
		else if (!other.device && device) {
			cpyKind = cudaMemcpyHostToDevice;
		}
		else {
			cpyKind = cudaMemcpyHostToHost;
		}

		CUDA_SAFE_CALL(cudaMemcpy(valid, other.valid, sizeof(bool) * width * height, cpyKind));
		for (int i = 0; i < histSize; ++i) {
			CUDA_SAFE_CALL(cudaMemcpy(hostData[i], other.hostData[i], sizeof(float) * width * height, cpyKind));
		}
	}

	CUDA_CALLABLE bool SPFH::isValid(int x, int y)
	{
		return valid[y * width + x];
	}

	CUDA_CALLABLE void SPFH::setValid(bool valid, int x, int y)
	{
		this->valid[y * width + x] = valid;
	}

	CUDA_CALLABLE void SPFH::addFeatures(const Features &f, int x, int y)
	{
		FeatureIndices idx = f.indicesForHist(div);
		data[idx.idx1][y * width + x] += 1.0f;
		data[div + idx.idx2][y * width + x] += 1.0f;
		data[2 * div + idx.idx3][y * width + x] += 1.0f;
	}

	size_t SPFH::serializedDataSize(unsigned div, size_t width, size_t height)
	{
		return sizeof(bool) * width * height + sizeof(float) * div * div * div * width * height;
	}

	void SPFH::serialize(char **outData)
	{
		size_t size = SPFH::serializedDataSize(div, width, height);
		(*outData) = new char[size];
		char *offset = *outData;

		int s = sizeof(bool) * width * height;
		memcpy(offset, (char *)valid, s);
		offset += s;

		s = sizeof(float) * width * height;
		for (int i = 0; i < div * div * div; ++i) {
			char *dataToCopy = (char *)data[i];
			memcpy(offset, dataToCopy, s);
			offset += s;
		}
	}

	void SPFH::deserialize(char *inData)
	{
		char *offset = inData;
		int s = sizeof(bool) * width * height;
		memcpy(valid, offset, s);
		offset += s;

		s = sizeof(float) * width * height;
		for (int i = 0; i < div * div * div; ++i) {
			char *dest = (char *)data[i];
			memcpy(dest, offset, s);
			offset += s;
		}
	}

	void SPFH::normalize()
	{
		for (int i = 0; i < width * height; ++i) {
			if (!valid[i])
				continue;

			float sum = 0.0f;
			for (int d = 0; d < histSize; ++d) {
				sum += data[d][i];
			}

			if (sum == 0.0f)
				continue;

			float factor = 1.0f / sum;
			for (int d = 0; d < histSize; ++d) {
				data[d][i] *= factor;
			}
		}
	}

	void SPFH::dispose()
	{
		if (!device) {
			for (int i = 0; i < histSize; ++i) {
				delete data[i];
				data[i] = nullptr;
				hostData[i] = nullptr;
			}

			delete[] data;
			delete[] hostData;

			data = hostData = nullptr;

			delete[] valid;
			valid = nullptr;
	
		}
		else {
			for (unsigned i = 0; i < histSize; i++)	{
				cudaFree(hostData[i]);
			}

			delete[] hostData;
			cudaFree(data);
			hostData = data = nullptr;

			cudaFree(valid);
			valid = nullptr;
		}
	}

	std::vector<float> SPFH::meanHistogram(int minIdx, int maxIdx)
	{
		std::vector<float> result;
		if (maxIdx - minIdx == 1) {
			for (int i = 0; i < histSize; ++i) {
				result.push_back(data[i][minIdx]);
			}
			return result;
		}

		int medIdx = (minIdx + maxIdx) / 2;
		std::vector<float> h1 = meanHistogram(minIdx, medIdx);
		std::vector<float> h2 = meanHistogram(medIdx, maxIdx);
		for (int i = 0; i < histSize; ++i) {
			result.push_back((h1[i] + h2[i]) * 0.5f);
		}
		return result;
	}

	std::vector<float> SPFH::meanHistogram()
	{
		return meanHistogram(0, width * height);
	}
}

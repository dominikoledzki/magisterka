#pragma once
#include "DataTypes.h"
#include "Utilities.h"

namespace FPFH {

	struct FeatureIndices
	{
		unsigned idx1;
		unsigned idx2;
		unsigned idx3;

		CUDA_CALLABLE FeatureIndices(unsigned idx1, unsigned idx2, unsigned idx3) : idx1(idx1), idx2(idx2), idx3(idx3) {}
	};

	struct Features
	{
		float alpha;
		float phi;
		float theta;

		CUDA_CALLABLE FeatureIndices indicesForHist(unsigned div) const;
	};

	struct SPFH
	{
		SPFH(unsigned div, size_t width, size_t height, bool device = false);
		SPFH(const SPFH &other) = default;
		SPFH(const SPFH &other, bool device);

		const size_t width;
		const size_t height;
		const unsigned div;
		const unsigned histSize;
		const bool device;

		CUDA_CALLABLE bool isValid(int x, int y);
		CUDA_CALLABLE void setValid(bool valid, int x, int y);
		CUDA_CALLABLE void addFeatures(const Features &f, int x, int y);
		void serialize(char **outData);
		void deserialize(char *inData);
		static size_t serializedDataSize(unsigned div, size_t width, size_t height);
		void normalize();

		void dispose();

		std::vector<float> meanHistogram();

		bool *valid;
		float **data;	// float[hist_size][width*height]
		float **hostData;

	protected:
		std::vector<float> meanHistogram(int minIdx, int maxIdx);
	};

}

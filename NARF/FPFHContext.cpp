#include "FPFHContext.h"
#include "cuda.h"
#include "cuda_runtime.h"

namespace FPFH
{
	FPFHContext::FPFHContext(int width, int height, unsigned binsPerFeature, Point3r *world) :
		width(width),
		height(height),
		binsPerFeature(binsPerFeature), 
		spfh(binsPerFeature, width, height),
		fpfh(binsPerFeature, width, height)
	{
		int size = width * height;

		this->world = new Point3r[size];
		if (world) {
			memcpy(this->world, world, sizeof(Point3r) * size);
		}

		normals = new Vector3r[size];
	}

	FPFHContext::FPFHContext(const cuFPFH::cuFPFHContext &context) :
		width(context.width),
		height(context.height),
		binsPerFeature(context.binsPerFeature),
		spfh(context.spfh, false),
		fpfh(context.fpfh, false)
	{
		int size = width * height;

		this->world = new Point3r[size];
		normals = new Vector3r[size];

		CUDA_SAFE_CALL(cudaMemcpy(world, context.world, size * sizeof(Point3r), cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(normals, context.normals, size * sizeof(Vector3r), cudaMemcpyDeviceToHost));
	}

	FPFHContext::~FPFHContext(){}

	void FPFHContext::dumpToFile(const string &filename)
	{
		fstream file = fstream(filename, ios_base::out | ios_base::binary);

		int size = width * height;

		// input data
		file.write((char *)&width, sizeof(width));
		file.write((char *)&height, sizeof(height));
		file.write((char *)world, sizeof(Point3r) * size);

		// parameters
		file.write((char *)&binsPerFeature, sizeof(binsPerFeature));
		file.write((char *)&normalsRadius, sizeof(normalsRadius));
		file.write((char *)&radius, sizeof(radius));
	
		// intermediate and output data
		file.write((char *)normals, sizeof(Point3r) * size);

		char *spfhData;
		size_t spfhDataSize = SPFH::serializedDataSize(spfh.div, spfh.width, spfh.height);
		spfh.serialize(&spfhData);
		file.write((char *)spfhData, spfhDataSize);
		delete[] spfhData;
	}

	void FPFHContext::loadFromFile(const string &filename)
	{
		fstream file = fstream(filename, ios_base::in | ios_base::binary);

		int size = width * height;

		// input data
		file.read((char *)&width, sizeof(width));
		file.read((char *)&height, sizeof(height));
		file.read((char *)world, sizeof(Point3r) * size);

		// parameters
		file.read((char *)&binsPerFeature, sizeof(binsPerFeature));
		file.read((char *)&normalsRadius, sizeof(normalsRadius));
		file.read((char *)&radius, sizeof(radius));

		// intermediate and output data
		file.read((char *)normals, sizeof(Point3r) * size);

		size_t spfhDataSize = SPFH::serializedDataSize(spfh.div, spfh.width, spfh.height);
		char *spfhData = new char[spfhDataSize];
		file.read((char *)spfhData, spfhDataSize);
		spfh.deserialize(spfhData);
		delete[] spfhData;
	}

	void FPFHContext::dispose()
	{
		delete[] world;
		delete[] normals;

		world = normals = nullptr;

		spfh.dispose();
		fpfh.dispose();
	}
}

#pragma once
#include "FPFHCommon.h"
#include "cuFPFHContext.h"

using namespace NARF;

namespace FPFH
{
	class FPFHContext
	{
	public:
		FPFHContext(int width, int height, unsigned binsPerFeature, Point3r *world);
		FPFHContext(const cuFPFH::cuFPFHContext &context);
		~FPFHContext();

		void copyFPFHData(const cuFPFH::cuFPFHContext &context);

		void dumpToFile(const string &filename);
		void loadFromFile(const string &filename);
		void dispose();

		// input data
		int width;
		int height;
		float xzFactor;
		float yzFactor;
		Point3r *world;

		// parameters
		unsigned binsPerFeature;
		float normalsRadius;
		float radius;	

		// intermediate and output data
		Vector3r *normals;
		SPFH spfh;
		SPFH fpfh;
	};
}

#include "Frame.h"
#include <string>
#include <iostream>
#include <fstream>

#include "Utilities.h"

using namespace std;

namespace NARF {

	Frame::Frame(int width, int height, real xzFactor, real yzFactor, depth_t *depthImage) :
		width(width), height(height), xzFactor(xzFactor), yzFactor(yzFactor), worldImage(nullptr)
	{
		this->depthImage = new depth_t[width * height];
		memcpy(this->depthImage, depthImage, sizeof(unsigned short) * width * height);

		//generateWorldImage();
	}

	Frame::Frame(int width, int height, real xzFactor, real yzFactor, istream &stream) :
		width(width), height(height), xzFactor(xzFactor), yzFactor(yzFactor), worldImage(nullptr)
	{
		this->depthImage = new depth_t[width * height];
		int count = sizeof(depth_t) / sizeof(char) * width * height;
		stream.read((char *)depthImage, count);
		//generateWorldImage();
	}

	const depth_t &Frame::depthAt(int x, int y) const
	{
		return depthImage[y*width + x];
	}

	const Point3r &Frame::pointAt(int x, int y) const
	{
		if (!worldImage)
			generateWorldImage();
		return worldImage[y*width + x];
	}

	const Point3r Frame::pointAt(int x, int y, depth_t depth) const
	{
		real normalizedY = (real)0.5 - (real)y / width;
		real normalizedX = (real)x / height - (real)0.5;
		Point3r newPoint;
		newPoint.x = normalizedX * depth * xzFactor;
		newPoint.y = normalizedY * depth * yzFactor;
		newPoint.z = depth;
		return newPoint;
	}

	/*const PointWithRange Frame::pointWithRangeAt(int x, int y) const
	{
		return PointWithRange(x, y, depthImage[y*width + x]);
	}*/

	const Point3r Frame::get1DPointAverage(int x, int y, int xDelta, int yDelta, int numberOfPoints) const
	{
		Point3r averagePoint;
		float sumWeight = 0.0f;

		int x2, y2;
		for (int step = 0; step < numberOfPoints; ++step) {
			x2 = x + step * xDelta;
			y2 = y + step * yDelta;

			if (x2 < 0 || y2 < 0 || x2 >= width || y2 >= height || !isValid(x2, y2))
				continue;

			Point3r neighbor = pointAt(x2, y2);
			averagePoint += neighbor;
			sumWeight += 1.0f;
		}

		if (sumWeight == 0.0f)
			return Point3r::invalidPoint();

		averagePoint /= sumWeight;
		return averagePoint;
	}

	bool Frame::isValid(int x, int y) const
	{
		if (x < 0 || x >= width || y < 0 || y >= height)
			return false;

		return depthImage[y*width + x] != 0;
	}

	Point2i Frame::imagePointAt(const Point3r &p) const
	{
		Point2i d;

		real normalizedX = p.x / (p.z * xzFactor);
		real normalizedY = p.y / (p.z * yzFactor);

		d.x = static_cast<int>(width * (normalizedX + 0.5f));
		d.y = static_cast<int>(height * (0.5f - normalizedY));

		return d;
	}

	int Frame::getWidth() const
	{
		return width;
	}

	int Frame::getHeight() const
	{
		return height;
	}

	int Frame::getSize() const {
		return width * height;
	}

	depth_t *Frame::getDepthData()
	{
		return depthImage;
	}

	const Point3r *Frame::getWorldData() const
	{
		if (!worldImage)
			generateWorldImage();

		return worldImage;
	}

	cv::Mat Frame::get_depth_mat()
	{
		return cv::Mat(height, width, CV_16UC1, depthImage);
	}

	cv::Mat Frame::get_grayscale_image()
	{
		cv::Mat image = get_depth_mat();
		cv::Mat grayscale;
		double minVal, maxVal;
		cv::minMaxLoc(image, &minVal, &maxVal);
		minVal = 0.0f;

		double a = 255.0 / (maxVal - minVal);
		double b = -a * minVal;

		image.convertTo(grayscale, CV_8UC1, a, b);
		return grayscale;
	}	

	cv::Mat Frame::get_depth_image()
	{
		cv::Mat mat = get_grayscale_image();
		vector<cv::Mat> channels = { mat, mat, mat };
		cv::Mat result;
		cv::merge(channels, result);
		return result;
	}

	cv::Mat Frame::get_miniature_image()
	{
		cv::Mat grayscale = get_grayscale_image();
		double scale = 60.0f / grayscale.size().width;
		cv::Mat scaled;
		cv::resize(grayscale, scaled, cv::Size(), scale, scale);
		vector<cv::Mat> channels = { scaled, scaled, scaled };
		
		cv::Mat result;
		cv::merge(channels, result);


		return result;
	}

	Matrix44r Frame::getRotationToViewerCoordinateFrame(const Point3r &point) const
	{
		Vector3r viewingDirection = point.normalized();
		return getTransformFromTwoUnitVectors(Vector3r(0.0f, -1.0f, 0.0f), viewingDirection);
	}

	void Frame::setPoint(int x, int y, const Point3r &p)
	{
		if (!worldImage)
			generateWorldImage();
		worldImage[y*width + x] = p;
	}

	void Frame::generateWorldImage() const
	{
		TIME_MEASURE_START(generateWorldImage)

		this->worldImage = new Point3r[width * height];
		for (int y = 0; y < height; ++y) {
			real normalizedY = (real)0.5 - (real)y / height;
			for (int x = 0; x < width; ++x) {
				real normalizedX = (real)x / width - (real)0.5;
				depth_t depth = depthAt(x, y);
				if (depth == 0) {
					worldImage[y * width + x] = Point3r::invalidPoint();
				}
				else {
					Point3r newPoint;
					newPoint.x = normalizedX * depth * xzFactor;
					newPoint.y = normalizedY * depth * yzFactor;
					newPoint.z = depth;
					worldImage[y * width + x] = newPoint;
				}
			}
		}

		TIME_MEASURE_STOP(generateWorldImage)
	}

	Frame::~Frame()
	{
		delete[] depthImage;
		delete[] worldImage;
	}

}
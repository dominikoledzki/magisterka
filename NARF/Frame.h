#pragma once
#include <iostream>
#include <fstream>
#include <memory>
#include "DataTypes.h"
#include <opencv2\core\core.hpp>

using namespace std;

namespace NARF
{


	class Frame
	{
	public:
		typedef std::shared_ptr<Frame> Ptr;

		Frame(int width, int height, real xzFactor, real yzFactor, depth_t *depthImage);
		Frame(int width, int height, real xzFactor, real yzFactor, istream &stream);
		~Frame();

		const depth_t &depthAt(int x, int y) const;
		const Point3r &pointAt(int x, int y) const;
		const Point3r pointAt(int x, int y, depth_t depth) const;
		//const PointWithRange pointWithRangeAt(int x, int y) const;
		const Point3r get1DPointAverage(int x, int y, int xDelta, int yDelta, int numberOfPoints) const;
		bool isValid(int x, int y) const;

		Point2i imagePointAt(const Point3r &p) const;

		int getWidth() const;
		int getHeight() const;
		int getSize() const;

		depth_t *getDepthData();
		const Point3r *getWorldData() const;

		cv::Mat get_depth_mat();
		cv::Mat get_grayscale_image();
		cv::Mat get_depth_image();
		cv::Mat get_miniature_image();

		Matrix44r getRotationToViewerCoordinateFrame(const Point3r &point) const;

	private:
		int width;
		int height;

		real xzFactor;
		real yzFactor;

		inline void setPoint(int x, int y, const Point3r &p);
		void generateWorldImage() const;

		depth_t *depthImage;
		mutable Point3r *worldImage;
	};

}
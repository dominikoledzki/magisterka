#include "InterestPointExtractor.h"
#include "BorderExtractor.h"
#include "Utilities.h"
#include "PointAverage.h"
#include "BivariatePolynomial2.h"
#include <array>
#include <vector>

namespace NARF
{
	InterestPointExtractor::InterestPointExtractor(const BorderExtractor &border_extractor) : borderExtractor(border_extractor)
	{
	}

	InterestPointExtractor::~InterestPointExtractor()
	{
		if (borderDirections)
			delete[] borderDirections;
		if (surfaceChangeDirection)
			delete[] surfaceChangeDirection;
		if (surfaceChangeScore)
			delete[] surfaceChangeScore;
		if (interestImage)
			delete[] interestImage;
	}

	void InterestPointExtractor::calculateBorderDirections()
	{
		cout << __FUNCTION__ << endl;
		const Frame &frame = borderExtractor.getFrame();
		int width = frame.getWidth();
		int height = frame.getHeight();

		if (borderDirections == nullptr)
			borderDirections = new Vector3r[width * height];

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int index = y * width + x;
				
				if (borderExtractor.getBorderType(x, y, Direction::Down)	== BorderExtractor::BorderType::ObstacleBorder ||
					borderExtractor.getBorderType(x, y, Direction::Up)		== BorderExtractor::BorderType::ObstacleBorder ||
					borderExtractor.getBorderType(x, y, Direction::Left)	== BorderExtractor::BorderType::ObstacleBorder ||
					borderExtractor.getBorderType(x, y, Direction::Right)	== BorderExtractor::BorderType::ObstacleBorder) {
					calculateBorderDirection(x, y);
				}
			}
		}

		Vector3r *averageBorderDirections = new Vector3r[width * height];
		int radius = pixelRadiusBorderDirections;
		int minimumWeight = radius + 1;
		float minCosAngle = cosf(deg2rad(120.0f));

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int index = y * width + x;

				Vector3r borderDirection = borderDirections[index];
				if (borderDirection.isZero())
					continue;

				Vector3r &averageBorderDirection = averageBorderDirections[index];
				averageBorderDirection = borderDirection;

				real weightSum = 0.0f;

				for (int y2 = std::max(0, y - radius); y2 <= std::min(y + radius, height - 1); ++y2) {
					for (int x2 = std::max(0, x - radius); x2 <= std::min(x + radius, width - 1); ++x2) {
						int index2 = y2 * width + x2;

						if (index == index2)
							continue;

						Vector3r neighBorderDirection = borderDirections[index2];
						if (neighBorderDirection.isZero())
							continue;

						float cosAngle = neighBorderDirection.dotProduct(borderDirection);
						if (cosAngle < minCosAngle)
							continue;

						real borderBetweenPointsScore = getNeighborDistanceChangeScore(borderExtractor.localSurfaceParameters[index], x, y, x2-x, y2-y, 3);
						if (fabsf(borderBetweenPointsScore) >= 0.95f * minimumBorderProbability)
							continue;

						averageBorderDirection = averageBorderDirection + neighBorderDirection;
						weightSum += 1.0f;
					}
				}

				if (weightSum < minimumWeight)
					averageBorderDirection = Vector3r();
				else
					averageBorderDirection.normalize();
			}
		}

		delete[] borderDirections;
		borderDirections = averageBorderDirections;
	}

	void InterestPointExtractor::calculateBorderDirection(int x, int y)
	{
		const Frame &frame = borderExtractor.getFrame();
		int index = y * frame.getWidth() + x;
		Vector3r &border_direction = borderDirections[index];

		int delta_x = 0, delta_y = 0;

		if (borderExtractor.getBorderType(x, y, Direction::Down) == BorderExtractor::BorderType::ObstacleBorder)
			delta_y++;
		if (borderExtractor.getBorderType(x, y, Direction::Up) == BorderExtractor::BorderType::ObstacleBorder)
			delta_y--;
		if (borderExtractor.getBorderType(x, y, Direction::Left) == BorderExtractor::BorderType::ObstacleBorder)
			delta_x--;
		if (borderExtractor.getBorderType(x, y, Direction::Right) == BorderExtractor::BorderType::ObstacleBorder)
			delta_x++;

		if (delta_x == 0 && delta_y == 0)
			return;

		Point3r point = frame.pointAt(x, y);
		Point3r neighbor_point = frame.pointAt(x + delta_x, y + delta_y, static_cast<depth_t>(point.z));

		border_direction = neighbor_point - point;
		border_direction.normalize();
	}

	real InterestPointExtractor::getNeighborDistanceChangeScore(const LocalSurface *localSurface, int x, int y, int xOffset, int yOffset, int pixelRadius)
	{
		Point3r point = borderExtractor.getFrame().pointAt(x, y);
		if (!isValid(point))
			return 1.0f;

		Point3r averagePoint = borderExtractor.getFrame().get1DPointAverage(x, y, xOffset, yOffset, pixelRadius);
		if (!isValid(averagePoint))
			return 1.0f;

		real neighborSquaredDistance = squaredDistance(point, averagePoint);
		if (neighborSquaredDistance <= localSurface->maxNeighborDistanceSquared)
			return 0.0f;

		real result = 1.0f - sqrtf(localSurface->maxNeighborDistanceSquared / neighborSquaredDistance);

		if (averagePoint.z < point.z)
			result = -result;

		return result;
	}

	PrincipalCurvature InterestPointExtractor::mainPrincipalCurvature(int x, int y, int radius)
	{
		const Frame &frame = borderExtractor.getFrame();
		int width = frame.getWidth();
		int height = frame.getHeight();
		int index = y * width + x;

		if (borderExtractor.localSurfaceParameters[index] == nullptr)
			return PrincipalCurvature::invalid();

		PointAverage average;
		bool beamsValid[9];

		for (int step = 1; step <= radius; ++step) {
			int beamIdx = 0;
			for (int y2 = y - step; y2 <= y + step; y2 += step) {
				for (int x2 = x - step; x2 <= x + step; x2 += step) {
					bool &beamValid = beamsValid[beamIdx++];

					if (step == 1) {
						beamValid = x2 != x || y2 != y;	// not center
					}
					else if (!beamValid)
						continue;

					if (!frame.isValid(x2, y2))
						continue;

					int index2 = y2 * width + x2;
					if ((borderExtractor.borderTypeWithDirection[index2] & (unsigned int)BorderExtractor::BorderTypeWithDirection::VeilPointOrShadowBorder) > 0) {
						beamValid = false;
						continue;
					}

					LocalSurface *localSurface2 = borderExtractor.localSurfaceParameters[index2];
					if (localSurface2 == nullptr)
						continue;

					average.addSample(localSurface2->normalNoJumps);
				}
			}
		}
		

		if (average.getNoOfSamples() < 3)
			return PrincipalCurvature::invalid();

		Matrix<real, 3, 1> eigenValues;
		Point3r eigenVector1, eigenVector2, eigenVector3;

		average.pca(eigenValues, eigenVector1, eigenVector2, eigenVector3);
		
		real magnitude = std::sqrt(eigenValues[2]);
		if (std::isnan(magnitude))
			return PrincipalCurvature::invalid();

		return PrincipalCurvature(magnitude, eigenVector3);
	}

	void InterestPointExtractor::calculateSurfaceChanges() {
		cout << __FUNCTION__ << endl;

		const Frame &frame = borderExtractor.getFrame();
		int width = frame.getWidth();
		int height = frame.getHeight();

		if (surfaceChangeDirection == nullptr) {
			surfaceChangeDirection = new Vector3r[width * height];
		}

		if (surfaceChangeScore == nullptr) {
			surfaceChangeScore = new real[width * height];
		}

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int index = y * width + x;

				surfaceChangeDirection[index] = Vector3r();
				surfaceChangeScore[index] = 0.0f;

				if ((borderExtractor.borderTypeWithDirection[y * width + x] & (unsigned int)BorderExtractor::BorderTypeWithDirection::VeilPointOrShadowBorder) > 0)
					continue;

				if (borderDirections[index] != Point3r()) {
					surfaceChangeScore[index] = 1.0f;
					surfaceChangeDirection[index] = borderDirections[index];
				}
				else {
					PrincipalCurvature mainCurvature = mainPrincipalCurvature(x, y, mainPrincipalCurvatureRadius);
					if (std::isnan(mainCurvature.magnitude)) {
						surfaceChangeScore[index] = 0.0f;
					}
					else {
						surfaceChangeDirection[index] = mainCurvature.direction;
						surfaceChangeScore[index] = mainCurvature.magnitude;
					}
				}
			}
		}
	}

	void InterestPointExtractor::calculateInterestImage()
	{
		cout << __FUNCTION__ << endl;

		const Frame &frame = borderExtractor.getFrame();
		int width = frame.getWidth();
		int height = frame.getHeight();
		int size = width * height;
		real radiusSquared = searchRadius * searchRadius;
		real radiusReciprocal = 1.0f / searchRadius;

		interestImage = new real[size];

		const auto &borderTypes = borderExtractor.borderType;
		const auto &borderWithDirection = borderExtractor.borderTypeWithDirection;
		
		const int angleHistogramSize = 18;
		array<real, angleHistogramSize> angleHistogram;
		bool *wasTouched = new bool[size];
		for (int i = 0; i < size; ++i)
			wasTouched[i] = false;

		vector<int> neighborsToCheck;

		for (int y = 0; y < height; ++y) {
			cout << y << endl;

			for (int x = 0; x < width; ++x) {

				int index = y * width + x;

				real &interestValue = interestImage[index];

				interestValue = 0.0f;

				if (!frame.isValid(x, y))
					continue;

				if ((borderWithDirection[index] & (unsigned int)BorderExtractor::BorderTypeWithDirection::VeilPointOrShadowBorder) > 0)
					continue;

				const Point3r &point = frame.pointAt(x, y);
				Matrix44r rotationToViewerCoordinateSystem = frame.getRotationToViewerCoordinateFrame(point);
				
				real negativeScore = 1.0f;
				
				neighborsToCheck.clear();
				neighborsToCheck.push_back(index);
				wasTouched[index] = true;

				angleHistogram.fill(0);
				for (size_t neighborIdx = 0; neighborIdx < neighborsToCheck.size(); ++neighborIdx) {
					int index2 = neighborsToCheck[neighborIdx];
					int y2 = index2 / width;
					int x2 = index2 % width;

					if (!frame.isValid(x2, y2))
						continue;

					if ((borderWithDirection[index2] & (unsigned int)BorderExtractor::BorderTypeWithDirection::VeilPointOrShadowBorder) > 0)
						continue;

					const Point3r &point2 = frame.pointAt(x2, y2);

					real pixelDistance = static_cast<real>(std::max(abs(x2 - x), abs(y2 - y)));
					real distanceSquared = squaredDistance(point, point2);
					 
					if (pixelDistance > 2.0f && distanceSquared > radiusSquared)
						continue;
					
					for (int y3 = y2 - 1; y3 <= y2 + 1; ++y3) {
						if (y3 < 0 || y3 >= height)
							continue;

						for (int x3 = x2 - 1; x3 <= x2 + 1; ++x3) {
							if (x3 < 0 || x3 >= width)
								continue;


							int index3 = y3 * width + x3;
							if (!wasTouched[index3]) {
								wasTouched[index3] = true;
								neighborsToCheck.push_back(index3);
							}
						}
					}

					real pointSurfaceChangeScore = surfaceChangeScore[index2];
					if (pointSurfaceChangeScore < minimumInterestValue)
						continue;

					Vector3r pointSurfaceChangeDirection = surfaceChangeDirection[index2];

					real distance = sqrtf(distanceSquared);
					real distanceFactor = distance * radiusReciprocal;
					real positiveScore;
					real currentNegativeScore;
					nkdGetScores(distanceFactor, pointSurfaceChangeScore, pixelDistance,optimalDistanceToHighSurfaceChange, currentNegativeScore, positiveScore);
					real angle = nkdGetDirectionAngle(pointSurfaceChangeDirection, rotationToViewerCoordinateSystem);

					int histogramCell = static_cast<int>((angle + deg2rad(90.0f)) / deg2rad(180.0f) * angleHistogramSize);
					histogramCell = min(histogramCell, angleHistogramSize - 1);

					angleHistogram[histogramCell] = max(positiveScore, angleHistogram[histogramCell]);
					negativeScore = min(negativeScore, currentNegativeScore);
				}

				// reset was touched for neighbors
				for (size_t neighborIdx = 0; neighborIdx < neighborsToCheck.size(); ++neighborIdx)
					wasTouched[neighborsToCheck[neighborIdx]] = false;

				real angleChangeValue = 0.0f;
				for (int histogramCell1 = 0; histogramCell1 < angleHistogramSize - 1; ++histogramCell1) {
					if (angleHistogram[histogramCell1] == 0.0f)
						continue;

					for (int histogramCell2 = histogramCell1 + 1; histogramCell2 < angleHistogramSize; ++histogramCell2) {
						if (angleHistogram[histogramCell2] == 0.0f)
							continue;

						real normalizedAngleDiff = 2.0f * real(histogramCell2 - histogramCell1) / real(angleHistogramSize);
						normalizedAngleDiff = normalizedAngleDiff <= 1.0f ? normalizedAngleDiff : 2.0f - normalizedAngleDiff;
						angleChangeValue = max(angleHistogram[histogramCell1] * angleHistogram[histogramCell2] * normalizedAngleDiff, angleChangeValue);
					}
				}

				angleChangeValue = sqrtf(angleChangeValue);
				interestValue = negativeScore * angleChangeValue;

				if (addPointsOnStraightEdges) {
					real maxHistogramCellValue = 0.0f;
					for (int histogramCell = 0; histogramCell < angleHistogramSize; ++histogramCell) {
						maxHistogramCellValue = max(maxHistogramCellValue, angleHistogram[histogramCell]);
						interestValue = 0.5f * (interestValue + maxHistogramCellValue);
					}
				}
			}
		}
				
		delete[] wasTouched;
	}

	void InterestPointExtractor::calculateInterestPoints() {
		/*
		real maxDistanceSquared = real(std::pow(borderExtractor.supportingSize * 0.3, 2.0));

		const Frame &frame = borderExtractor.getFrame();
		int width = frame.getWidth();
		int height = frame.getHeight();
		int size = width * height;

		isInterestPoint = new bool[size];
		interestPoints.clear();

		vector<InterestPoint> tmpInterestPoints;
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int index = y * width + x;

				isInterestPoint[index] = false;

				const Point3r &point = frame.pointAt(x, y);

				if (!frame.isValid(x, y))
					continue;

				real interestValue = interestImage[index];
				if (interestValue < minimumInterestValue)
					continue;

				if (doNonMaximumSuppression) {
					bool isMaximum = true;
					for (int y2 = y - 1; y2 <= y + 1; ++y2) {
						for (int x2 = x - 1; x2 <= x + 1; ++x2) {
							int index2 = y2 * width + x2;

							if (!frame.isValid(x2, y2))
								continue;

							if (interestImage[index2] > interestValue) {
								isMaximum = false;
								break;
							}

						}
						if (!isMaximum)
							break;
					}

					if (!isMaximum)
						continue;
				}


				Point3r keypoint = point;
				int xKeypoint = x;
				int yKeypoint = y;

				int numberOfApproximations = numberOfPolynomialApproximations;
				if (!doNonMaximumSuppression)
					numberOfApproximations = 0;

				for (int polyStep = 0; polyStep < numberOfApproximations; ++polyStep) {

					std::vector<Point3r> samplePoints;
					std::vector<bool> invalidBeams;
					std::vector<bool> oldInvalidBeams;

					bool stop = false;
					int radius = 0;

					while (!stop) {
						std::swap(invalidBeams, oldInvalidBeams);
						propagateInvalidBeams(radius, oldInvalidBeams, invalidBeams);
						int x2 = xKeypoint - radius - 1;
						int y2 = yKeypoint - radius;

						stop = true;

						for (int i = 0; i < 8 * radius || (radius == 0 && i == 0); ++i) {
							if (i <= 2 * radius)
								++x2;
							else if (i <= 4 * radius)
								++y2;
							else if (i <= 6 * radius)
								--x2;
							else
								--y2;

							if (invalidBeams[i] || !frame.isValid(x2, y2))
								continue;

							int index2 = y2 * width + x2;
							if ((borderExtractor.borderTypeWithDirection[index2] & (unsigned int)BorderExtractor::BorderTypeWithDirection::VeilPointOrShadowBorder) > 0) {
								invalidBeams[i] = true;
								continue;
							}

							Point3r neighbor = frame.pointAt(x2, y2);
							real distanceSquared = squaredDistance(point, neighbor);
							if (distanceSquared > maxDistanceSquared) {
								invalidBeams[i] = true;
								continue;
							}

							stop = false;

							real interestValue2 = interestImage[index2];
							samplePoints.push_back(Point3r(real(x2 - xKeypoint), real(y2 - yKeypoint), interestValue2));
						}

						++radius;
					}

					// bivariate polynomial approximation
					BivariatePolynomial2<real> polynomial = bivariatePolynomialApproximation<real>(samplePoints);
					if (polynomial.isInvalid())
						continue;

					real xCriticalPoint, yCriticalPoint;
					int type;

					if (!polynomial.criticalPoint(xCriticalPoint, yCriticalPoint, type))
						break;

					if (type != 0)
						break;

					xKeypoint = static_cast<int>(round(xKeypoint + xCriticalPoint));
					yKeypoint = static_cast<int>(round(yKeypoint + yCriticalPoint));

					if (xKeypoint < 0 || xKeypoint >= width || yKeypoint < 0 || yKeypoint >= height)
						break;

					keypoint = frame.pointAt(xKeypoint, yKeypoint);
					if (!frame.isValid(xKeypoint, yKeypoint)) {
						keypoint = point;
						break;
					}
				}

				InterestPoint interestPoint;
				interestPoint.point3d = keypoint;
				interestPoint.point2d = frame.imagePointAt(keypoint);
				interestPoint.interestValue = interestValue;
				tmpInterestPoints.push_back(interestPoint);
			}
			
		}
		
		std::sort(tmpInterestPoints.begin(), tmpInterestPoints.end(), InterestPoint::isBetter);
		
		real minDistanceSquared = powf(minDistanceBetweenInterestPoints * borderExtractor.supportingSize, 2.0f);
		for (int idx1 = 0; idx1 < tmpInterestPoints.size(); ++idx1) {
			if (maxNumberOfInterestPoints > 0 && interestPoints.size() >= maxNumberOfInterestPoints)
				break;

			const InterestPoint &interestPoint = tmpInterestPoints[idx1];

			bool isBetterPointTooClose = false;
			for (int idx2 = 0; idx2 < interestPoints.size(); ++idx2) {
				const InterestPoint &interestPoint2 = interestPoints[idx2];
				real distanceSquared = squaredDistance(interestPoint.point3d, interestPoint2.point3d);
				if (distanceSquared < minDistanceSquared) {
					isBetterPointTooClose = true;
					break;
				}
			}

			if (isBetterPointTooClose)
				continue;

			interestPoints.push_back(interestPoint);
			if (frame.isValid(interestPoint.point2d.x, interestPoint.point2d.y)) {
				int index = interestPoint.point2d.y * width + interestPoint.point2d.x;
				isInterestPoint[index] = true;
			}
		}
		*/
	}

	// helper functions
	void InterestPointExtractor::nkdGetScores(real distanceFactor, real surfaceChangeScore, real pixelDistance, real optimalDistance, real& negativeScore, real& positiveScore)
	{
		negativeScore = 1.0f - 0.5f * surfaceChangeScore * (std::max) (1.0f - distanceFactor / optimalDistance, 0.0f);
		negativeScore = powf(negativeScore, 2);

		if (pixelDistance < 2.0)
			positiveScore = surfaceChangeScore;
		else
			positiveScore = surfaceChangeScore * (1.0f - distanceFactor);
	}


	real InterestPointExtractor::nkdGetDirectionAngle(const Vector3r& direction, const Matrix44r &rotation)
	{
		Matrix41r directionMat = Matrix41r(direction, 1.0f);
		Matrix41r rotatedDirection = matMul(rotation, directionMat);
		Vector2r directionVector(rotatedDirection(0, 0) / rotatedDirection(3, 0), rotatedDirection(1, 0) / rotatedDirection(3, 0));
		directionVector.normalize();
		real angle = 0.5f * normAngle(2.0f * acosf(direction.x));

		return angle;
	}

	void InterestPointExtractor::propagateInvalidBeams(int newRadius, std::vector<bool> &oldBeams, std::vector<bool> &newBeams)
	{
		newBeams.clear();
		newBeams.resize(std::max(1, 8 * newRadius), false);

		if (newRadius >= 2) {
			real mappingFactor = 1.0f + (1.0f / static_cast<real>(newRadius - 1));
			for (int oldIdx = 0; oldIdx < oldBeams.size(); ++oldIdx) {
				if (oldBeams[oldIdx]) {
					int middleIdx = static_cast<int>(round(mappingFactor * oldIdx));
					for (int idxOffset = -1; idxOffset <= 1; ++idxOffset) {
						if (idxOffset != 0) {
							int oldNeighborIdx = oldIdx + idxOffset;
							if (oldNeighborIdx < 0)
								oldNeighborIdx += oldBeams.size();
							if (oldNeighborIdx >= oldBeams.size())
								oldNeighborIdx -= oldBeams.size();
							if (!oldBeams[oldNeighborIdx])
								continue;
						}

						int newIdx = middleIdx + idxOffset;
						if (newIdx < 0)
							newIdx += newBeams.size();
						if (newIdx >= newBeams.size())
							newIdx -= newBeams.size();
						newBeams[newIdx] = true;
					}
				}
			}
		}
	}

}

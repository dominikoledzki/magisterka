#pragma once
#include "DataTypes.h"
#include "BorderExtractor.h"

namespace NARF
{
	/*struct InterestPoint
	{
		Point2i point2d;
		Point3r point3d;
		real interestValue;

		static bool isBetter(const InterestPoint &ip1, const InterestPoint &ip2);
	};*/

	/*struct PrincipalCurvature
	{
		PrincipalCurvature(real magnitude = 0, const Vector3r &direction = Vector3r::zero) : magnitude(magnitude), direction(direction) {}

		real magnitude;
		Vector3r direction;

		static PrincipalCurvature invalid;
	};*/

	class InterestPointExtractor
	{
	public:
		InterestPointExtractor(const BorderExtractor &borderExtractor);
		~InterestPointExtractor();

		int pixelRadiusBorderDirections;
		real minimumBorderProbability;
		int minimumWeight;
		int mainPrincipalCurvatureRadius;
		real minimumInterestValue;
		real searchRadius;
		real optimalDistanceToHighSurfaceChange;
		bool addPointsOnStraightEdges;
		bool doNonMaximumSuppression;
		int numberOfPolynomialApproximations;
		int maxNumberOfInterestPoints;
		real minDistanceBetweenInterestPoints;

		void calculateBorderDirections();
		void calculateSurfaceChanges();
		void calculateInterestImage();
		void calculateInterestPoints();

		real getNeighborDistanceChangeScore(const LocalSurface *localSurface, int x, int y, int xOffset, int yOffset, int pixelRadius);
		PrincipalCurvature mainPrincipalCurvature(int x, int y, int radius);

		void nkdGetScores(real distanceFactor, real surfaceChangeScore, real pixelDistance, real optimalDistance, real& negativeScore, real& positiveScore);
		real nkdGetDirectionAngle(const Vector3r& direction, const Matrix44r &rotation);
		void propagateInvalidBeams(int newRadius, std::vector<bool> &oldBeams, std::vector<bool> &newBeams);

	public:
		const BorderExtractor &borderExtractor;
		Vector3r *borderDirections;
		Vector3r *surfaceChangeDirection;
		real *surfaceChangeScore;
		real *interestImage;
		bool *isInterestPoint;
		std::vector<InterestPoint> interestPoints;

		void calculateBorderDirection(int x, int y);
	};

}
#include "NARF.h"
#include "BorderExtractor.h"
#include "PointAverage.h"
#include "Sort.h"
#include <set>
#include <unordered_set>
#include "BivariatePolynomial2.h"
#include "BivariatePolynomialApprox.h"

namespace NARF
{
	void initialize(NARFContext &context)
	{
		const int &width = context.width;
		const int &height = context.height;

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				for (int i = 0; i < 4; ++i) {
					context.borderType[i][y * width + x] = BorderExtractor::BorderType::NoBorder;
				}
			}
		}
	}

	//===============================================================================================================//

	void depthToWorld(NARFContext &context)
	{
		LOG_FUNCTION_CALL

		const int &width = context.width;
		const int &height = context.height;

		for (int y = 0; y < height; ++y) {
			real normalizedY = (real)0.5 - (real)y / height;
			for (int x = 0; x < width; ++x) {
				real normalizedX = (real)x / width - (real)0.5;
				depth_t depth = context.depth[y * width + x];
				if (depth == 0) {
					context.world[y * width + x] = Point3r::invalidPoint();
				}
				else
				{
					Point3r &newPoint = context.world[y * width + x];
					newPoint.x = normalizedX * depth * context.xzFactor;
					newPoint.y = normalizedY * depth * context.yzFactor;
					newPoint.z = depth;
				}
			}
		}
	}

	//===============================================================================================================//

	void calculateTypical3DDistance(NARFContext &context)
	{
		LOG_FUNCTION_CALL

		const int &width = context.width;
		const int &height = context.height;

		int s = context.supportingSize;
		real nan = numeric_limits<real>::quiet_NaN();

		int M = (s + 1)*(s + 1) / 4;

		

		for (int y = 0; y < s / 2; ++y) {
			for (int x = 0; x < width; ++x) {
				context.typical3dDistance[y*width + x] = nan;
			}
		}

		for (int y = height - s + s / 2; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				context.typical3dDistance[y*width + x] = nan;
			}
		}

		for (int y = s / 2; y < height - s + s / 2; ++y) {
			for (int x = 0; x < s / 2; ++x) {
				context.typical3dDistance[y*width + x] = nan;
			}
			for (int x = width - s + s / 2; x < width; ++x) {
				context.typical3dDistance[y*width + x] = nan;
			}
		}

		real *distances = new real[s*s];
		int distances_size = 0;

		for (int y = s / 2; y < height - s + s / 2; ++y) {
			for (int x = s / 2; x < width - s + s / 2; ++x) {

				bool invalidValue = false;
				Point3r p = context.world[y * width + x];

				if (isValid(p)) {
					for (int j = -s / 2; j < s - s / 2; ++j) {
						for (int i = -s / 2; i < s - s / 2; ++i) {
							int py = y + j;
							int px = x + i;

							Point3r n = context.world[py * width + px];
							if (isValid(n)) {
								real d = sumOfSquares(n - p);
								distances[distances_size++] = d;
							}
							else {
								invalidValue = true;
							}
						}

						if (invalidValue) {
							break;
						}
					}
				}
				else {
					invalidValue = true;
				}

				if (!invalidValue) {

					// small sort
					real minVal, val;
					int minIdx, i1, i2;
					for (i1 = 0; i1 < M; ++i1) {
						minVal = distances[i1];
						minIdx = i1;

						for (i2 = i1 + 1; i2 < s*s; ++i2) {
							val = distances[i2];
							if (val < minVal) {
								minVal = val;
								minIdx = i2;
							}
						}
						distances[minIdx] = distances[i1];
						distances[i1] = minVal;
					}

					real dist = std::sqrt(distances[M-1]);
					context.typical3dDistance[y*width + x] = dist;
				}
				else {
					context.typical3dDistance[y*width + x] = nan;
				}

				distances_size = 0;
			}
		}

		delete[] distances;
	}

	//===============================================================================================================//

	void calculateBorderScore(NARFContext &context)
	{
		LOG_FUNCTION_CALL

		const int &width = context.width;
		const int &height = context.height;

		real nan = numeric_limits<real>::quiet_NaN();
		int mp = context.pointsUsedForAverage;

		std::vector<Direction> directions = { Direction::Up, Direction::Right, Direction::Down, Direction::Left };
		std::map<Direction, std::pair<int, int> > directionPoints = {
			{ Direction::Up, std::make_pair(0, 1) },
			{ Direction::Right, std::make_pair(1, 0) },
			{ Direction::Down, std::make_pair(0, -1) },
			{ Direction::Left, std::make_pair(-1, 0) }
		};

		for (auto it = directions.begin(); it != directions.end(); ++it) {
			Direction dir = *it;
			int dirIdx = static_cast<int>(dir);

			int xOffset = directionPoints[dir].first;
			int yOffset = directionPoints[dir].second;

			real *dirBorderScore = context.borderScore[dirIdx];
			real *dirAvgNeighborRange = context.avgNeighborRange[dirIdx];

			//for each point
			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; ++x) {

					if (x + mp * xOffset < 0 ||
						x + mp * xOffset >= width ||
						y + mp * yOffset < 0 ||
						y + mp * yOffset >= height) {
						dirBorderScore[y * width + x] = nan;
						dirAvgNeighborRange[y*width + x] = nan;
						continue;
					}

					Point3r p = context.world[y * width + x];
					Point3r avgNeighborPosition;
					for (int i = 1; i <= mp; ++i) {
						int nx = x + i * xOffset;
						int ny = y + i * yOffset;
						Point3r neighbour = context.world[ny * width + nx];
						avgNeighborPosition = avgNeighborPosition + neighbour;
					}
					avgNeighborPosition = avgNeighborPosition * (1 / (real)mp);
					real avgNeighborRange = avgNeighborPosition.z;
					dirAvgNeighborRange[y * width + x] = avgNeighborRange;

					real avgDistance = std::sqrt(sumOfSquares(p - avgNeighborPosition));
					real typical3DDistance = context.typical3dDistance[y * width + x];
					real score = std::max(0.0f, 1 - typical3DDistance / avgDistance);
					dirBorderScore[y * width + x] = score;
				}
			}
		}
	}

	//===============================================================================================================//

	void smoothBorderScore(NARFContext &context)
	{
		LOG_FUNCTION_CALL

		const int &width = context.width;
		const int &height = context.height;

		real nan = numeric_limits<real>::quiet_NaN();
		int radius = context.smoothingRadius;

		for (int dir = 0; dir < 4; ++dir) {
			memcpy(context.smoothBorderScore[dir], context.borderScore[dir], sizeof(real) * width * height);

			real *dirSmoothedBorderScore = context.smoothBorderScore[dir];
			real *dirBorderScore = context.borderScore[dir];

			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; ++x) {

					real sum = 0.0f;
					int pointsCount = 0;

					for (int yOffset = -radius; yOffset <= radius; ++yOffset) {
						for (int xOffset = -radius; xOffset <= radius; ++xOffset) {

							int py = y + yOffset;
							int px = x + xOffset;

							if (px < 0 || px >= width || py < 0 || py >= height)
								continue;

							real pval = dirBorderScore[py * width + px];
							if (isnan(pval))
								continue;

							sum += pval;
							++pointsCount;
						}
					}

					if (pointsCount > 0) {
						real avg = sum / pointsCount;
						dirSmoothedBorderScore[y * width + x] = avg;
					}
				}
			}
		}
	}

	//===============================================================================================================//

	void firstClassification(NARFContext &context)
	{
		LOG_FUNCTION_CALL

		const int &width = context.width;
		const int &height = context.height;

		for (int dir = 0; dir < 4; ++dir) {
			BorderExtractor::PotentialBorderType *dirBorderType = context.potentialBorderType[dir];

			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; ++x) {
					real pRange = context.depth[y * width + x];
					real pNeighRange = context.avgNeighborRange[dir][y * width + x];

					if (isnan(pRange + pNeighRange))
						dirBorderType[y * width + x] = BorderExtractor::PotentialBorderType::Unknown;
					else
						dirBorderType[y * width + x] = pRange < pNeighRange ?
						BorderExtractor::PotentialBorderType::ObstacleBorder :
						BorderExtractor::PotentialBorderType::ShadowBorder;
				}
			}
		}
	}

	//===============================================================================================================//

	void setBorderType(BorderExtractor::BorderType *dirBorderType, int x, int y, int width, BorderExtractor::BorderType newVal)
	{
		BorderExtractor::BorderType curVal = dirBorderType[y * width + x];
		
		switch (curVal)
		{
		case NARF::BorderExtractor::BorderType::NoBorder:
			break;

		case NARF::BorderExtractor::BorderType::ObstacleBorder:
			newVal = BorderExtractor::BorderType::ObstacleBorder;
			break;

		case NARF::BorderExtractor::BorderType::ShadowBorder:
			if (newVal == BorderExtractor::BorderType::NoBorder || newVal == BorderExtractor::BorderType::VeilPoint)
				newVal = BorderExtractor::BorderType::ShadowBorder;
			break;

		case NARF::BorderExtractor::BorderType::VeilPoint:
			if (newVal == BorderExtractor::BorderType::NoBorder)
				newVal = BorderExtractor::BorderType::VeilPoint;
			break;
		}

		dirBorderType[y * width + x] = newVal;
	}

	void secondClassification(NARFContext &context)
	{
		LOG_FUNCTION_CALL

		const int &width = context.width;
		const int &height = context.height;

		real nan = numeric_limits<real>::quiet_NaN();
		int searchingDistance = context.reduceBorderSearchingDistance;

		std::vector<Direction> directions = { Direction::Up, Direction::Right, Direction::Down, Direction::Left };
		std::map<Direction, std::pair<int, int> > directionPoints = {
			{ Direction::Up, std::make_pair(0, 1) },
			{ Direction::Right, std::make_pair(1, 0) },
			{ Direction::Down, std::make_pair(0, -1) },
			{ Direction::Left, std::make_pair(-1, 0) }
		};

		for (auto it = directions.begin(); it != directions.end(); ++it) {
			Direction dir = *it;
			int dirIdx = static_cast<int>(dir);

			int xOffset = directionPoints[dir].first;
			int yOffset = directionPoints[dir].second;

			BorderExtractor::PotentialBorderType *dirPotentialBorderType = context.potentialBorderType[dirIdx];
			
			real *dirBorderScore = context.smoothBorderScore[dirIdx];
			memcpy(context.secondBorderScore[dirIdx], context.smoothBorderScore[dirIdx], sizeof(real) * width * height);
			real *newDirBorderScore = context.secondBorderScore[dirIdx];

			BorderExtractor::BorderType *dirBorderType = context.borderType[dirIdx];

			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; ++x) {

					BorderExtractor::PotentialBorderType type = dirPotentialBorderType[y * width + x];
					if (type != BorderExtractor::PotentialBorderType::ObstacleBorder)
						continue;

					real thisScore = dirBorderScore[y * width + x];

					// looking for shadow border on the direction
					real maxScore = nan;
					int maxIndex = 0;
					for (int i = 1; i <= searchingDistance; ++i) {
						int py = y + i * yOffset;
						int px = x + i * xOffset;

						if (py < 0 || py >= height || px < 0 || px >= width)
							continue;

						real pScore = dirBorderScore[py * width + px];
						if (isnan(maxScore) || (!isnan(pScore) && pScore > maxScore)) {
							maxScore = pScore;
							maxIndex = i;
						}
					}

					if (!isnan(maxScore)) {
						real oneMinusSSCubed = (1 - maxScore)*(1 - maxScore)*(1 - maxScore);
						real newScore = max((real)0.9, 1 - oneMinusSSCubed) * thisScore;
						newDirBorderScore[y * width + x] = newScore;

						if (newScore > context.borderScoreThreshold) {
							bool isMax = true;

							int x1 = x - xOffset;
							int y1 = y - yOffset;
							int x2 = x + xOffset;
							int y2 = y + yOffset;

							if (x1 >= 0 && x1 < width && y1 >= 0 && y1 < height && dirBorderScore[y1 * width + x1] > newScore)
								isMax = false;

							if (x2 >= 0 && x2 < width && y2 >= 0 && y2 < height && dirBorderScore[y2 * width + x2] > newScore)
								isMax = false;

							if (isMax) {
								setBorderType(dirBorderType, x, y, width, BorderExtractor::BorderType::ObstacleBorder);
								//dirBorderType[y * width + x] = BorderExtractor::BorderType::ObstacleBorder;

								setBorderType(dirBorderType, x + maxIndex * xOffset, (y + maxIndex * yOffset), width, BorderExtractor::BorderType::ShadowBorder);
								//dirBorderType[(y + maxIndex * yOffset) * width + x + maxIndex * xOffset] = BorderExtractor::BorderType::ShadowBorder;

								for (int i = 1; i < maxIndex; ++i) {
									setBorderType(dirBorderType, x + i * xOffset, (y + i * yOffset), width, BorderExtractor::BorderType::VeilPoint);
									//dirBorderType[(y + i * yOffset) * width + x + i * xOffset] = BorderExtractor::BorderType::VeilPoint;
								}
							}
						}
					}
				}
			}
		}
	}

	//===============================================================================================================//

	void cumulateBorderType(NARFContext &context)
	{
		LOG_FUNCTION_CALL

		const int &width = context.width;
		const int &height = context.height;

		BorderExtractor::BorderType *upBorderType = context.borderType[(int)Direction::Up];
		BorderExtractor::BorderType *rightBorderType = context.borderType[(int)Direction::Right];
		BorderExtractor::BorderType *downBorderType = context.borderType[(int)Direction::Down];
		BorderExtractor::BorderType *leftBorderType = context.borderType[(int)Direction::Left];

		unsigned int *borderTypeWithDirection = context.borderTypeWithDirection;

		for (int idx = 0; idx < width * height; ++idx) {
			unsigned int &pixel = borderTypeWithDirection[idx];

			pixel = (unsigned int)BorderExtractor::BorderTypeWithDirection::NoBorder;

			if (upBorderType[idx] == BorderExtractor::BorderType::ObstacleBorder)
				pixel |= (unsigned int)BorderExtractor::BorderTypeWithDirection::ObstacleBorderUp;

			if (rightBorderType[idx] == BorderExtractor::BorderType::ObstacleBorder)
				pixel |= (unsigned int)BorderExtractor::BorderTypeWithDirection::ObstacleBorderRight;

			if (downBorderType[idx] == BorderExtractor::BorderType::ObstacleBorder)
				pixel |= (unsigned int)BorderExtractor::BorderTypeWithDirection::ObstacleBorderDown;

			if (leftBorderType[idx] == BorderExtractor::BorderType::ObstacleBorder)
				pixel |= (unsigned int)BorderExtractor::BorderTypeWithDirection::ObstacleBorderLeft;


			if (upBorderType[idx] == BorderExtractor::BorderType::ShadowBorder)
				pixel |= (unsigned int)BorderExtractor::BorderTypeWithDirection::ShadowBorderUp;

			if (rightBorderType[idx] == BorderExtractor::BorderType::ShadowBorder)
				pixel |= (unsigned int)BorderExtractor::BorderTypeWithDirection::ShadowBorderRight;

			if (downBorderType[idx] == BorderExtractor::BorderType::ShadowBorder)
				pixel |= (unsigned int)BorderExtractor::BorderTypeWithDirection::ShadowBorderDown;

			if (leftBorderType[idx] == BorderExtractor::BorderType::ShadowBorder)
				pixel |= (unsigned int)BorderExtractor::BorderTypeWithDirection::ShadowBorderLeft;


			if (upBorderType[idx] == BorderExtractor::BorderType::VeilPoint)
				pixel |= (unsigned int)BorderExtractor::BorderTypeWithDirection::VeilPointUp;

			if (rightBorderType[idx] == BorderExtractor::BorderType::VeilPoint)
				pixel |= (unsigned int)BorderExtractor::BorderTypeWithDirection::VeilPointRight;

			if (downBorderType[idx] == BorderExtractor::BorderType::VeilPoint)
				pixel |= (unsigned int)BorderExtractor::BorderTypeWithDirection::VeilPointDown;

			if (leftBorderType[idx] == BorderExtractor::BorderType::VeilPoint)
				pixel |= (unsigned int)BorderExtractor::BorderTypeWithDirection::VeilPointLeft;
		}
	}

	//===============================================================================================================//

	void calculateLocalSurfaceParametersAtPoint(NARFContext &context, int x, int y)
	{
		const int &width = context.width;
		const int &height = context.height;

		int index = y * width + x;

		LocalSurface &localSurface = context.localSurface[index];

		if (context.depth[index] == 0) {
			localSurface.valid = false;
			return;
		}

		localSurface.normal = Vector3r();
		localSurface.neighborhoodMean = Vector3r();
		localSurface.eigenValues = Vector3r();
		localSurface.normalNoJumps = Vector3r();
		localSurface.nighborhoodMeanNoJumps = Vector3r();
		localSurface.eigenValuesNoJumps = Vector3r();
		localSurface.maxNeighborDistanceSquared = 0.0f;

		Point3r point = context.world[y * width + x];

		int radius = context.localSurfaceRadius;
		int noOfClosestNeighbors = context.localSurfaceNumerOfClosestNeighbors;
		int blockSize = (2 * radius + 1) * (2 * radius + 1);

		int neighborCount = 0;
		NeighborWithDistance *neighbors = new NeighborWithDistance[blockSize];

		for (int y2 = y - radius; y2 <= y + radius; ++y2) {
			for (int x2 = x - radius; x2 <= x + radius; ++x2) {
				if (x2 < 0 || y2 < 0 || x2 >= width || y2 >= height || context.depth[y2 * width + x2] == 0)
					continue;

				NeighborWithDistance &neighbor = neighbors[neighborCount];
				neighbor.point = context.world[y2 * width + x2];
				neighbor.distance = squaredDistance(point, neighbor.point);
				++neighborCount;
			}
		}

		noOfClosestNeighbors = std::min(neighborCount, noOfClosestNeighbors);
		//std::sort(neighbors, neighbors + neighborCount);
		insertionSort(neighbors, neighborCount);

		localSurface.maxNeighborDistanceSquared = neighbors[noOfClosestNeighbors - 1].distance;

		real maxDistanceSquared = 4.0f * localSurface.maxNeighborDistanceSquared;

		PointAverage pointAverage;

		int neighborIdx;
		for (neighborIdx = 0; neighborIdx < neighborCount; ++neighborIdx) {
			if (neighbors[neighborIdx].distance > maxDistanceSquared)
				break;

			pointAverage.addSample(neighbors[neighborIdx].point);
		}

		if (pointAverage.getNoOfSamples() < 3) {
			localSurface.valid = false;
			return;
		}

		Vector3r eigenVector2, eigenVector3;
		pointAverage.pca(localSurface.eigenValues, localSurface.normal, eigenVector2, eigenVector3);

		Vector3r viewingDirection(-point.x, -point.y, -point.z);
		if (localSurface.normal.dotProduct(viewingDirection) < 0.0f)
			localSurface.normal = localSurface.normal * (-1.0f);

		localSurface.neighborhoodMean = pointAverage.getMean();

		// add left neightbors
		for (; neighborIdx < neighborCount; ++neighborIdx) {
			pointAverage.addSample(neighbors[neighborIdx].point);
		}

		pointAverage.pca(localSurface.eigenValuesNoJumps, localSurface.normalNoJumps, eigenVector2, eigenVector3);
		if (localSurface.normalNoJumps.dotProduct(viewingDirection) < 0.0f)
			localSurface.normalNoJumps = localSurface.normalNoJumps * (-1.0f);
		localSurface.nighborhoodMeanNoJumps = pointAverage.getMean();

		delete[] neighbors;

		localSurface.valid = true;
	}

	void calculateLocalSurfaceParameters(NARFContext &context)
	{
		LOG_FUNCTION_CALL

		for (int y = 0; y < context.height; ++y) {
			for (int x = 0; x < context.width; ++x) {
				calculateLocalSurfaceParametersAtPoint(context, x, y);
			}
		}
	}

	//===============================================================================================================//

	// INTEREST POINTS EXTRACTION
	Point3r pointAt(NARFContext &context, int x, int y, depth_t depth)
	{
		real normalizedY = (real)0.5 - (real)y / context.width;
		real normalizedX = (real)x / context.height - (real)0.5;
		Point3r newPoint;
		newPoint.x = normalizedX * depth * context.xzFactor;
		newPoint.y = normalizedY * depth * context.yzFactor;
		newPoint.z = depth;
		return newPoint;
	}

	void calculateBorderDirection(NARFContext &context, int x, int y)
	{
		const int width = context.width;
		const int height = context.height;

		const int index = y * width + x;

		Vector3r &borderDirection = context.borderDirections[index];

		int delta_x = 0, delta_y = 0;

		if (context.borderTypeWithDirection[index] & (unsigned int)BorderExtractor::BorderTypeWithDirection::ObstacleBorderDown)
			delta_y++;

		if (context.borderTypeWithDirection[index] & (unsigned int)BorderExtractor::BorderTypeWithDirection::ObstacleBorderUp)
			delta_y--;

		if (context.borderTypeWithDirection[index] & (unsigned int)BorderExtractor::BorderTypeWithDirection::ObstacleBorderLeft)
			delta_x--;

		if (context.borderTypeWithDirection[index] & (unsigned int)BorderExtractor::BorderTypeWithDirection::ObstacleBorderRight)
			delta_x++;

		if (delta_x == 0 && delta_y == 0) {
			borderDirection = Vector3r();
			return;
		}

		Point3r point = context.world[index];
		Point3r neighbor_point = pointAt(context, x + delta_x, y + delta_y, static_cast<depth_t>(point.z));

		borderDirection = neighbor_point - point;
		borderDirection.normalize();
	}

	void calculateBorderDirections(NARFContext &context)
	{
		LOG_FUNCTION_CALL
		const int &width = context.width;
		const int &height = context.height;

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int index = y * width + x;

				if (context.borderTypeWithDirection[index] & (unsigned int)BorderExtractor::BorderTypeWithDirection::ObstacleBorder)
				{
					calculateBorderDirection(context, x, y);
				}
				else
				{
					context.borderDirections[index] = Vector3r();
				}
			}
		}
	}

	//===============================================================================================================//

	Point3r get1DPointAverage(NARFContext &context, int x, int y, int xDelta, int yDelta, int numberOfPoints)
	{
		const int &width = context.width;
		const int &height = context.height;

		Point3r averagePoint;
		float sumWeight = 0.0f;

		int x2, y2;
		for (int step = 0; step < numberOfPoints; ++step) {
			x2 = x + step * xDelta;
			y2 = y + step * yDelta;

			if (x2 < 0 || y2 < 0 || x2 >= width || y2 >= height || context.depth[y2 * width + x2] == 0)
				continue;

			Point3r neighbor = context.world[y2 * width + x2];
			averagePoint += neighbor;
			sumWeight += 1.0f;
		}

		if (sumWeight == 0.0f)
			return Point3r::invalidPoint();

		averagePoint /= sumWeight;
		return averagePoint;
	}

	real getNeighborDistanceChangeScore(NARFContext &context, int x, int y, int xOffset, int yOffset, int pixelRadius)
	{
		const int &width = context.width;

		Point3r point = context.world[y * width + x];
		if (!isValid(point))
			return 1.0f;

		Point3r averagePoint = get1DPointAverage(context, x, y, xOffset, yOffset, pixelRadius);
		if (!isValid(averagePoint))
			return 1.0f;

		const LocalSurface &localSurface = context.localSurface[y * width + x];

		real neighborSquaredDistance = squaredDistance(point, averagePoint);
		if (neighborSquaredDistance <= localSurface.maxNeighborDistanceSquared)
			return 0.0f;

		real result = 1.0f - sqrtf(localSurface.maxNeighborDistanceSquared / neighborSquaredDistance);

		if (averagePoint.z < point.z)
			result = -result;

		return result;
	}

	void calculateAverageBorderDirections(NARFContext &context)
	{
		LOG_FUNCTION_CALL
		const int &width = context.width;
		const int &height = context.height;

		int radius = context.pixelRadiusBorderDirections;
		int minimumWeight = radius + 1;
		float minCosAngle = cosf(deg2rad(120.0f));

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int index = y * width + x;

				context.averageBorderDirections[index] = Vector3r();

				Vector3r borderDirection = context.borderDirections[index];
				if (borderDirection.isZero())
					continue;

				Vector3r averageBorderDirection = borderDirection;

				real weightSum = 0.0f;

				for (int y2 = MAX(0, y - radius); y2 <= MIN(y + radius, height - 1); ++y2) {
					for (int x2 = MAX(0, x - radius); x2 <= MIN(x + radius, width - 1); ++x2) {
						int index2 = y2 * width + x2;

						if (index == index2)
							continue;

						Vector3r neighBorderDirection = context.borderDirections[index2];
						if (neighBorderDirection.isZero())
							continue;

						float cosAngle = neighBorderDirection.dotProduct(borderDirection);
						if (cosAngle < minCosAngle)
							continue;

						real borderBetweenPointsScore = getNeighborDistanceChangeScore(context, x, y, x2-x, y2-y, 3);
						if (fabsf(borderBetweenPointsScore) >= 0.95f * context.minimumBorderProbability)
							continue;

						averageBorderDirection = averageBorderDirection + neighBorderDirection;
						weightSum += 1.0f;
					}
				}

				if (weightSum < minimumWeight)
					averageBorderDirection = Vector3r();
				else
					averageBorderDirection.normalize();

				context.averageBorderDirections[index] = averageBorderDirection;
			}
		}
	}

	//===============================================================================================================//

	PrincipalCurvature mainPrincipalCurvature(NARFContext &context, int x, int y)
	{
		const int &width = context.width;
		const int &height = context.height;
		const int index = y * width + x;
		const int radius = context.mainPrincipalCurvatureRadius;

		if (!context.localSurface[index].valid)
			return PrincipalCurvature::invalid();

		PointAverage average;
		bool beamsValid[9];

		for (int step = 1; step <= radius; ++step) {
			int beamIdx = 0;
			for (int y2 = y - step; y2 <= y + step; y2 += step) {
				for (int x2 = x - step; x2 <= x + step; x2 += step) {
					bool &beamValid = beamsValid[beamIdx++];

					if (step == 1) {
						beamValid = x2 != x || y2 != y;	// not center
					}
					else if (!beamValid)
						continue;

					if (x2 < 0 || x2 >= width || y2 < 0 || y2 >= height)
						continue;

					if (context.depth[y2 * width + x2] == 0)
						continue;

					int index2 = y2 * width + x2;
					if ((context.borderTypeWithDirection[index2] & (unsigned int)BorderExtractor::BorderTypeWithDirection::VeilPointOrShadowBorder) > 0) {
						beamValid = false;
						continue;
					}

					const LocalSurface &localSurface2 = context.localSurface[index2];
					if (!localSurface2.valid)
						continue;


					average.addSample(localSurface2.normalNoJumps);
				}
			}
		}


		if (average.getNoOfSamples() < 3)
			return PrincipalCurvature::invalid();

		Matrix<real, 3, 1> eigenValues;
		Point3r eigenVector1, eigenVector2, eigenVector3;

		average.pca(eigenValues, eigenVector1, eigenVector2, eigenVector3);

		real magnitude = sqrt(eigenValues[2]);
		if (isnan(magnitude))
			return PrincipalCurvature::invalid();

		return PrincipalCurvature(magnitude, eigenVector3);
	}

	void calculateSurfaceChanges(NARFContext &context)
	{
		LOG_FUNCTION_CALL
		const int &width = context.width;
		const int &height = context.height;

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int index = y * width + x;

				context.surfaceChangeDirection[index] = Vector3r();
				context.surfaceChangeScore[index] = 0.0f;

				if ((context.borderTypeWithDirection[y * width + x] & (unsigned int)BorderExtractor::BorderTypeWithDirection::VeilPointOrShadowBorder) > 0)
					continue;

				if (context.borderDirections[index] != Point3r()) {
					context.surfaceChangeScore[index] = 1.0f;
					context.surfaceChangeDirection[index] = context.borderDirections[index];
				}
				else {
					PrincipalCurvature mainCurvature = mainPrincipalCurvature(context, x, y);
					if (isnan(mainCurvature.magnitude)) {
						context.surfaceChangeScore[index] = 0.0f;
					}
					else {
						context.surfaceChangeDirection[index] = mainCurvature.direction;
						context.surfaceChangeScore[index] = mainCurvature.magnitude;
					}
				}
			}
		}
	}

	//===============================================================================================================//

	Matrix44r getRotationToViewerCoordinateFrame(const Point3r &point)
	{
		Vector3r viewingDirection = point.normalized();
		return getTransformFromTwoUnitVectors(Vector3r(0.0f, -1.0f, 0.0f), viewingDirection);
	}

	void nkdGetScores(real distanceFactor, real surfaceChangeScore, real pixelDistance, real optimalDistance, real& negativeScore, real& positiveScore)
	{
		negativeScore = 1.0f - 0.5f * surfaceChangeScore * (std::max) (1.0f - distanceFactor / optimalDistance, 0.0f);
		negativeScore = powf(negativeScore, 2);

		if (pixelDistance < 2.0)
			positiveScore = surfaceChangeScore;
		else
			positiveScore = surfaceChangeScore * (1.0f - distanceFactor);
	}

	real nkdGetDirectionAngle(const Vector3r& direction, const Matrix44r &rotation)
	{
		Matrix41r directionMat = Matrix41r(direction, 1.0f);
		Matrix41r rotatedDirection = matMul(rotation, directionMat);
		Vector2r directionVector(rotatedDirection(0, 0) / rotatedDirection(3, 0), rotatedDirection(1, 0) / rotatedDirection(3, 0));
		directionVector.normalize();
		real angle = 0.5f * normAngle(2.0f * acosf(directionVector.x));

		return angle;
	}

	void calculateInterestImage(NARFContext &context)
	{
		LOG_FUNCTION_CALL
		
		const int &width = context.width;
		const int &height = context.height;
		const int size = width * height;

		real radiusSquared = context.searchRadius * context.searchRadius;
		real radiusReciprocal = 1.0f / context.searchRadius;

		const auto &borderTypes = context.borderType;
		const auto &borderWithDirection = context.borderTypeWithDirection;

		vector<int> neighborsToCheck;
		vector<int> touchedPoints;

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {

				const int angleHistogramSize = 18;
				real angleHistogram[angleHistogramSize] = { 0 };

				int index = y * width + x;

				real &interestValue = context.interestImage[index];

				interestValue = 0.0f;

				if (context.depth[index] == 0)
					continue;

				if ((borderWithDirection[index] & (unsigned int)BorderExtractor::BorderTypeWithDirection::VeilPointOrShadowBorder) > 0)
					continue;

				const Point3r &point = context.world[index];
				if (!isValid(point))
					continue;

				Matrix44r rotationToViewerCoordinateSystem = getRotationToViewerCoordinateFrame(point);

				real negativeScore = 1.0f;

				neighborsToCheck.clear();
				neighborsToCheck.push_back(index);
				touchedPoints.push_back(index);

				fillArray(angleHistogram, angleHistogramSize, 0.0f);

				for (size_t neighborIdx = 0; neighborIdx < neighborsToCheck.size(); ++neighborIdx) {
					int index2 = neighborsToCheck[neighborIdx];
					int y2 = index2 / width;
					int x2 = index2 % width;

					if (x2 < 0 || x2 >= width || y2 < 0 || y2 >= height || context.depth[index2] == 0)
						continue;

					const Point3r &point2 = context.world[y2 * width + x2];

					if (!isValid(point2))
						continue;

					if ((borderWithDirection[index2] & (unsigned int)BorderExtractor::BorderTypeWithDirection::VeilPointOrShadowBorder) > 0)
						continue;

					real pixelDistance = static_cast<real>(max(abs(x2 - x), abs(y2 - y)));
					real distanceSquared = squaredDistance(point, point2);

					if (pixelDistance > 2.0f && distanceSquared > radiusSquared)
						continue;

					for (int y3 = y2 - 1; y3 <= y2 + 1; ++y3) {
						if (y3 < 0 || y3 >= height)
							continue;

						for (int x3 = x2 - 1; x3 <= x2 + 1; ++x3) {
							if (x3 < 0 || x3 >= width)
								continue;


							int index3 = y3 * width + x3;
							if (find(touchedPoints.begin(), touchedPoints.end(), index3) == touchedPoints.end()) {
								touchedPoints.push_back(index3);
								neighborsToCheck.push_back(index3);
							}
						}
					}

					real pointSurfaceChangeScore = context.surfaceChangeScore[index2];
					if (pointSurfaceChangeScore < context.minimumInterestValue)
						continue;

					Vector3r pointSurfaceChangeDirection = context.surfaceChangeDirection[index2];

					real distance = sqrtf(distanceSquared);
					real distanceFactor = distance * radiusReciprocal;
					real positiveScore;
					real currentNegativeScore;
					nkdGetScores(distanceFactor, pointSurfaceChangeScore, pixelDistance, context.optimalDistanceToHighSurfaceChange, currentNegativeScore, positiveScore);
					real angle = nkdGetDirectionAngle(pointSurfaceChangeDirection, rotationToViewerCoordinateSystem);

					int histogramCell = static_cast<int>((angle + deg2rad(90.0f)) / deg2rad(180.0f) * angleHistogramSize);
					histogramCell = min(histogramCell, angleHistogramSize - 1);

					angleHistogram[histogramCell] = max(positiveScore, angleHistogram[histogramCell]);
					negativeScore = min(negativeScore, currentNegativeScore);
				}

				// reset was touched for neighbors
				touchedPoints.clear();

				real angleChangeValue = 0.0f;
				for (int histogramCell1 = 0; histogramCell1 < angleHistogramSize - 1; ++histogramCell1) {
					if (angleHistogram[histogramCell1] == 0.0f)
						continue;

					for (int histogramCell2 = histogramCell1 + 1; histogramCell2 < angleHistogramSize; ++histogramCell2) {
						if (angleHistogram[histogramCell2] == 0.0f)
							continue;

						real normalizedAngleDiff = 2.0f * real(histogramCell2 - histogramCell1) / real(angleHistogramSize);
						normalizedAngleDiff = normalizedAngleDiff <= 1.0f ? normalizedAngleDiff : 2.0f - normalizedAngleDiff;
						angleChangeValue = max(angleHistogram[histogramCell1] * angleHistogram[histogramCell2] * normalizedAngleDiff, angleChangeValue);
					}
				}

				angleChangeValue = sqrtf(angleChangeValue);
				interestValue = negativeScore * angleChangeValue;

				if (context.addPointsOnStraightEdges) {
					real maxHistogramCellValue = 0.0f;
					for (int histogramCell = 0; histogramCell < angleHistogramSize; ++histogramCell) {
						maxHistogramCellValue = max(maxHistogramCellValue, angleHistogram[histogramCell]);
						interestValue = 0.5f * (interestValue + maxHistogramCellValue);
					}
				}
			}
		}
	}

	//===============================================================================================================//

	void propagateInvalidBeams(int newRadius, std::vector<bool> &oldBeams, std::vector<bool> &newBeams)
	{
		newBeams.clear();
		newBeams.resize(std::max(1, 8 * newRadius), false);

		if (newRadius >= 2) {
			real mappingFactor = 1.0f + (1.0f / static_cast<real>(newRadius - 1));
			for (int oldIdx = 0; oldIdx < oldBeams.size(); ++oldIdx) {
				if (oldBeams[oldIdx]) {
					int middleIdx = static_cast<int>(round(mappingFactor * oldIdx));
					for (int idxOffset = -1; idxOffset <= 1; ++idxOffset) {
						if (idxOffset != 0) {
							int oldNeighborIdx = oldIdx + idxOffset;
							if (oldNeighborIdx < 0)
								oldNeighborIdx += oldBeams.size();
							if (oldNeighborIdx >= oldBeams.size())
								oldNeighborIdx -= oldBeams.size();
							if (!oldBeams[oldNeighborIdx])
								continue;
						}

						int newIdx = middleIdx + idxOffset;
						if (newIdx < 0)
							newIdx += newBeams.size();
						if (newIdx >= newBeams.size())
							newIdx -= newBeams.size();
						newBeams[newIdx] = true;
					}
				}
			}
		}
	}

	Point2i imagePointAt(NARFContext &context, const Point3r &p)
	{
		Point2i d;

		real normalizedX = p.x / (p.z * context.xzFactor);
		real normalizedY = p.y / (p.z * context.yzFactor);

		d.x = static_cast<int>(context.width * (normalizedX + 0.5f));
		d.y = static_cast<int>(context.height * (0.5f - normalizedY));

		return d;
	}

	void calculateInterestPoints(NARFContext &context)
	{
		real maxDistanceSquared = real(pow(context.supportingSize * 0.3, 2.0));

		const int width = context.width;
		const int height = context.height;
		const int size = width * height;

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int index = y * width + x;

				context.isInterestPoint[index] = false;

				const Point3r &point = context.world[index];

				if (!isValid(point))
					continue;

				real interestValue = context.interestImage[index];
				if (interestValue < context.minimumInterestValue)
					continue;

				if (context.doNonMaximumSuppression) {
					bool isMaximum = true;
					for (int y2 = y - 1; y2 <= y + 1; ++y2) {
						for (int x2 = x - 1; x2 <= x + 1; ++x2) {
							int index2 = y2 * width + x2;

							if (x2 < 0 || x2 >= width || y2 < 0 || y2 >= height)
								continue;

							const Point3r &p2 = context.world[index2];

							if (!isValid(p2))
								continue;

							if (context.interestImage[index2] > interestValue) {
								isMaximum = false;
								break;
							}

						}
						if (!isMaximum)
							break;
					}

					if (!isMaximum)
						continue;
				}

				Point3r keypoint = point;
				int xKeypoint = x;
				int yKeypoint = y;

				int numberOfApproximations = context.numberOfPolynomialApproximations;
				if (!context.doNonMaximumSuppression)
					numberOfApproximations = 0;

				for (int polyStep = 0; polyStep < numberOfApproximations; ++polyStep) {

					std::vector<Point3r> samplePoints;
					std::vector<bool> invalidBeams;
					std::vector<bool> oldInvalidBeams;

					bool stop = false;
					int radius = 0;

					while (!stop) {
						std::swap(invalidBeams, oldInvalidBeams);
						propagateInvalidBeams(radius, oldInvalidBeams, invalidBeams);
						int x2 = xKeypoint - radius - 1;
						int y2 = yKeypoint - radius;

						stop = true;

						for (int i = 0; i < 8 * radius || (radius == 0 && i == 0); ++i) {
							if (i <= 2 * radius)
								++x2;
							else if (i <= 4 * radius)
								++y2;
							else if (i <= 6 * radius)
								--x2;
							else
								--y2;

							int index2 = y2 * width + x2;

							if (x2 < 0 || x2 >= width || y2 < 0 || y2 >= height)
								continue;

							Point3r neighbor = context.world[index2];

							if (!isValid(neighbor))
								continue;
							
							if ((context.borderTypeWithDirection[index2] & (unsigned int)BorderExtractor::BorderTypeWithDirection::VeilPointOrShadowBorder) > 0) {
								invalidBeams[i] = true;
								continue;
							}

							real distanceSquared = squaredDistance(point, neighbor);
							if (distanceSquared > maxDistanceSquared) {
								invalidBeams[i] = true;
								continue;
							}

							stop = false;

							real interestValue2 = context.interestImage[index2];
							samplePoints.push_back(Point3r(real(x2 - xKeypoint), real(y2 - yKeypoint), interestValue2));
						}

						++radius;
					}

					// bivariate polynomial approximation
					BivariatePolynomial2 polynomial = bivariatePolynomialApproximation(samplePoints);
					if (polynomial.isInvalid())
						continue;

					real xCriticalPoint, yCriticalPoint;
					int type;

					if (!polynomial.criticalPoint(xCriticalPoint, yCriticalPoint, type))
						break;

					if (type != 0)
						break;

					xKeypoint = static_cast<int>(round(xKeypoint + xCriticalPoint));
					yKeypoint = static_cast<int>(round(yKeypoint + yCriticalPoint));

					if (xKeypoint < 0 || xKeypoint >= width || yKeypoint < 0 || yKeypoint >= height)
						break;

					keypoint = context.world[yKeypoint * width + xKeypoint];
					if (!isValid(keypoint)) {
						keypoint = point;
						break;
					}
				}

				InterestPoint interestPoint;
				interestPoint.point3d = keypoint;
				interestPoint.point2d = imagePointAt(context, keypoint);
				interestPoint.interestValue = interestValue;
				context.tmpInterestPoints.push_back(interestPoint);
			}
		}
	}

	//===============================================================================================================//

	void sortAndFilterInterestPoints(NARFContext &context)
	{
		const int width = context.width;
		const int height = context.height;

		std::sort(context.tmpInterestPoints.begin(), context.tmpInterestPoints.end(), InterestPoint::isBetter);

		real minDistanceSquared = powf(context.minDistanceBetweenInterestPoints * context.supportingSize, 2.0f);
		for (int idx1 = 0; idx1 < context.tmpInterestPoints.size(); ++idx1) {
			if (context.maxNumberOfInterestPoints > 0 && context.interestPoints.size() >= context.maxNumberOfInterestPoints)
				break;

			const InterestPoint &interestPoint = context.tmpInterestPoints[idx1];

			bool isBetterPointTooClose = false;
			for (int idx2 = 0; idx2 < context.interestPoints.size(); ++idx2) {
				const InterestPoint &interestPoint2 = context.interestPoints[idx2];
				real distanceSquared = squaredDistance(interestPoint.point3d, interestPoint2.point3d);
				if (distanceSquared < minDistanceSquared) {
					isBetterPointTooClose = true;
					break;
				}
			}

			if (isBetterPointTooClose)
				continue;

			context.interestPoints.push_back(interestPoint);
			const Point2i &ip = interestPoint.point2d;

			if (ip.x >= 0 && ip.x < width && ip.y >= 0 && ip.y < height && context.depth[ip.y * width + ip.x] != 0) {
				context.isInterestPoint[ip.y * width + ip.x] = true;
			}
		}
	}
}
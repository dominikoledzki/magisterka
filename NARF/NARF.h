#pragma once
#include "NARFContext.h"

namespace NARF
{
	// border extraction
	void initialize(NARFContext &context);
	void depthToWorld(NARFContext &context);
	void calculateTypical3DDistance(NARFContext &context);
	void calculateBorderScore(NARFContext &context);
	void smoothBorderScore(NARFContext &context);
	void firstClassification(NARFContext &context);
	void secondClassification(NARFContext &context);
	void cumulateBorderType(NARFContext &context);
	void calculateLocalSurfaceParameters(NARFContext &context);

	// interest points extraction
	void calculateBorderDirections(NARFContext &context);
	void calculateAverageBorderDirections(NARFContext &context);
	void calculateSurfaceChanges(NARFContext &context);
	void calculateInterestImage(NARFContext &context);
	void calculateInterestPoints(NARFContext &context);
	void sortAndFilterInterestPoints(NARFContext &context);
}

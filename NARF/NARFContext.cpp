#include "NARFContext.h"
#include "BorderExtractor.h"
#include "cuda.h"
#include "cuda_runtime.h"

namespace NARF
{

	NARFContext::NARFContext(int width, int height, real xzFactor, real yzFactor, depth_t *inputDepth) : width(width), height(height), xzFactor(xzFactor), yzFactor(yzFactor)
	{
		int size = width * height;

		// border extraction
		depth = new depth_t[size];
		if (inputDepth)
			memcpy(depth, inputDepth, size * sizeof(depth_t));

		world = new Point3r[size];
		typical3dDistance = new real[size];
		borderTypeWithDirection = new unsigned int[size];
		localSurface = new LocalSurface[size];

		for (int i = 0; i < 4; ++i) {
			borderScore[i] = new real[size];
			avgNeighborRange[i] = new real[size];
			smoothBorderScore[i] = new real[size];
			potentialBorderType[i] = new BorderExtractor::PotentialBorderType[size];
			borderType[i] = new BorderExtractor::BorderType[size];
			secondBorderScore[i] = new real[size];
		}

		// interest points extraction
		borderDirections = new Vector3r[size];
		averageBorderDirections = new Vector3r[size];
		surfaceChangeDirection = new Vector3r[size];
		surfaceChangeScore = new real[size];
		interestImage = new real[size];
		isInterestPoint = new bool[size];
	}

	NARFContext::NARFContext(const cuNARF::cuNARFContext &cuContext) :
		NARFContext(cuContext.width, cuContext.height, cuContext.xzFactor, cuContext.yzFactor, nullptr)
	{
		supportingSize = cuContext.supportingSize;
		pointsUsedForAverage = cuContext.pointsUsedForAverage;
		smoothingRadius = cuContext.smoothingRadius;
		reduceBorderSearchingDistance = cuContext.reduceBorderSearchingDistance;
		borderScoreThreshold = cuContext.borderScoreThreshold;
		localSurfaceRadius = cuContext.localSurfaceRadius;
		localSurfaceNumerOfClosestNeighbors = cuContext.localSurfaceNumerOfClosestNeighbors;

		pixelRadiusBorderDirections = cuContext.pixelRadiusBorderDirections;
		minimumBorderProbability = cuContext.minimumBorderProbability;
		minimumWeight = cuContext.minimumWeight;
		mainPrincipalCurvatureRadius = cuContext.mainPrincipalCurvatureRadius;
		minimumInterestValue = cuContext.minimumInterestValue;
		searchRadius = cuContext.searchRadius;
		optimalDistanceToHighSurfaceChange = cuContext.optimalDistanceToHighSurfaceChange;
		addPointsOnStraightEdges = cuContext.addPointsOnStraightEdges;
		doNonMaximumSuppression = cuContext.doNonMaximumSuppression;
		numberOfPolynomialApproximations = cuContext.numberOfPolynomialApproximations;
		maxNumberOfInterestPoints = cuContext.maxNumberOfInterestPoints;
		minDistanceBetweenInterestPoints = cuContext.minDistanceBetweenInterestPoints;

		int size = width * height;

		CUDA_SAFE_CALL(cudaMemcpy(depth, cuContext.depth, sizeof(depth_t) * size, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(world, cuContext.world, sizeof(Point3r) * size, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(typical3dDistance, cuContext.typical3dDistance, sizeof(real) * size, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(borderTypeWithDirection, cuContext.borderTypeWithDirection, sizeof(unsigned int) * size, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(localSurface, cuContext.localSurface, sizeof(LocalSurface) * size, cudaMemcpyDeviceToHost));
	
		for (int i = 0; i < 4; ++i) {
			cudaMemcpy(borderScore[i], cuContext.borderScore[i], sizeof(real) * size, cudaMemcpyDeviceToHost);
			cudaMemcpy(avgNeighborRange[i], cuContext.avgNeighborRange[i], sizeof(real) * size, cudaMemcpyDeviceToHost);
			cudaMemcpy(smoothBorderScore[i], cuContext.smoothBorderScore[i], sizeof(real) * size, cudaMemcpyDeviceToHost);
			cudaMemcpy(potentialBorderType[i], cuContext.potentialBorderType[i], sizeof(BorderExtractor::PotentialBorderType) * size, cudaMemcpyDeviceToHost);
			cudaMemcpy(borderType[i], cuContext.borderType[i], sizeof(BorderExtractor::BorderType) * size, cudaMemcpyDeviceToHost);
			cudaMemcpy(secondBorderScore[i], cuContext.secondBorderScore[i], sizeof(real) * size, cudaMemcpyDeviceToHost);
		}

		// interest points extraction
		CUDA_SAFE_CALL(cudaMemcpy(borderDirections, cuContext.borderDirections, width * height * sizeof(Vector3r), cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(averageBorderDirections, cuContext.averageBorderDirections, width * height * sizeof(Vector3r), cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(surfaceChangeDirection, cuContext.surfaceChangeDirection, width * height * sizeof(Vector3r), cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(surfaceChangeScore, cuContext.surfaceChangeScore, width * height * sizeof(real), cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(interestImage, cuContext.interestImage, width * height * sizeof(real), cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(isInterestPoint, cuContext.isInterestPoint, width * height * sizeof(bool), cudaMemcpyDeviceToHost));

		tmpInterestPoints = cuContext.hostTmpInterestPoints;
		interestPoints = cuContext.hostInterestPoints;
	}


	void NARFContext::setParameters(const NARFParameters &params)
	{
		supportingSize = params.supportingSize;
		pointsUsedForAverage = params.pointsUsedForAverage;
		smoothingRadius = params.smoothingRadius;
		reduceBorderSearchingDistance = params.reduceBorderSearchingDistance;
		borderScoreThreshold = params.borderScoreThreshold;
		localSurfaceRadius = params.localSurfaceRadius;
		localSurfaceNumerOfClosestNeighbors = params.localSurfaceNumerOfClosestNeighbors;

		pixelRadiusBorderDirections = params.pixelRadiusBorderDirections;
		minimumBorderProbability = params.minimumBorderProbability;
		minimumWeight = params.minimumWeight;
		mainPrincipalCurvatureRadius = params.mainPrincipalCurvatureRadius;
		minimumInterestValue = params.minimumInterestValue;
		searchRadius = params.searchRadius;
		optimalDistanceToHighSurfaceChange = params.optimalDistanceToHighSurfaceChange;
		addPointsOnStraightEdges = params.addPointsOnStraightEdges;
		doNonMaximumSuppression = params.doNonMaximumSuppression;
		numberOfPolynomialApproximations = params.numberOfPolynomialApproximations;
		maxNumberOfInterestPoints = params.maxNumberOfInterestPoints;
		minDistanceBetweenInterestPoints = params.minDistanceBetweenInterestPoints;
	}	

	void NARFContext::dumpToFile(const string &filename)
	{
		fstream file = fstream(filename, ios_base::out | ios_base::binary);

		int size = width * height;

		// input data
		file.write((char *)&width, sizeof(width));
		file.write((char *)&height, sizeof(height));
		file.write((char *)&xzFactor, sizeof(xzFactor));
		file.write((char *)&yzFactor, sizeof(yzFactor));
		
		file.write((char *)depth, sizeof(depth_t) * size);

		// parameters
		file.write((char *)&supportingSize, sizeof(supportingSize));
		file.write((char *)&pointsUsedForAverage, sizeof(pointsUsedForAverage));
		file.write((char *)&smoothingRadius, sizeof(smoothingRadius));
		file.write((char *)&reduceBorderSearchingDistance, sizeof(reduceBorderSearchingDistance));
		file.write((char *)&borderScoreThreshold, sizeof(borderScoreThreshold));
		file.write((char *)&localSurfaceRadius, sizeof(localSurfaceRadius));
		file.write((char *)&localSurfaceNumerOfClosestNeighbors, sizeof(localSurfaceNumerOfClosestNeighbors));

		// interest points parameters
		file.write((char *)&pixelRadiusBorderDirections, sizeof(pixelRadiusBorderDirections));
		file.write((char *)&minimumBorderProbability, sizeof(minimumBorderProbability));
		file.write((char *)&minimumWeight, sizeof(minimumWeight));
		file.write((char *)&mainPrincipalCurvatureRadius, sizeof(mainPrincipalCurvatureRadius));
		file.write((char *)&minimumInterestValue, sizeof(minimumInterestValue));
		file.write((char *)&searchRadius, sizeof(searchRadius));
		file.write((char *)&optimalDistanceToHighSurfaceChange, sizeof(optimalDistanceToHighSurfaceChange));
		file.write((char *)&addPointsOnStraightEdges, sizeof(addPointsOnStraightEdges));
		file.write((char *)&doNonMaximumSuppression, sizeof(doNonMaximumSuppression));
		file.write((char *)&numberOfPolynomialApproximations, sizeof(numberOfPolynomialApproximations));
		file.write((char *)&maxNumberOfInterestPoints, sizeof(maxNumberOfInterestPoints));
		file.write((char *)&minDistanceBetweenInterestPoints, sizeof(minDistanceBetweenInterestPoints));
		
		// intermediate and output data
		file.write((char *)world, sizeof(Point3r) * size);
		file.write((char *)typical3dDistance, sizeof(real) * size);

		for (int i = 0; i < 4; ++i)
			file.write((char *)borderScore[i], sizeof(real) * size);

		for (int i = 0; i < 4; ++i)
			file.write((char *)avgNeighborRange[i], sizeof(real) * size);

		for (int i = 0; i < 4; ++i)
			file.write((char *)smoothBorderScore[i], sizeof(real) * size);

		for (int i = 0; i < 4; ++i)
			file.write((char *)potentialBorderType[i], sizeof(BorderExtractor::PotentialBorderType) * size);

		for (int i = 0; i < 4; ++i)
			file.write((char *)borderType[i], sizeof(BorderExtractor::BorderType) * size);

		for (int i = 0; i < 4; ++i)
			file.write((char *)secondBorderScore[i], sizeof(real) * size);

		for (int i = 0; i < 4; ++i)
			file.write((char *)borderTypeWithDirection, sizeof(unsigned int) * size);

		file.write((char *)localSurface, sizeof(LocalSurface) * size);

		// interest points extraction data
		file.write((char *)borderDirections, sizeof(Vector3r) * size);
		file.write((char *)averageBorderDirections, sizeof(Vector3r) * size);
		file.write((char *)surfaceChangeDirection, sizeof(Vector3r) * size);
		file.write((char *)surfaceChangeScore, sizeof(real) * size);
		file.write((char *)interestImage, sizeof(real) * size);
		file.write((char *)isInterestPoint, sizeof(bool) * size);
	}

	void NARFContext::loadFromFile(const string &filename)
	{
		fstream file = fstream(filename, ios_base::in | ios_base::binary);

		int size = width * height;

		// input data
		file.read((char *)&width, sizeof(width));
		file.read((char *)&height, sizeof(height));
		file.read((char *)&xzFactor, sizeof(xzFactor));
		file.read((char *)&yzFactor, sizeof(yzFactor));

		file.read((char *)depth, sizeof(depth_t) * size);

		// parameters
		file.read((char *)&supportingSize, sizeof(supportingSize));
		file.read((char *)&pointsUsedForAverage, sizeof(pointsUsedForAverage));
		file.read((char *)&smoothingRadius, sizeof(smoothingRadius));
		file.read((char *)&reduceBorderSearchingDistance, sizeof(reduceBorderSearchingDistance));
		file.read((char *)&borderScoreThreshold, sizeof(borderScoreThreshold));
		file.read((char *)&localSurfaceRadius, sizeof(localSurfaceRadius));
		file.read((char *)&localSurfaceNumerOfClosestNeighbors, sizeof(localSurfaceNumerOfClosestNeighbors));

		// interest points parameters
		file.read((char *)&pixelRadiusBorderDirections, sizeof(pixelRadiusBorderDirections));
		file.read((char *)&minimumBorderProbability, sizeof(minimumBorderProbability));
		file.read((char *)&minimumWeight, sizeof(minimumWeight));
		file.read((char *)&mainPrincipalCurvatureRadius, sizeof(mainPrincipalCurvatureRadius));
		file.read((char *)&minimumInterestValue, sizeof(minimumInterestValue));
		file.read((char *)&searchRadius, sizeof(searchRadius));
		file.read((char *)&optimalDistanceToHighSurfaceChange, sizeof(optimalDistanceToHighSurfaceChange));
		file.read((char *)&addPointsOnStraightEdges, sizeof(addPointsOnStraightEdges));
		file.read((char *)&doNonMaximumSuppression, sizeof(doNonMaximumSuppression));
		file.read((char *)&numberOfPolynomialApproximations, sizeof(numberOfPolynomialApproximations));
		file.read((char *)&maxNumberOfInterestPoints, sizeof(maxNumberOfInterestPoints));
		file.read((char *)&minDistanceBetweenInterestPoints, sizeof(minDistanceBetweenInterestPoints));

		// intermediate and output data
		file.read((char *)world, sizeof(Point3r) * size);
		file.read((char *)typical3dDistance, sizeof(real) * size);

		for (int i = 0; i < 4; ++i)
			file.read((char *)borderScore[i], sizeof(real) * size);

		for (int i = 0; i < 4; ++i)
			file.read((char *)avgNeighborRange[i], sizeof(real) * size);

		for (int i = 0; i < 4; ++i)
			file.read((char *)smoothBorderScore[i], sizeof(real) * size);

		for (int i = 0; i < 4; ++i)
			file.read((char *)potentialBorderType[i], sizeof(BorderExtractor::PotentialBorderType) * size);

		for (int i = 0; i < 4; ++i)
			file.read((char *)borderType[i], sizeof(BorderExtractor::BorderType) * size);

		for (int i = 0; i < 4; ++i)
			file.read((char *)secondBorderScore[i], sizeof(real) * size);

		for (int i = 0; i < 4; ++i)
			file.read((char *)borderTypeWithDirection, sizeof(unsigned int) * size);

		file.read((char *)localSurface, sizeof(LocalSurface) * size);

		// interest points extraction data
		file.read((char *)borderDirections, sizeof(Vector3r) * size);
		file.read((char *)averageBorderDirections, sizeof(Vector3r) * size);
		file.read((char *)surfaceChangeDirection, sizeof(Vector3r) * size);
		file.read((char *)surfaceChangeScore, sizeof(real) * size);
		file.read((char *)interestImage, sizeof(real) * size);
		file.read((char *)isInterestPoint, sizeof(bool) * size);
	}

	void NARFContext::copyParameters(const cuNARF::cuNARFContext &cuContext)
	{
		supportingSize = cuContext.supportingSize;
		pointsUsedForAverage = cuContext.pointsUsedForAverage;
		smoothingRadius = cuContext.smoothingRadius;
		reduceBorderSearchingDistance = cuContext.reduceBorderSearchingDistance;
		borderScoreThreshold = cuContext.borderScoreThreshold;
		localSurfaceRadius = cuContext.localSurfaceRadius;
		localSurfaceNumerOfClosestNeighbors = cuContext.localSurfaceNumerOfClosestNeighbors;

		pixelRadiusBorderDirections = cuContext.pixelRadiusBorderDirections;
		minimumBorderProbability = cuContext.minimumBorderProbability;
		minimumWeight = cuContext.minimumWeight;
		mainPrincipalCurvatureRadius = cuContext.mainPrincipalCurvatureRadius;
		minimumInterestValue = cuContext.minimumInterestValue;
		searchRadius = cuContext.searchRadius;
		optimalDistanceToHighSurfaceChange = cuContext.optimalDistanceToHighSurfaceChange;
		addPointsOnStraightEdges = cuContext.addPointsOnStraightEdges;
		doNonMaximumSuppression = cuContext.doNonMaximumSuppression;
		numberOfPolynomialApproximations = cuContext.numberOfPolynomialApproximations;
		maxNumberOfInterestPoints = cuContext.maxNumberOfInterestPoints;
		minDistanceBetweenInterestPoints = cuContext.minDistanceBetweenInterestPoints;
	}

	void NARFContext::copyTypical3DDistance(const cuNARF::cuNARFContext &cuContext)
	{
		int size = width * height;

		CUDA_SAFE_CALL(cudaMemcpy(depth, cuContext.depth, sizeof(depth_t) * size, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(world, cuContext.world, sizeof(Point3r) * size, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(typical3dDistance, cuContext.typical3dDistance, sizeof(real) * size, cudaMemcpyDeviceToHost));
	}

	void NARFContext::copyBorderScore(const cuNARF::cuNARFContext &cuContext)
	{
		int size = width * height;
		for (int i = 0; i < 4; ++i) {
			cudaMemcpy(borderScore[i], cuContext.borderScore[i], sizeof(real) * size, cudaMemcpyDeviceToHost);
			cudaMemcpy(avgNeighborRange[i], cuContext.avgNeighborRange[i], sizeof(real) * size, cudaMemcpyDeviceToHost);
		}
	}

	void NARFContext::copySmoothedBorderScore(const cuNARF::cuNARFContext &cuContext)
	{
		int size = width * height;
		for (int i = 0; i < 4; ++i) {
			cudaMemcpy(smoothBorderScore[i], cuContext.smoothBorderScore[i], sizeof(real) * size, cudaMemcpyDeviceToHost);
		}
	}

	void NARFContext::copyPotentialBorderType(const cuNARF::cuNARFContext &cuContext)
	{
		int size = width * height;
		for (int i = 0; i < 4; ++i) {
			cudaMemcpy(potentialBorderType[i], cuContext.potentialBorderType[i], sizeof(BorderExtractor::PotentialBorderType) * size, cudaMemcpyDeviceToHost);
		}
	}

	void NARFContext::copyBorderType(const cuNARF::cuNARFContext &cuContext)
	{
		int size = width * height;
		for (int i = 0; i < 4; ++i) {
			cudaMemcpy(borderType[i], cuContext.borderType[i], sizeof(BorderExtractor::BorderType) * size, cudaMemcpyDeviceToHost);
			cudaMemcpy(secondBorderScore[i], cuContext.secondBorderScore[i], sizeof(real) * size, cudaMemcpyDeviceToHost);
		}
		CUDA_SAFE_CALL(cudaMemcpy(borderTypeWithDirection, cuContext.borderTypeWithDirection, sizeof(unsigned int) * size, cudaMemcpyDeviceToHost));
	}

	void NARFContext::copyLocalSurfaceParameters(const cuNARF::cuNARFContext &cuContext)
	{
		int size = width * height;
		CUDA_SAFE_CALL(cudaMemcpy(localSurface, cuContext.localSurface, sizeof(LocalSurface) * size, cudaMemcpyDeviceToHost));
	}

	void NARFContext::copyBorderDirections(const cuNARF::cuNARFContext &cuContext) {
		CUDA_SAFE_CALL(cudaMemcpy(borderDirections, cuContext.borderDirections, width * height * sizeof(Vector3r), cudaMemcpyDeviceToHost));
	}

	void NARFContext::copyAvgBorderDirections(const cuNARF::cuNARFContext &cuContext) {
		CUDA_SAFE_CALL(cudaMemcpy(averageBorderDirections, cuContext.averageBorderDirections, width * height * sizeof(Vector3r), cudaMemcpyDeviceToHost));
	}

	void NARFContext::copySurfaceChanges(const cuNARF::cuNARFContext &cuContext) {
		CUDA_SAFE_CALL(cudaMemcpy(surfaceChangeDirection, cuContext.surfaceChangeDirection, width * height * sizeof(Vector3r), cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(surfaceChangeScore, cuContext.surfaceChangeScore, width * height * sizeof(real), cudaMemcpyDeviceToHost));
	}

	void NARFContext::copyInterestImage(const cuNARF::cuNARFContext &cuContext) {
		CUDA_SAFE_CALL(cudaMemcpy(interestImage, cuContext.interestImage, width * height * sizeof(real), cudaMemcpyDeviceToHost));
	}

	void NARFContext::copyInterestPoints(const cuNARF::cuNARFContext &cuContext) {
		CUDA_SAFE_CALL(cudaMemcpy(isInterestPoint, cuContext.isInterestPoint, width * height * sizeof(bool), cudaMemcpyDeviceToHost));

		tmpInterestPoints = cuContext.hostTmpInterestPoints;
		interestPoints = cuContext.hostInterestPoints;
	}

	void NARFContext::dispose()
	{
		delete[] depth;
		delete[] world;
		delete[] typical3dDistance;
		delete[] borderTypeWithDirection;
		delete[] localSurface;

		for (int i = 0; i < 4; ++i) {
			delete[] borderScore[i];
			delete[] avgNeighborRange[i];
			delete[] smoothBorderScore[i];
			delete[] potentialBorderType[i];
			delete[] borderType[i];
			delete[] secondBorderScore[i];

			borderScore[i] = nullptr;
			avgNeighborRange[i] = nullptr;
			smoothBorderScore[i] = nullptr;
			potentialBorderType[i] = nullptr;
			borderType[i] = nullptr;
			secondBorderScore[i] = nullptr;
		}

		// interest points extraction
		delete[] borderDirections;
		delete[] averageBorderDirections;
		delete[] surfaceChangeDirection;
		delete[] surfaceChangeScore;
		delete[] interestImage;
		delete[] isInterestPoint;

		depth = nullptr;
		world = nullptr;
		typical3dDistance = nullptr;
		borderTypeWithDirection = nullptr;
		localSurface = nullptr;
		borderDirections = nullptr;
		averageBorderDirections = nullptr;
		surfaceChangeDirection = nullptr;
		surfaceChangeScore = nullptr;
		interestImage = nullptr;
		isInterestPoint = nullptr;
	}
}

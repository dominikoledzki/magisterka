#pragma once
#include "BorderExtractor.h"
#include "DataTypes.h"
#include "cuNARF.h"

namespace NARF
{
	class NARFContext
	{
	public:

		NARFContext(int width, int height, real xzFactor, real yzFactor, depth_t *depth);
		NARFContext(const cuNARF::cuNARFContext &cuContext);

		void setParameters(const NARFParameters &params);

		void dumpToFile(const string &filename);
		void loadFromFile(const string &filename);

		void copyParameters(const cuNARF::cuNARFContext &cuContext);
		void copyTypical3DDistance(const cuNARF::cuNARFContext &cuContext);
		void copyBorderScore(const cuNARF::cuNARFContext &cuContext);
		void copySmoothedBorderScore(const cuNARF::cuNARFContext &cuContext);
		void copyPotentialBorderType(const cuNARF::cuNARFContext &cuContext);
		void copyBorderType(const cuNARF::cuNARFContext &cuContext);
		void copyLocalSurfaceParameters(const cuNARF::cuNARFContext &cuContext);

		void copyBorderDirections(const cuNARF::cuNARFContext &cuContext);
		void copyAvgBorderDirections(const cuNARF::cuNARFContext &cuContext);
		void copySurfaceChanges(const cuNARF::cuNARFContext &cuContext);
		void copyInterestImage(const cuNARF::cuNARFContext &cuContext);
		void copyInterestPoints(const cuNARF::cuNARFContext &cuContext);

		void dispose();

		// input data
		int width;
		int height;
		real xzFactor;
		real yzFactor;
		depth_t *depth;

		// parameters
		int supportingSize;
		int pointsUsedForAverage;
		int smoothingRadius;
		int reduceBorderSearchingDistance;
		real borderScoreThreshold;
		int localSurfaceRadius;
		int localSurfaceNumerOfClosestNeighbors;

		// interest points parameters
		int pixelRadiusBorderDirections;
		real minimumBorderProbability;
		int minimumWeight;
		int mainPrincipalCurvatureRadius;
		real minimumInterestValue;
		real searchRadius;
		real optimalDistanceToHighSurfaceChange;
		bool addPointsOnStraightEdges;
		bool doNonMaximumSuppression;
		int numberOfPolynomialApproximations;
		int maxNumberOfInterestPoints;
		real minDistanceBetweenInterestPoints;

		// intermediate and output data
		Point3r *world;
		real *typical3dDistance;
		real *borderScore[4];
		real *avgNeighborRange[4];
		real *smoothBorderScore[4];
		BorderExtractor::PotentialBorderType *potentialBorderType[4];
		BorderExtractor::BorderType *borderType[4];
		real *secondBorderScore[4];
		unsigned int *borderTypeWithDirection;
		LocalSurface *localSurface;

		// interest points extraction data
		Vector3r *borderDirections;
		Vector3r *averageBorderDirections;
		Vector3r *surfaceChangeDirection;
		real *surfaceChangeScore;
		real *interestImage;
		bool *isInterestPoint;
		std::vector<InterestPoint> tmpInterestPoints;
		std::vector<InterestPoint> interestPoints;
	};

}

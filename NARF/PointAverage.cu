#include "PointAverage.h"
#include "eig3.h"

namespace NARF
{
	CUDA_CALLABLE PointAverage::PointAverage() : noOfSamples(0), accumulatedWeight(0.0), mean(Point3r()), covariance(Matrix33r())
	{
	}

	CUDA_CALLABLE PointAverage::~PointAverage()
	{
	}

	CUDA_CALLABLE void PointAverage::reset()
	{
		noOfSamples = 0;
		accumulatedWeight = 0.0;
		mean = Point3r();
		covariance = Matrix33r();
	}

	CUDA_CALLABLE const Point3r &PointAverage::getMean() const
	{
		return mean;
	}

	CUDA_CALLABLE const Matrix33r &PointAverage::getCovariance() const
	{
		return covariance;
	}

	CUDA_CALLABLE real PointAverage::getAccumulatedWeight() const
	{
		return accumulatedWeight;
	}

	CUDA_CALLABLE unsigned int PointAverage::getNoOfSamples() const
	{
		return noOfSamples;
	}

	CUDA_CALLABLE void PointAverage::addSample(const Point3r &sample, real weight)
	{
		if (weight == 0.0)
			return;

		++noOfSamples;
		accumulatedWeight += weight;
		real alpha = weight / accumulatedWeight;
		Matrix<real, 3, 1> diff = sample - mean;

		//covariance = (covariance + (diff * diff.transposed()) * alpha) * (1.0f - alpha);
		covariance = (covariance + diff.multiplyByTransposition() * alpha) * (1.0f - alpha);
		mean = Matrix<real, 3, 1>(mean) + diff * alpha;
	}

	CUDA_CALLABLE void PointAverage::pca(Matrix<real, 3, 1> &eigenValues, Point3r &eigenVector1, Point3r &eigenVector2, Point3r &eigenVector3) const
	{
		// copy data
		double cov[3][3];
		double V[3][3];
		double D[3];

		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 3; ++j) {
				cov[i][j] = covariance[i * 3 + j];
			}
		}

		eigen_decomposition(cov, V, D);
		
		
		eigenValues[0] = static_cast<real>(D[0]);
		eigenValues[1] = static_cast<real>(D[1]);
		eigenValues[2] = static_cast<real>(D[2]);

		eigenVector1.x = static_cast<real>(V[0][0]);
		eigenVector1.y = static_cast<real>(V[0][1]);
		eigenVector1.z = static_cast<real>(V[0][2]);

		eigenVector2.x = static_cast<real>(V[1][0]);
		eigenVector2.y = static_cast<real>(V[1][1]);
		eigenVector2.z = static_cast<real>(V[1][2]);

		eigenVector3.x = static_cast<real>(V[2][0]);
		eigenVector3.y = static_cast<real>(V[2][1]);
		eigenVector3.z = static_cast<real>(V[2][2]);
	}

}

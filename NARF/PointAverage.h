#pragma once
#include "DataTypes.h"
#include "Utilities.h"

namespace NARF
{

	class PointAverage
	{
	public:
		CUDA_CALLABLE PointAverage();
		CUDA_CALLABLE ~PointAverage();

		CUDA_CALLABLE void reset();
		CUDA_CALLABLE const Point3r &getMean() const;
		CUDA_CALLABLE const Matrix33r &getCovariance() const;
		CUDA_CALLABLE real getAccumulatedWeight() const;
		CUDA_CALLABLE unsigned int getNoOfSamples() const;
		CUDA_CALLABLE void addSample(const Point3r &sample, real weight = 1.0);
		CUDA_CALLABLE void pca(Matrix<real, 3, 1> &eigenValues, Point3r &eigenVector1, Point3r &eigenVector2, Point3r &eigenVector3) const;

	private:
		unsigned int noOfSamples;
		real accumulatedWeight;
		Point3r mean;
		Matrix33r covariance;

	};

}
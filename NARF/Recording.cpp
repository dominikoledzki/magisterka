#include <algorithm>
#include "Recording.h"
#include "DataTypes.h"

namespace NARF {

	Recording::Recording(istream &stream)
	{
		stream.read(reinterpret_cast<char *>(&width), sizeof(width));
		stream.read(reinterpret_cast<char *>(&height), sizeof(height));
		stream.read(reinterpret_cast<char *>(&xzFactor), sizeof(xzFactor));
		stream.read(reinterpret_cast<char *>(&yzFactor), sizeof(yzFactor));

		char buf[640 * 480 * 2];
		stream.read(buf, 640 * 480 * 2);

		while (stream.peek() != EOF)
		{
			Frame::Ptr frame = Frame::Ptr(new Frame(width, height, xzFactor, yzFactor, stream));
			frames.push_back(frame);
		}
	}

	Recording::~Recording()
	{
		frames.clear();
	}

	int Recording::getWidth()
	{
		return width;
	}

	int Recording::getHeight()
	{
		return height;
	}

	real Recording::getXZFactor()
	{
		return xzFactor;
	}

	real Recording::getYZFactor()
	{
		return yzFactor;
	}

	Frame::Ptr Recording::getFrame(int index)
	{
		return frames[index];
	}

	const int Recording::getFramesCount()
	{
		return frames.size();
	}

}
#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include "DataTypes.h"
#include "Frame.h"

using namespace std;

namespace NARF
{

	class Recording
	{
	public:
		Recording(istream &stream);
		~Recording();

		int getWidth();
		int getHeight();
		real getXZFactor();
		real getYZFactor();

		Frame::Ptr getFrame(int index);
		const int getFramesCount();

	private:
		int width;
		int height;
		real xzFactor;
		real yzFactor;
		vector<Frame::Ptr> frames;
	};

}
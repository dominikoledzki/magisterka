#pragma once
#include "Utilities.h"

template<typename T>
CUDA_CALLABLE void insertionSort(T *array, int size)
{
	for (int i = 0; i < size-1; ++i) {
		int minIdx = i;
		for (int j = i+1; j < size; ++j) {
			if (array[j] < array[minIdx])
				minIdx = j;
		}
		T temp = array[i];
		array[i] = array[minIdx];
		array[minIdx] = temp;
	}
}
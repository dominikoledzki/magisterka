#include "UICommons.h"

GtkWidget *gtk_container_get_child(GtkContainer *container, const gchar *name)
{
	if (name == NULL)
		return NULL;

	GList *children = gtk_container_get_children(container);
	GtkWidget *result = NULL;

	for (GList *current = children; current != NULL; current = current->next) {
		const gchar *widgetName = gtk_buildable_get_name(GTK_BUILDABLE(current->data));

		if (g_strcmp0(widgetName, name) == 0) {
			result = GTK_WIDGET(current->data);
			break;
		}

		if (GTK_IS_CONTAINER(current->data)) {
			result = gtk_container_get_child(GTK_CONTAINER(current->data), name);
			if (result != NULL)
				break;
		}
	}

	g_list_free(children);
	return result;
}


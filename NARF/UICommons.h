#pragma once
#include <gtk/gtk.h>
#include "Recording.h"
#include <vector>
#include "cuNARF.h"
#include "NARFContext.h"
#include "cuFPFH.h"
#include "FPFHContext.h"
#include "cuFPFHContext.h"

using namespace std;
using namespace NARF;
using namespace FPFH;

enum ImageType {
	DepthMap,
	Typical3DDistance,
	BorderScore,
	BorderScoreUp,
	BorderScoreRight,
	BorderScoreDown,
	BorderScoreLeft,
	SmoothedBorderScore,
	SmoothedBorderScoreUp,
	SmoothedBorderScoreRight,
	SmoothedBorderScoreDown,
	SmoothedBorderScoreLeft,
	FirstClassification,
	FirstClassificationUp,
	FirstClassificationRight,
	FirstClassificationDown,
	FirstClassificationLeft,
	SecondClassification,
	SecondClassificationUp,
	SecondClassificationRight,
	SecondClassificationDown,
	SecondClassificationLeft,
	LocalSurfaceParameters,
	LocalSurfaceParametersNormal,
	LocalSurfaceParametersMeanNeighborDistance,
	LocalSurfaceParametersEigenValues,
	LocalSurfaceParametersNormalNoJumps,
	LocalSurfaceParametersMeanNeighborDistanceNoJumps,
	LocalSurfaceParametersEigenValuesNoJumps,
	LocalSurfaceParametersMaxNeighborDistance,
	//////////
	BorderDirections,
	AvgBorderDirections,
	SurfaceChange,
	SurfaceChangeScore,
	SurfaceChangeDirection,
	InterestImage,
	InterestPoints,
	/////////
	FPFHNormals,
	SPFHImage,
	FPFHImage,
	NoImageType
};

struct DataRow {
	ImageType imageType;
	GdkPixbuf *pixbuf;
	GdkPixbuf *miniature;
	vector<DataRow> children;

	DataRow(ImageType type, GdkPixbuf *pixbuf = nullptr, GdkPixbuf *miniature = nullptr)
		: imageType(type),
		pixbuf(pixbuf),
		miniature(miniature)
	{}
};

struct Document {
	bool changed;
	Recording *recording;
	unsigned currentFrame;
	vector<int> selectedItem;

	vector<vector<DataRow> > data;
	vector<cuNARF::cuNARFContext *> narfContexts;
	vector<NARF::NARFContext *> cpuNarfContexts;

	vector<cuFPFH::cuFPFHContext *>fpfhContexts;
	vector<FPFH::FPFHContext *> cpuFpfhContexts;

	Document() : recording(nullptr), changed(false), currentFrame(0) {}

	void setSelectedItem(const vector<int> newSelection) {
		if (isPathValid(newSelection))
			selectedItem = newSelection;
	}

	bool isPathValid(const vector<int> &path) {
		return isPathValidImpl(path, data[currentFrame]);
	}

	string potentialBorderTypeToString(NARF::BorderExtractor::PotentialBorderType potentialBorderType) {
		switch (potentialBorderType)
		{
		case NARF::BorderExtractor::PotentialBorderType::ObstacleBorder:
			return "Obstacle Border";

		case NARF::BorderExtractor::PotentialBorderType::ShadowBorder:
			return "Shadow Border";

		case NARF::BorderExtractor::PotentialBorderType::Unknown:
			return "Unknown";
		default:
			return "";
		}
	}

	string borderTypeToString(NARF::BorderExtractor::BorderType borderType) {
		switch (borderType) {
		case NARF::BorderExtractor::BorderType::NoBorder:
			return "No Border";
		case NARF::BorderExtractor::BorderType::ObstacleBorder:
			return "Obstacle Border";
		case NARF::BorderExtractor::BorderType::ShadowBorder:
			return "Shadow Border";
		case NARF::BorderExtractor::BorderType::VeilPoint:
			return "Veil Point";
		}
	}

	string toolTipForPoint(int x, int y) {
		ImageType selectedImageType = imageTypeForSelectedItem();
		NARF::NARFContext *cpuContext = cpuNarfContexts[currentFrame];
		int width = 0;
		int height = 0;
		if (cpuContext) {
			width = cpuContext->width;
			height = cpuContext->height;
		}
		else if (recording) {
			width = recording->getWidth();
			height = recording->getHeight();
		}

		FPFH::FPFHContext *fpfhContext = cpuFpfhContexts[currentFrame];

		if (x < 0 || y < 0 || x >= width || y >= height) {
			return "";
		}

		stringstream ss;

		ss << "(" << x << "; " << y << "): ";

		switch (selectedImageType)
		{
		case DepthMap:
		{
			ss << recording->getFrame(currentFrame)->depthAt(x, y);
		}
		break;
		case Typical3DDistance:
		{
			ss << cpuContext->typical3dDistance[y * width + x];
		}
		break;

		case BorderScoreUp:
		{
			ss << cpuContext->borderScore[0][y * width + x];
		}
		break;
		case BorderScoreRight:
		{
			ss << cpuContext->borderScore[1][y * width + x];
		}
		break;
		case BorderScoreDown:
		{
			ss << cpuContext->borderScore[2][y * width + x];
		}
		break;
		case BorderScoreLeft:
		{
			ss << cpuContext->borderScore[3][y * width + x];
		}
		break;

		case SmoothedBorderScoreUp:
		{
			ss << cpuContext->smoothBorderScore[0][y * width + x];
		}
		break;
		case SmoothedBorderScoreRight:
		{
			ss << cpuContext->smoothBorderScore[1][y * width + x];
		}
		break;
		case SmoothedBorderScoreDown:
		{
			ss << cpuContext->smoothBorderScore[2][y * width + x];
		}
		break;
		case SmoothedBorderScoreLeft:
		{
			ss << cpuContext->smoothBorderScore[3][y * width + x];
		}
		break;
		case FirstClassificationUp:
		{
			ss << potentialBorderTypeToString(cpuContext->potentialBorderType[0][y * width + x]);
		}
		break;
		case FirstClassificationRight:
		{
			ss << potentialBorderTypeToString(cpuContext->potentialBorderType[1][y * width + x]);
		}
		break;
		case FirstClassificationDown:
		{
			ss << potentialBorderTypeToString(cpuContext->potentialBorderType[2][y * width + x]);
		}
		break;
		case FirstClassificationLeft:
		{
			ss << potentialBorderTypeToString(cpuContext->potentialBorderType[3][y * width + x]);
		}
		break;
		case SecondClassificationUp:
		{
			ss << borderTypeToString(cpuContext->borderType[0][y * width + x]);
		}
		break;
		case SecondClassificationRight:
		{
			ss << borderTypeToString(cpuContext->borderType[1][y * width + x]);
		}
		break;
		case SecondClassificationDown:
		{
			ss << borderTypeToString(cpuContext->borderType[2][y * width + x]);
		}
		break;
		case SecondClassificationLeft:
		{
			ss << borderTypeToString(cpuContext->borderType[3][y * width + x]);
		}
		break;

		case LocalSurfaceParametersNormal:
		{
			ss << cpuContext->localSurface[y * width + x].normal.toString();
		}
		break;
		case LocalSurfaceParametersMeanNeighborDistance:
		{
			ss << cpuContext->localSurface[y * width + x].neighborhoodMean.toString();
		}
		break;
		case LocalSurfaceParametersEigenValues:
		{
			ss << cpuContext->localSurface[y * width + x].eigenValues.toString();
		}
		break;
		case LocalSurfaceParametersNormalNoJumps:
		{
			ss << cpuContext->localSurface[y * width + x].normalNoJumps.toString();
		}
		break;
		case LocalSurfaceParametersMeanNeighborDistanceNoJumps:
		{
			ss << cpuContext->localSurface[y * width + x].nighborhoodMeanNoJumps.toString();
		}
		break;
		case LocalSurfaceParametersEigenValuesNoJumps:
		{
			ss << cpuContext->localSurface[y * width + x].eigenValuesNoJumps.toString();
		}
		break;
		case LocalSurfaceParametersMaxNeighborDistance:
		{
			ss << sqrtf(cpuContext->localSurface[y * width + x].maxNeighborDistanceSquared);
		}
		break;

		case BorderDirections:
		{
			ss << cpuContext->borderDirections[y*width + x].toString();
		}
		break;

		case AvgBorderDirections:
		{
			ss << cpuContext->averageBorderDirections[y*width + x].toString();
		}
		break;

		case SurfaceChangeScore:
		{
			ss << cpuContext->surfaceChangeScore[y * width + x];
		}
		break;

		case SurfaceChangeDirection:
		{
			ss << cpuContext->surfaceChangeDirection[y * width + x].toString();
		}
		break;

		case InterestImage:
		{
			ss << cpuContext->interestImage[y * width + x];
		}
		break;

		case InterestPoints:
		{
			ss << cpuContext->isInterestPoint[y * width + x] ? "" : "punkt charakterystyczny";
		}
		break;

		case FPFHNormals:
		{
			ss << fpfhContext->normals[y * width + x].toString();
		}
		break;

		case SPFHImage:
		{

		}
		break;

		case FPFHImage:
		{

		}
			break;



		default:
			break;
		}

		return ss.str();
	}

	void importRecording(string recordingFilename) {
		ifstream recordFile;
		recordFile.open(recordingFilename, ifstream::in | ifstream::binary);
		recording = new Recording(recordFile);
		changed = true;

		data.clear();
		data.resize(recording->getFramesCount());

		narfContexts.clear();
		narfContexts.resize(recording->getFramesCount(), nullptr);

		cpuNarfContexts.clear();
		cpuNarfContexts.resize(recording->getFramesCount(), nullptr);

		fpfhContexts.clear();
		fpfhContexts.resize(recording->getFramesCount(), nullptr);

		cpuFpfhContexts.clear();
		cpuFpfhContexts.resize(recording->getFramesCount(), nullptr);

		for (int frame = 0; frame < recording->getFramesCount(); ++frame) {
			cv::Mat depthMapMiniature = recording->getFrame(frame)->get_miniature_image();
			GdkPixbuf *depthMapMini = gdk_pixbuf_new_from_cvmat(depthMapMiniature);
			depthMapMiniature.release();

			cv::Mat depthMatImage = recording->getFrame(frame)->get_depth_image();
			GdkPixbuf *depthPixbuf = gdk_pixbuf_new_from_cvmat(depthMatImage);
			depthMatImage.release();

			data[frame].push_back(DataRow(ImageType::DepthMap, depthPixbuf, depthMapMini));
		}
	}

	~Document() {
		delete recording;
	}

private:
	bool isPathValidImpl(const vector<int> &path, const vector<DataRow> &data) {
		if (path.size() == 0)
			return true;

		int idx = path.front();
		if (data.size() <= idx)
			return false;

		vector<int> newPath = vector<int>(path.begin()+1, path.end());
		const vector<DataRow> &newData = data[idx].children;

		return isPathValidImpl(newPath, newData);
	}

	ImageType imageTypeForSelectedItem()
	{
		if (selectedItem.size() == 0) {
			return NoImageType;
		}

		int pathIdx = 0;
		DataRow *current = &(data[currentFrame][selectedItem[pathIdx]]);

		pathIdx++;
		for (; pathIdx < selectedItem.size(); ++pathIdx) {
			current = &(current->children[selectedItem[pathIdx]]);
		}

		return current->imageType;
	}
};

struct AppData {
	Document *document;

	GtkBuilder *builder;
	GtkApplication *application;

	void set_document(Document *new_document) {
		if (document) {
			delete document;
		}
		document = new_document;
	}

	// UI elements
	GtkWindow *mainWindow;
	GtkScale *framesScale;
	GtkSpinButton *framesSpinButton;
	GtkTreeStore *imagesTreeStore;
	GtkTreeView *treeView;
	GtkAlignment *imageFrameAlignment;
	GtkImage *previewWidget;
};

GtkWidget *gtk_container_get_child(GtkContainer *container, const gchar *name);

#include "Utilities.h"
#include <ctime>
#include <sstream>
#include <iomanip>

std::string dateTimeString()
{
	time_t rawtime;
	struct tm timeinfo;
	char buffer[80];

	time(&rawtime);
	localtime_s(&timeinfo, &rawtime);

	strftime(buffer, 80, "%Y%m%d-%H%M%S", &timeinfo);
	
	return std::string(buffer);
}

GdkPixbuf *gdk_pixbuf_new_from_cvmat(cv::Mat mat) {
	GdkPixbuf *pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8, mat.size().width, mat.size().height);
	guchar *pixels = gdk_pixbuf_get_pixels(pixbuf);
	memcpy(pixels, mat.data, mat.dataend - mat.data);

	return pixbuf;
}
#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include <math_constants.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <opencv2\opencv.hpp>
#include <vector>

#ifdef __CUDACC__
#define CUDA_CALLABLE __host__ __device__
#define CUDA_DEVICE __device__
#else
#define CUDA_CALLABLE
#define CUDA_DEVICE
#endif 

// Macro to catch CUDA errors in CUDA runtime calls
#define CUDA_SAFE_CALL(call) \
do { \
	cudaError_t err = call; \
	if (cudaSuccess != err) { \
		fprintf (stderr, "Cuda error in file '%s' in line %i : %s.\n", \
				 __FILE__, __LINE__, cudaGetErrorString(err) ); \
		system("PAUSE"); \
		exit(EXIT_FAILURE);       \
		} \
} while (0)

#if defined(__CUDACC__) // NVCC
	#define MY_ALIGN(n) __align__(n)
#elif defined(__GNUC__) // GCC
	#define MY_ALIGN(n) __attribute__((aligned(n)))
#elif defined(_MSC_VER) // MSVC
	#define MY_ALIGN(n) __declspec(align(n))
#else
	#error "Please provide a definition for MY_ALIGN macro for your host compiler!"
#endif

const double PI = 3.1415926535897932384;

using namespace std;

template<typename T>
void writeToFile(const string &filename, T *array, size_t size) {
	size_t bytes = sizeof(T) / sizeof(char) * size;
	ofstream file;
	file.open(filename, ofstream::out | ofstream::binary);
	file.write((char *)array, bytes);
	file.close();
}

template<int exp>
class mexp2
{
public:
	enum {
		value = mexp2<exp / 2>::value * mexp2<exp / 2>::value * mexp2<exp % 2>::value
	};
};

template<>
class mexp2 < 1 >
{
public:
	enum {
		value = 2
	};
};

template<>
class mexp2 < 0 >
{
public:
	enum {
		value = 1
	};
};

template<typename T> class Stats;
template<typename T> ostream& operator<< (ostream &, const Stats<T> &);

template<typename T>
class Stats
{
public:
	double mean;
	double stddev;
	T min;
	T max;
	size_t argmin;
	size_t argmax;

	Stats(T *arr, size_t size) {
		T minValue = std::numeric_limits<T>::min();
		T maxValue = std::numeric_limits<T>::max();
					
		min = maxValue;
		max = minValue;

		argmin = argmax = (size_t)(-1);	// big positive
		
		double sum = 0;
		size_t meanSize = 0;

		for (size_t i = 0; i < size; ++i) {
			T val = arr[i];

			if (!isnan(val)) {
				sum += val;
				meanSize++;
			}

			if (val < min) {
				argmin = i;
				min = val;
			}

			if (val > max) {
				argmax = i;
				max = val;
			}
		}

		mean = sum / (double)meanSize;

		double diff_squared_sum = 0.0;
		for (size_t i = 0; i < size; ++i) {
			T val = arr[i];
			double diff = val - mean;
			diff = diff * diff;
			diff_squared_sum += diff;
		}

		double mean_diff_squared = diff_squared_sum / size;

		stddev = sqrt(mean_diff_squared);
		
	}

	Stats(const Stats &other) {
		mean = other.mean;
		stddev = other.stddev;
		min = other.min;
		max = other.max;
		argmin = other.argmin;
		argmax = other.argmax;
	}

	friend ostream& operator<< <> (ostream &, const Stats<T> &);
};

template <typename T>
ostream &operator<<(ostream &os, const Stats<T> &stats) {
	os <<
		"Mean: " << stats.mean <<
		"\nStddev: " << stats.stddev <<
		"\nMin: " << stats.min <<
		"\nMax: " << stats.max <<
		"\nArgmin: " << stats.argmin <<
		"\nArgmax: " << stats.argmax << endl;
	return os;
}

template<typename T>
Stats<T> get_stats(T *arr, size_t size) {
	return Stats<T>(arr, size);
}

template<typename T>
CUDA_CALLABLE inline T deg2rad(T val) {
#ifdef __CUDA_ARCH__
	return T(val * CUDART_PI_F / 180.0);
#else
	return T(val * PI / 180.0);
#endif
}

template<typename T>
CUDA_CALLABLE inline T rad2deg(T val) {
#ifdef __CUDA_ARCH__
	return T(val * 180.0 / CUDART_PI_F);
#else
	return T(val * 180.0 / PI);
#endif
}

std::string dateTimeString();

template<typename T>
CUDA_CALLABLE T normAngle(T alpha)
{
#ifdef __CUDA_ARCH__
	const T pi = T(CUDART_PI);
#else
	const T pi = T(PI);
#endif

	return (alpha >= 0 ?
		fmod(alpha + static_cast<T>(pi),
		2.0f * static_cast<T>(pi))
		- static_cast<T>(pi)
		:
		-(fmod(static_cast<T>(pi)-alpha,
		2.0f * static_cast<T>(pi))
		- static_cast<T>(pi)));
}

template<typename T>
void exportToCSV(T *arr, int width, int height, string filename)
{
	fstream file = fstream(filename, ios_base::out);
	
	locale mylocale("");   // get global locale
	file.imbue(mylocale);  // imbue global locale

	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			file << arr[y * width + x] << ";";
		}
		file << endl;
	}

	file.close();
}

template<typename T>
void nonMaximumSuppression(T *inArr, T *outArr, int width, int height, int maxNeighborsHigher)
{
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			
			int neighborsHigherCount = 0;

			for (int y2 = y - 1; y2 <= y + 1; ++y2) {
				for (int x2 = x - 1; x2 <= x + 1; ++x2) {
					if (x2 < 0 || x2 >= width || y2 < 0 || y2 >= height || (x2 == x && y2 == y))
						continue;

					if (inArr[y2 * width + x2] >= inArr[y * width + x])
						neighborsHigherCount++;
				}
			}

			if (neighborsHigherCount <= maxNeighborsHigher)
				outArr[y * width + x] = 1;
			else
				outArr[y * width + x] = 0;
		}
	}
}

template<typename T>
CUDA_CALLABLE void fillArray(T *array, size_t size, const T &value)
{
	for (size_t i = 0; i < size; ++i) {
		array[i] = value;
	}
}

GdkPixbuf *gdk_pixbuf_new_from_cvmat(cv::Mat mat);

template<typename T>
T identityFunc(T x) {
	return x;
}

//template<typename T>
//cv::Vec3b grayscaleVec3b(T value) {
//	return cv::Vec3b(value, value, value);
//}

//template<class T, class V, class W>
//struct FuncComposition {
//	typedef T(*Func1Type)(V arg);
//	typedef V(*Func2Type)(W arg);
//
//	FuncComposition(Func1Type f1, Func2Type f2) : f1(f1), f2(f2) {}
//
//	T operator()(W arg) {
//		return f1(f2(arg));
//	}
//
//private:
//	Func1Type f1;
//	Func2Type f2;
//};
//
//template<class T, class V, class W>
//FuncComposition<T, V, W> compose(T(*f1)(V arg), V(*f2)(W arg)) {
//	return FuncComposition<T, V, W>(f1, f2);
//}

template<typename T, class TransformFunc>
void gdk_pixbuf_new_from_data_custom(T *data, int width, int height, TransformFunc transform, GdkPixbuf **normalOut, GdkPixbuf **smallOut) {
	cv::Mat normalMat = cv::Mat(height, width, CV_8UC3);

	T min = std::numeric_limits<T>::max();
	T max = std::numeric_limits<T>::lowest();

	for (int i = 0; i < width * height; ++i) {
		if (!isnan(data[i])) {
			min = MIN(min, data[i]);
			max = MAX(max, data[i]);
		}
	}

	min = transform(min);
	max = transform(max);

	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {			
			T value = (transform(data[y * width + x]) - min) / (max - min) * 255.0f;
			normalMat.at<cv::Vec3b>(y, x) = cv::Vec3b(value, value, value);
		}
	}

	if (normalOut) {
		(*normalOut) = gdk_pixbuf_new_from_cvmat(normalMat);
	}

	if (smallOut) {
		cv::Mat small;
		float scale = 60.0f / width;
		cv::resize(normalMat, small, cv::Size(), scale, scale);

		(*smallOut) = gdk_pixbuf_new_from_cvmat(small);
		small.release();
	}

	normalMat.release();
}

template<typename T, class TransformFunc>
void gdk_pixbuf_new_from_data_vec3b(T *data, int width, int height, TransformFunc transform, GdkPixbuf **normalOut, GdkPixbuf **smallOut) {
	cv::Mat normalMat = cv::Mat(height, width, CV_8UC3);
	
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			normalMat.at<cv::Vec3b>(y, x) = transform(data[y * width + x]);
		}
	}

	if (normalOut) {
		(*normalOut) = gdk_pixbuf_new_from_cvmat(normalMat);
	}

	if (smallOut) {
		cv::Mat small;
		float scale = 60.0f / width;
		cv::resize(normalMat, small, cv::Size(), scale, scale);

		(*smallOut) = gdk_pixbuf_new_from_cvmat(small);
		small.release();
	}

	normalMat.release();
}

template<typename T>
std::vector<T> g_list_to_vector(GList *list) {
	std::vector<T> vec;
	for (GList *it = list; it != NULL; it = it->next) {
		if (it->data) {
			T *data = (T*)(it->data);
			vec.push_back(*data);
		}
	}
	return vec;
}


// Time measurement

#ifdef TIME_MEASURE_ENABLED

#define TIME_MEASURE_START(name) \
clock_t time_measure_start_##name = clock();

#define TIME_MEASURE_STOP(name) \
clock_t time_measure_stop_##name = clock(); \
std::cout << "Time of " << #name << ": " << ((double)(time_measure_stop_##name - time_measure_start_##name) / CLOCKS_PER_SEC) << std::endl;

#else

#define TIME_MEASURE_START(name)
#define TIME_MEASURE_STOP(name)

#endif

#ifdef LOG_FUNCTION_CALL_ENABLED

#define LOG_FUNCTION_CALL std::cout << __FUNCTION__ << std::endl;

#else
#define LOG_FUNCTION_CALL
#endif

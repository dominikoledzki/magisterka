#include "cuFPFH.h"
#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <math_constants.h>
#include <math_functions.h>
#include <device_atomic_functions.h>
#include "device_vector.h"
#include "PointAverage.h"

static const int blocksPerGrid = 32;
static dim3 gridSize(blocksPerGrid, blocksPerGrid);

namespace FPFH {
	namespace cuFPFH {

		__global__ void _initialize(cuFPFHContext context)
		{
			int x = blockIdx.x * blockDim.x + threadIdx.x;
			int y = blockIdx.y * blockDim.y + threadIdx.y;
			const int width = context.width;

			context.spfh.valid[y * width + x] = false;
			for (int i = 0; i < context.spfh.histSize; ++i) {
				context.spfh.data[i][y * width + x] = 0.0f;
			}
		}

		void initialize(cuFPFHContext &context)
		{
			LOG_FUNCTION_CALL
			dim3 threads = dim3(context.width / blocksPerGrid, context.height / blocksPerGrid);
			dim3 blocks = gridSize;

			_initialize<<<blocks, threads>>>(context);
		}

		//===============================================================================================================//

		__global__ void _calculateNormals(cuFPFHContext context)
		{
			const int width = context.width;
			const int height = context.height;
			const float radius = context.normalsRadius;
			const float squaredRadius = radius * radius;

			const Point3r *world = context.world;

			device_vector<Point2i> pointsToCheck;
			device_vector<Point2i> touchedPoints;

			int x = blockIdx.x * blockDim.x + threadIdx.x;
			int y = blockIdx.y * blockDim.y + threadIdx.y;

			context.normals[y * width + x] = Vector3r::invalidPoint();

			Point3r p = world[y * width + x];
			if (!isValid(p))
				return;

			pointsToCheck.clear();
			touchedPoints.clear();

			pointsToCheck.push_back(Point2i(x, y));
			touchedPoints.push_back(Point2i(x, y));

			for (int i = 0; i < pointsToCheck.size(); ++i) {

				Point2i c = pointsToCheck[i];

				for (int y2 = c.y - 1; y2 <= c.y + 1; ++y2) {
					for (int x2 = c.x - 1; x2 <= c.x + 1; ++x2) {

						if (x2 == c.x && y2 == c.y)
							continue;

						if (x2 < 0 || x2 >= width || y2 < 0 || y2 >= height)
							continue;

						if (touchedPoints.contains(Point2i(x2, y2)))
							continue;

						touchedPoints.push_back(Point2i(x2, y2));

						if (!isValid(world[y2 * width + x2]))
							continue;

						Point3r p2 = world[y2 * width + x2];
						if ((p2 - p).squaredNorm() > squaredRadius)
							continue;

						pointsToCheck.push_back(Point2i(x2, y2));
					}
				}
			}

			if (pointsToCheck.size() >= 3) {
				PointAverage average;
				for (size_t i = 0; i < pointsToCheck.size(); ++i) {
					const Point2i &ip = pointsToCheck[i];
					const Point3r &p = world[ip.y * width + ip.x];
					average.addSample(p);
				}

				Vector3r normal, eigenVector2, eigenVector3;
				Matrix<float, 3, 1> eigenValues;
				average.pca(eigenValues, normal, eigenVector2, eigenVector3);

				Vector3r viewingDirection = -p;
				if (normal.dotProduct(viewingDirection) < 0.0f)
					normal = -normal;

				context.normals[y * width + x] = normal;
			}
		}

		void calculateNormals(cuFPFHContext &context)
		{
			LOG_FUNCTION_CALL
			dim3 threads = dim3(context.width / blocksPerGrid, context.height / blocksPerGrid);
			dim3 blocks = gridSize;

			_calculateNormals<<<blocks, threads>>>(context);
		}

		//===============================================================================================================//

		__device__ Point2i point2dAt(const Point3f &p, int width, int height, float xzFactor, float yzFactor)
		{
			float normalizedX = p.x / (p.z * xzFactor);
			float normalizedY = p.y / (p.z * yzFactor);

			Point2i d;
			d.x = static_cast<int>(width * (normalizedX + 0.5f));
			d.y = static_cast<int>(height * (0.5f - normalizedY));
			return d;
		}

		__global__ void _calculateBounds(cuFPFHContext context, int *minY, int *maxY, int *minX, int *maxX, float radius)
		{
			const int width = context.width;
			const int height = context.height;
			const float squaredRadius = radius * radius;

			int x = blockIdx.x * blockDim.x + threadIdx.x;
			int y = blockIdx.y * blockDim.y + threadIdx.y;

			int index = y * width + x;

			const Point3r *world = context.world;

			const Point3r &p = world[index];

			if (!isValid(p)) {
				return;
			}

			Point3r edgePoints[4];

			{
				float a1 = (p.z * p.x - radius * sqrtf(p.z * p.z + p.x * p.x - radius * radius)) / (p.z * p.z - radius * radius);
				float a2 = (p.z * p.x + radius * sqrtf(p.z * p.z + p.x * p.x - radius * radius)) / (p.z * p.z - radius * radius);

				float z1 = (p.z + a1*p.x) / (a1*a1 + 1);
				float z2 = (p.z + a2*p.x) / (a2*a2 + 1);

				float x1 = a1 * z1;
				float x2 = a2 * z2;

				edgePoints[0] = Point3r(x1, p.y, z1);
				edgePoints[1] = Point3r(x2, p.y, z2);
			}

				{
					float a1 = (p.z * p.y - radius * sqrtf(p.z * p.z + p.y * p.y - radius * radius)) / (p.z * p.z - radius * radius);
					float a2 = (p.z * p.y + radius * sqrtf(p.z * p.z + p.y * p.y - radius * radius)) / (p.z * p.z - radius * radius);

					float z1 = (p.z + a1*p.y) / (a1*a1 + 1);
					float z2 = (p.z + a2*p.y) / (a2*a2 + 1);

					float y1 = a1 * z1;
					float y2 = a2 * z2;

					edgePoints[2] = Point3r(p.x, y1, z1);
					edgePoints[3] = Point3r(p.x, y2, z2);
				}

				minY[index] = height;
				maxY[index] = 0;
				minX[index] = width;
				maxX[index] = 0;

				for (int i = 0; i < 4; ++i) {
					Point2i pd = point2dAt(edgePoints[i], width, height, context.xzFactor, context.yzFactor);
					minY[index] = MIN(minY[index], pd.y);
					maxY[index] = MAX(maxY[index], pd.y);

					minX[index] = MIN(minX[index], pd.x);
					maxX[index] = MAX(maxX[index], pd.x);
				}

				minY[index] = MAX(minY[index], 0);
				maxY[index] = MIN(maxY[index], height - 1);

				minX[index] = MAX(minX[index], 0);
				maxX[index] = MIN(maxX[index], width - 1);
		}

		__global__ void _calculateNormals2(cuFPFHContext context, int *minY, int *maxY, int *minX, int *maxX)
		{
			const int width = context.width;
			const int height = context.height;
			const float radius = context.normalsRadius;
			const float squaredRadius = radius * radius;

			const Point3r *world = context.world;

			int x = blockIdx.x * blockDim.x + threadIdx.x;
			int y = blockIdx.y * blockDim.y + threadIdx.y;

			int index = y * width + x;

			context.normals[index] = Vector3r::invalidPoint();

			Point3r p = world[index];
			if (!isValid(p))
				return;

			PointAverage average;

			for (int y2 = minY[index]; y2 <= maxY[index]; ++y2) {
				for (int x2 = minX[index]; x2 <= maxX[index]; ++x2) {
					const Point3r &p2 = world[y2 * width + x2];

					if ((p - p2).squaredNorm() < squaredRadius)
						average.addSample(p2);
				}
			}

			if (average.getNoOfSamples() >= 3) {
				Vector3r normal, eigenVector2, eigenVector3;
				Matrix<float, 3, 1> eigenValues;
				average.pca(eigenValues, normal, eigenVector2, eigenVector3);

				Vector3r viewingDirection = -p;
				if (normal.dotProduct(viewingDirection) < 0.0f)
					normal = -normal;

				context.normals[y * width + x] = normal;
			}
		}

		void calculateNormals2(cuFPFHContext &context)
		{
			LOG_FUNCTION_CALL
			dim3 threads = dim3(context.width / blocksPerGrid, context.height / blocksPerGrid);
			dim3 blocks = gridSize;

			int *minY;
			int *maxY;
			int *minX;
			int *maxX;

			size_t s = sizeof(int) * context.width * context.height;
			cudaMalloc(&minY, s);
			cudaMalloc(&maxY, s);
			cudaMalloc(&minX, s);
			cudaMalloc(&maxX, s);

			_calculateBounds<<<blocks, threads>>>(context, minY, maxY, minX, maxX, context.normalsRadius);
			_calculateNormals2<<<blocks, threads>>>(context, minY, maxY, minX, maxX);

			cudaFree(minY);
			cudaFree(maxY);
			cudaFree(minX);
			cudaFree(maxX);
		}

		//===============================================================================================================//

		__device__ Features _computeFeatures(const Point3r &p, const Vector3r &pn, const Point3r &q, const Vector3r &qn)
		{
			Point3r pi = p;
			Vector3r ni = pn;
			Point3r pj = q;
			Vector3r nj = qn;

			Point3r ps, pt;
			Vector3r ns, nt;

			float distance = (pj - pi).norm();
			float anglei = acos(ni.dotProduct(pj-pi)/distance);
			float anglej = acos(nj.dotProduct(pi-pj)/distance);

			if (anglei < anglej)
			{
				ps = pi;
				pt = pj;

				ns = ni;
				nt = nj;
			}
			else
			{
				ps = pj;
				pt = pi;

				ns = nj;
				nt = ni;
			}


			// Darboux frame
			Vector3r u = ns;
			Vector3r v = (pt-ps).normalized().crossProduct(u);
			Vector3r w = u.crossProduct(v);

			Features features;
			features.alpha = v.dotProduct(nt);
			features.phi = u.dotProduct(pt - ps)/(pt-ps).norm();
			features.theta = atan2f(w.dotProduct(nt), u.dotProduct(nt));

			return features;
		}

		__global__ void _calculateSPFHs(cuFPFHContext context)
		{
			const int width = context.width;
			const int height = context.height;

			int x = blockIdx.x * blockDim.x + threadIdx.x;
			int y = blockIdx.y * blockDim.y + threadIdx.y;

			const Point3r &p = context.world[y * width + x];
			if (!isValid(p))
				return;

			const Vector3r &n = context.normals[y * width + x];
			if (!isValid(n))
				return;

			for (int y2 = y - 1; y2 <= y + 1; ++y2) {
				if (y2 < 0 || y2 >= height)
					continue;

				for (int x2 = x - 1; x2 <= x + 1; ++x2) {
					if (x2 < 0 || x2 >= width)
						continue;

					if (x2 == x && y2 == y)
						continue;

					const Point3r &p2 = context.world[y2 * width + x2];
					if (!isValid(p2))
						continue;

					const Vector3r &n2 = context.normals[y2 * width + x2];
					if (!isValid(n2))
						continue;

					Features f = _computeFeatures(p, n, p2, n2);
					context.spfh.addFeatures(f, x, y);
					context.spfh.setValid(true, x, y);
				}
			}
		}

		__global__ void _normalizeSPFHs(cuFPFHContext context)
		{
			const int width = context.width;
			int x = blockIdx.x * blockDim.x + threadIdx.x;
			int y = blockIdx.y * blockDim.y + threadIdx.y;

			if (!context.spfh.valid[y * width + x])
				return;

			float sum = 0.0f;
			for (int i = 0; i < context.spfh.histSize; ++i) {
				sum += context.spfh.data[i][y * width + x];
			}

			if (sum == 0.0f)
				return;

			float factor = 1.0f / sum;
			for (int i = 0; i < context.spfh.histSize; ++i) {
				context.spfh.data[i][y * width + x] *= factor;
			}
		}

		void calculateSPFHs(cuFPFHContext &context)
		{
			LOG_FUNCTION_CALL

			dim3 threads = dim3(context.width / blocksPerGrid, context.height / blocksPerGrid);
			dim3 blocks = gridSize;

			_calculateSPFHs<<<blocks, threads>>>(context);
			_normalizeSPFHs<<<blocks, threads>>>(context);
		}

		//===============================================================================================================//

		__global__ void _calculateFPFHs(cuFPFHContext context)
		{
			const int width = context.width;
			const int height = context.height;
			const float radius = context.radius;
			const float squaredRadius = radius * radius;
			int x = blockIdx.x * blockDim.x + threadIdx.x;
			int y = blockIdx.y * blockDim.y + threadIdx.y;

			context.fpfh.valid[y * width + x] = context.spfh.valid[y * width + x];
			for (int i = 0; i < context.spfh.histSize; ++i) {
				context.fpfh.data[i][y * width + x] = context.spfh.data[i][y * width + x];
			}

			const Point3r *world = context.world;

			device_vector<Point2i> pointsToCheck;
			device_vector<Point2i> touchedPoints;

			Point3r p = world[y * width + x];
			if (!isValid(p))
				return;

			pointsToCheck.clear();
			touchedPoints.clear();

			pointsToCheck.push_back(Point2i(x, y));
			touchedPoints.push_back(Point2i(x, y));

			for (int i = 0; i < pointsToCheck.size(); ++i) {

				Point2i c = pointsToCheck[i];

				for (int y2 = c.y - 1; y2 <= c.y + 1; ++y2) {
					for (int x2 = c.x - 1; x2 <= c.x + 1; ++x2) {

						if (x2 == c.x && y2 == c.y)
							continue;

						if (x2 < 0 || x2 >= width || y2 < 0 || y2 >= height)
							continue;

						if (touchedPoints.contains(Point2i(x2, y2)))
							continue;
						touchedPoints.push_back(Point2i(x2, y2));

						if (!isValid(world[y2 * width + x2]))
							continue;

						if (!context.spfh.isValid(x2, y2))
							continue;

						Point3r p2 = world[y2 * width + x2];
						if ((p2 - p).squaredNorm() > squaredRadius)
							continue;

						pointsToCheck.push_back(Point2i(x2, y2));
					}
				}
			}

			float scale = 1.0f / pointsToCheck.size();
			for (int i = 0; i < pointsToCheck.size(); ++i) {
				const Point2i &p2i = pointsToCheck[i];

				if (p2i.x == x && p2i.y == y)
					continue;

				const Point3r &p2 = world[p2i.y * width + p2i.x];
				float dist = (p-p2).norm();

				for (int i = 0; i < context.fpfh.histSize; ++i) {
					float k = scale / dist;
					context.fpfh.data[i][y * width + x] += k * context.spfh.data[i][p2i.y * width + p2i.x];
				}
			}
		}

		__global__ void _calculateFPFHs2(cuFPFHContext context, int *minY, int *maxY, int *minX, int *maxX)
		{
			const int width = context.width;
			const int height = context.height;
			const float radius = context.radius;
			const float squaredRadius = radius * radius;
			int x = blockIdx.x * blockDim.x + threadIdx.x;
			int y = blockIdx.y * blockDim.y + threadIdx.y;

			int index = y * width + x;

			context.fpfh.valid[y * width + x] = context.spfh.valid[y * width + x];
			for (int i = 0; i < context.spfh.histSize; ++i) {
				context.fpfh.data[i][y * width + x] = context.spfh.data[i][y * width + x];
			}

			const Point3r *world = context.world;

			Point3r p = world[y * width + x];
			if (!isValid(p))
				return;

			int pointsCount = 0;
			for (int y2 = minY[index]; y2 <= maxY[index]; ++y2) {
				for (int x2 = minX[index]; x2 <= maxX[index]; ++x2) {
					const Point3r &p2 = world[y2 * width + x2];

					if ((p-p2).squaredNorm() <= squaredRadius)
						pointsCount++;
				}
			}

			float scale = 1.0f / (float)(pointsCount);

			for (int y2 = minY[index]; y2 <= maxY[index]; ++y2) {
				for (int x2 = minX[index]; x2 <= maxX[index]; ++x2) {
					const Point3r &p2 = world[y2 * width + x2];

					if (x2 == x && y2 == y)
						continue;

					float dist = (p-p2).norm();
					for (int i = 0; i < context.fpfh.histSize; ++i) {
						float k = scale / dist;
						context.fpfh.data[i][y * width + x] += k * context.spfh.data[i][y2 * width + x2];
					}
				}
			}
		}

		__global__ void _normalizeFPFHs(cuFPFHContext context)
		{
			const int width = context.width;
			int x = blockIdx.x * blockDim.x + threadIdx.x;
			int y = blockIdx.y * blockDim.y + threadIdx.y;

			if (!context.fpfh.valid[y * width + x])
				return;

			float sum = 0.0f;
			for (int i = 0; i < context.fpfh.histSize; ++i) {
				sum += context.fpfh.data[i][y * width + x];
			}

			if (sum == 0.0f)
				return;

			float factor = 1.0f / sum;
			for (int i = 0; i < context.fpfh.histSize; ++i) {
				context.fpfh.data[i][y * width + x] *= factor;
			}
		}

		void calculateFPFHs(cuFPFHContext &context)
		{
			LOG_FUNCTION_CALL

			dim3 threads = dim3(context.width / blocksPerGrid, context.height / blocksPerGrid);
			dim3 blocks = gridSize;

			_calculateFPFHs<<<blocks, threads>>>(context);
			_normalizeFPFHs<<<blocks, threads>>>(context);
		}

		void calculateFPFHs2(cuFPFHContext &context)
		{
			LOG_FUNCTION_CALL

			dim3 threads = dim3(context.width / blocksPerGrid, context.height / blocksPerGrid);
			dim3 blocks = gridSize;

			int *minY;
			int *maxY;
			int *minX;
			int *maxX;

			size_t s = sizeof(int) * context.width * context.height;
			cudaMalloc(&minY, s);
			cudaMalloc(&maxY, s);
			cudaMalloc(&minX, s);
			cudaMalloc(&maxX, s);

			_calculateBounds<<<blocks, threads>>>(context, minY, maxY, minX, maxX, context.radius);
			_calculateFPFHs2<<<blocks, threads>>>(context, minY, maxY, minX, maxX);
			_normalizeFPFHs<<<blocks, threads>>>(context);

			cudaFree(minY);
			cudaFree(maxY);
			cudaFree(minX);
			cudaFree(maxX);
		}

	}
}

#pragma once
#include "DataTypes.h"
#include "cuFPFHContext.h"

namespace FPFH { 
	namespace cuFPFH {

		void initialize(cuFPFHContext &context);
		void calculateNormals(cuFPFHContext &context);
		void calculateNormals2(cuFPFHContext &context);
		void calculateSPFHs(cuFPFHContext &context);
		void calculateFPFHs(cuFPFHContext &context);
		void calculateFPFHs2(cuFPFHContext &context);
	}
}
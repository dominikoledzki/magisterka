#include "cuFPFHContext.h"
#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "FPFHContext.h"

namespace FPFH {
	namespace cuFPFH {

		cuFPFHContext::cuFPFHContext(int width, int height, unsigned binsPerFeature, Point3r *inWorld) :
			width(width),
			height(height),
			binsPerFeature(binsPerFeature),
			spfh(binsPerFeature, width, height, true),
			fpfh(binsPerFeature, width, height, true)
		{
			const int size = width * height;
			
			CUDA_SAFE_CALL(cudaMalloc(&world, size * sizeof(Point3r)));
			if (inWorld)
				CUDA_SAFE_CALL(cudaMemcpy(world, inWorld, size * sizeof(Point3r), cudaMemcpyHostToDevice));
			CUDA_SAFE_CALL(cudaMalloc(&normals, size * sizeof(Vector3r)));
		}

		cuFPFHContext::cuFPFHContext(const FPFHContext &context) :
			width(context.width),
			height(context.height),
			binsPerFeature(context.binsPerFeature),
			spfh(context.spfh, true),
			fpfh(context.fpfh, true)
		{
			const int size = width * height;

			CUDA_SAFE_CALL(cudaMalloc(&world, size * sizeof(Point3r)));
			CUDA_SAFE_CALL(cudaMemcpy(world, context.world, size * sizeof(Point3r), cudaMemcpyHostToDevice));

			CUDA_SAFE_CALL(cudaMalloc(&normals, size * sizeof(Vector3r)));
			CUDA_SAFE_CALL(cudaMemcpy(normals, context.normals, size * sizeof(Vector3r), cudaMemcpyHostToDevice));


		}

		cuFPFHContext::~cuFPFHContext(){}

		void cuFPFHContext::dispose()
		{
			cudaFree(world);
			cudaFree(normals);

			world = normals = nullptr;

			spfh.dispose();
			fpfh.dispose();
		}

	}
}

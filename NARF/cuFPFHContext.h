#pragma once
#include "DataTypes.h"
#include "FPFHCommon.h"

using namespace NARF;

namespace FPFH {

	class FPFHContext;
	namespace cuFPFH {

		class cuFPFHContext
		{
		public:
			cuFPFHContext(int width, int height, unsigned binsPerFeature, Point3r *world);
			cuFPFHContext(const FPFHContext &context);
			~cuFPFHContext();
			void dispose();


			// input data
			int width;
			int height;
			float xzFactor;
			float yzFactor;
			Point3r *world;

			// parameters
			unsigned binsPerFeature;
			float normalsRadius;
			float radius;

			// intermediate and output data
			Vector3r *normals;
			SPFH spfh;
			SPFH fpfh;
		};
	}
}

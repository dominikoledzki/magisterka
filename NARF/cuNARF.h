#pragma once
#include "DataTypes.h"
#include "BorderExtractor.h"

namespace NARF
{
	class NARFContext;

	namespace cuNARF {

		class cuNARFContext
		{
		public:
			cuNARFContext(int width, int height, real xzFactor, real yzFactor, depth_t *hostDepth);
			cuNARFContext(const NARFContext &cpuContext);

			void setParameters(const NARFParameters &params);
			void dispose();

			// input data
			int width;
			int height;
			real xzFactor;
			real yzFactor;
			depth_t *depth;

			// parameters
			int supportingSize;
			int pointsUsedForAverage;
			int smoothingRadius;
			int reduceBorderSearchingDistance;
			real borderScoreThreshold;
			int localSurfaceRadius;
			int localSurfaceNumerOfClosestNeighbors;

			// interest points parameters
			int pixelRadiusBorderDirections;
			real minimumBorderProbability;
			int minimumWeight;
			int mainPrincipalCurvatureRadius;
			real minimumInterestValue;
			real searchRadius;
			real optimalDistanceToHighSurfaceChange;
			bool addPointsOnStraightEdges;
			bool doNonMaximumSuppression;
			int numberOfPolynomialApproximations;
			int maxNumberOfInterestPoints;
			real minDistanceBetweenInterestPoints;

			// intermediate and output data
			Point3r *world;
			real *typical3dDistance;
			real *borderScore[4];
			real *avgNeighborRange[4];
			real *smoothBorderScore[4];
			BorderExtractor::PotentialBorderType *potentialBorderType[4];
			BorderExtractor::BorderType *borderType[4];
			real *secondBorderScore[4];
			unsigned int *borderTypeWithDirection;
			LocalSurface *localSurface;

			// interest points extraction data
			Vector3r *borderDirections;
			Vector3r *averageBorderDirections;
			Vector3r *surfaceChangeDirection;
			real *surfaceChangeScore;
			real *interestImage;
			bool *isInterestPoint;
			InterestPoint *tmpInterestPoints;
			unsigned int *tmpInterestPointsSize;

			std::vector<InterestPoint> hostTmpInterestPoints;
			std::vector<InterestPoint> hostInterestPoints;
			bool *hostIsInterestPoint;
		};

		void initialize(cuNARFContext &context);
		void depthToWorld(cuNARFContext &context);
		void calculateTypical3DDistance(cuNARFContext &context);
		void calculateBorderScore(cuNARFContext &context);
		void smoothBorderScore(cuNARFContext &context);
		void firstClassification(cuNARFContext &context);
		void secondClassification(cuNARFContext &context);
		void cumulateBorderType(cuNARFContext &context);
		void calculateLocalSurfaceParameters(cuNARFContext &context);

		// interest points extraction
		void calculateBorderDirections(cuNARFContext &context);
		void calculateAverageBorderDirections(cuNARFContext &context);
		void calculateSurfaceChanges(cuNARFContext &context);
		void calculateInterestImage(cuNARFContext &context);
		void calculateInterestPoints(cuNARFContext &context);
		void sortAndFilterInterestPoints(cuNARFContext &context);

		// utilities
		template<typename T>
		void copyToHost(T **dst, T *src) {
			int dataSize = sizeof(T) * width * height;
			(*dst) = new T[width * height];
			cudaError_t err = cudaMemcpy((*dst), src, dataSize, cudaMemcpyDeviceToHost);

		}
	}
}


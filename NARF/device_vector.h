#pragma once
#include <cuda.h>
#include "Utilities.h"

template<typename T>
class device_vector
{
public:
	CUDA_CALLABLE device_vector() : _data(nullptr), _size(0), _capacity(0)
	{}

	CUDA_CALLABLE ~device_vector()
	{
		clear();
	}

	CUDA_CALLABLE size_t size() const
	{
		return _size;
	}

	CUDA_CALLABLE void push_back(const T &item) 
	{
		increaseSize();
		_data[_size++] = item;
	}

	CUDA_CALLABLE void pop_back()
	{
		_size--;
		decreaseSize();
	}

	CUDA_CALLABLE void clear()
	{
		delete[] _data;
		_data = nullptr;
		_size = 0;
		_capacity = 0;
	}

	CUDA_CALLABLE bool contains(const T &item) const
	{
		for (size_t i = 0; i < _size; ++i)
		{
			if (_data[i] == item)
				return true;
		}

		return false;
	}

	CUDA_CALLABLE void resize(size_t n, const T &val)
	{
		while (_size < n)
			push_back(val);

		while (_size > n)
			pop_back();
	}

	CUDA_CALLABLE const T &operator[](size_t idx) const
	{
		return _data[idx];
	}

	CUDA_CALLABLE T &operator[](size_t idx)
	{
		return _data[idx];
	}

private:
	T *_data;
	size_t _size;
	size_t _capacity;

	CUDA_CALLABLE void increaseSize()
	{
		if (_capacity == 0) {
			_data = new T[1];
			_capacity = 1;
		}
		else if (_size + 1 > _capacity / 2)
		{
			T *newData = new T[_capacity * 2];
			memcpy(newData, _data, sizeof(T) * _size);
			delete[] _data;

			_data = newData;
			_capacity *= 2;
		}
	}

	CUDA_CALLABLE void decreaseSize()
	{
		if (_size <= _capacity / 4)
		{
			T *newData = new T[_capacity / 2];
			memcpy(newData, _data, sizeof(T) * _size);
			delete[] _data;

			_data = newData;
			_capacity /= 2;
		}
	}

	friend void swap<T>(device_vector<T> &vec1, device_vector<T> &vec2);
};

template<typename T>
CUDA_CALLABLE void swap(device_vector<T> &vec1, device_vector<T> &vec2)
{
	T *tempData = vec1._data;
	size_t tempSize = vec1._size;
	size_t tempCapacity = vec1._capacity;

	vec1._data = vec2._data;
	vec1._size = vec2._size;
	vec1._capacity = vec2._capacity;

	vec2._data = tempData;
	vec2._size = tempSize;
	vec2._capacity = tempCapacity;
}

template<typename T>
class shared_device_vector : public device_vector<T>
{
public:
	CUDA_CALLABLE shared_device_vector() : device_vector()
	{}

	CUDA_CALLABLE ~shared_device_vector()
	{
		clear();
	}


};
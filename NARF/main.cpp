#define LOG_FUNCTION_CALL_ENABLED
#define TIME_MEASURE_ENABLED

#define EXPORT extern "C" G_MODULE_EXPORT

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include "Recording.h"
#include "BorderExtractor.h"
//#include "InterestPointExtractor.h"
#include "Utilities.h"
#include "DataTypes.h"
//#include "BivariatePolynomial2.h"
#include <ctime>
#include <cuda_runtime.h>
#include "cuNARF.h"
#include "NARFContext.h"
#include "NARF.h"
#include "Compare.h"
#include <opencv2\opencv.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <boost/filesystem.hpp>
#include <gsl/gsl_errno.h>
#include "FPFH.h"
#include "cuFPFH.h"
#include <random>
#include <array>

#include <gtk/gtk.h>
#include <cairo.h>
#include <gmodule.h>

#include "UICommons.h"
#include "Dialogs.h"

using namespace std;
using namespace NARF;
using namespace FPFH;
using namespace cv;

namespace fs = boost::filesystem;

float support_size = 0.2f;

void beforeExit() {
	cudaDeviceReset();
	system("PAUSE");
}

float histogramDistance(float **arr1, float **arr2, NARF::Point2i p1, NARF::Point2i p2, int width, int bins) {
	// euclide
	float sum = 0.0f;
	for (int i = 0; i < bins; ++i) {
		float diff = arr1[i][p1.y * width + p1.x] - arr2[i][p2.y * width + p2.x];
		sum += diff * diff;
	}
	return sqrtf(sum);

	//// Vector Cosine Angle Distance
	//float sum1 = 0.0f;
	//float sum2 = 0.0f;
	//float sum3 = 0.0f;

	//int index1 = p1.y * width + p1.x;
	//int index2 = p2.y * width + p2.x;

	//for (int i = 0; i < bins; ++i) {
	//	sum1 += arr1[i][index1] * arr2[i][index2];
	//	sum2 += arr1[i][index1] * arr1[i][index1];
	//	sum3 += arr2[i][index2] * arr2[i][index2];
	//}

	//return sqrt(sum2) * sqrt(sum3) / sum1;
}

float histogramDistance(std::array<float, 33> hist1, std::array<float, 33> hist2) {
	float sum = 0.0f;
	for (int i = 0; i < 33; ++i) {
		float diff = hist1[i] - hist2[i];
		sum += diff * diff;
	}
	return sqrtf(sum);
}

Point3r pointAt(int x, int y, depth_t z, float xzFactor, float yzFactor, int width, int height)
{
	float x3d = ((float)x/(float)width - 0.5f) * z * xzFactor;
	float y3d = (0.5f - (float)y/(float)height) * z * yzFactor;
	float z3d = z;

	return Point3r(x3d, y3d, z3d);
}

NARF::Point2i point2DAt(float x, float y, float z, float xzFactor, float yzFactor, int width, int height)
{
	int x2d = (x / (z * xzFactor) + 0.5f) * width;
	int y2d = (0.5f - y / (z * yzFactor)) * height;

	return NARF::Point2i(x2d, y2d);
}

int mainFPFHQuality(int argc, char **argv) {

	string folderName = argv[1];

	gsl_set_error_handler_off();

	const int bins = 11;

	NARFParameters narfParameters;
	narfParameters.supportingSize = 5;
	narfParameters.pointsUsedForAverage = 7;
	narfParameters.smoothingRadius = 2;
	narfParameters.reduceBorderSearchingDistance = 5;
	narfParameters.borderScoreThreshold = 0.7f;
	narfParameters.localSurfaceRadius = 5;
	narfParameters.localSurfaceNumerOfClosestNeighbors = 10;

	narfParameters.pixelRadiusBorderDirections = 3;
	narfParameters.minimumBorderProbability = 0.25f;
	narfParameters.minimumWeight = 3;
	narfParameters.mainPrincipalCurvatureRadius = 5;
	narfParameters.minimumInterestValue = 0.45;
	narfParameters.searchRadius = 50;
	narfParameters.optimalDistanceToHighSurfaceChange = 0.25f;
	//narfParameters.addPointsOnStraightEdges = true;
	narfParameters.doNonMaximumSuppression = true;
	narfParameters.numberOfPolynomialApproximations = 1;
	narfParameters.minDistanceBetweenInterestPoints = 5;
	//narfParameters.maxNumberOfInterestPoints = 1000;

	std::vector<NARF::InterestPoint> firstSet;
	std::vector<Point3r> points;
	std::vector<std::array<float, 3 * bins> > interestPointsFPFH;


	string camera = folderName + "livingRoom0.gt.freiburg";
	fstream cameraFile = fstream(camera, ios_base::in);

	cv::Mat firstRGBFrame;

	bool firstFrame = true;

	cudaThreadSetLimit(cudaLimitMallocHeapSize, 512*1024*1024);

	int step = 1;
	for (int i = 0; i < 1000; i += step) {

		stringstream ss;
		ss << i;

		string filename = folderName + "depth/" + ss.str() + ".png";
		string rgbFile = folderName + "rgb/" + ss.str() + ".png";

		int indx;
		float tx, ty, tz, qx, qy, qz, qw;

		cameraFile >> indx >> tx >> ty >> tz >> qx >> qy >> qz >> qw;
		
		for (int s = 1; s < step; ++s) {
			int sindx;
			float stx, sty, stz, sqx, sqy, sqz, sqw;
			cameraFile >> sindx >> stx >> sty >> stz >> sqx >> sqy >> sqz >> sqw;
		}

		Vector3r translation = Vector3r(tx, ty, tz) * 1000;
		Quaternion rot = Quaternion(qw, qx, qy, qz);

		//cudaDeviceReset();
		

		float factor = 5.0f;

		Mat image = imread(filename, cv::IMREAD_ANYCOLOR | cv::IMREAD_ANYDEPTH);
		image /= factor;

		depth_t *data = (depth_t *)image.data;

		const int width = 640;
		const int height = 480;
		const float focalLenght = 480;

		cv::Mat rgbImage = cv::imread(rgbFile);

		if (firstFrame) {
			firstFrame = false;

			firstRGBFrame = rgbImage;


			//////
			cuNARF::cuNARFContext gpuContext = cuNARF::cuNARFContext(width, height, width / focalLenght, height / focalLenght, data);

			gpuContext.setParameters(narfParameters);

			cuNARF::initialize(gpuContext);
			cuNARF::depthToWorld(gpuContext);
			cuNARF::calculateTypical3DDistance(gpuContext);
			cuNARF::calculateBorderScore(gpuContext);
			cuNARF::smoothBorderScore(gpuContext);
			cuNARF::firstClassification(gpuContext);
			cuNARF::secondClassification(gpuContext);
			cuNARF::cumulateBorderType(gpuContext);
			cuNARF::calculateLocalSurfaceParameters(gpuContext);
			cuNARF::calculateBorderDirections(gpuContext);
			cuNARF::calculateAverageBorderDirections(gpuContext);
			cuNARF::calculateSurfaceChanges(gpuContext);
			cuNARF::calculateInterestImage(gpuContext);
			cuNARF::calculateInterestPoints(gpuContext);
			cuNARF::sortAndFilterInterestPoints(gpuContext);
			NARFContext deviceContext = NARFContext(gpuContext);

			firstSet = deviceContext.interestPoints;

			
			/////
			/*
			firstSet.clear();
			InterestPoint ip;
			ip.interestValue = 1.0f;
			ip.point2d = NARF::Point2i(304, 318);
			ip.point3d = pointAt(304, 318, image.at<depth_t>(318, 304), width / focalLenght, height / focalLenght, width, height);
			firstSet.push_back(ip);
			*/
			//////

			points.clear();
			interestPointsFPFH.clear();

			for (InterestPoint p : firstSet) {
				Point3r pp = p.point3d;

				pp = (rot * pp * rot.conj()).toVector();
				pp += translation;

				points.push_back(pp);
			}

			////////////

			TIME_MEASURE_START(FPFH_TIME);

			cuFPFH::cuFPFHContext gpuFPFHContext = cuFPFH::cuFPFHContext(width, height, bins, deviceContext.world);
			gpuFPFHContext.normalsRadius = 50.0f;
			gpuFPFHContext.radius = 20.0f;
			gpuFPFHContext.xzFactor = width / focalLenght;
			gpuFPFHContext.yzFactor = height / focalLenght;

			cuFPFH::initialize(gpuFPFHContext);
			cuFPFH::calculateNormals2(gpuFPFHContext);
			cuFPFH::calculateSPFHs(gpuFPFHContext);
			cuFPFH::calculateFPFHs2(gpuFPFHContext);

			TIME_MEASURE_STOP(FPFH_TIME);

			FPFH::FPFHContext gpuCopy(gpuFPFHContext);

			for (InterestPoint p : firstSet) {
				std::array<float, 3*bins> hist;

				for (int i = 0; i < 3 * bins; ++i) {
					hist[i] = gpuCopy.fpfh.data[i][p.point2d.y * width + p.point2d.x];
				}

				interestPointsFPFH.push_back(hist);
			}

			gpuContext.dispose();
			deviceContext.dispose();

			gpuFPFHContext.dispose();
			gpuCopy.dispose();
			
		}
		else {

			// map interest points from first frame to this frame
			std::vector<Point3r> currentPoints;
			std::vector<NARF::Point2i> current2DPoints;

			for (Point3r p : points) {
				Point3r pp = p;

				pp -= translation;
				pp = (rot.conj() * pp * rot).toVector();
				currentPoints.push_back(pp);

				NARF::Point2i p2d = point2DAt(pp.x, pp.y, pp.z, width / focalLenght, height / focalLenght, width, height);
				current2DPoints.push_back(p2d);
			}


			cuNARF::cuNARFContext gpuContext = cuNARF::cuNARFContext(width, height, width / focalLenght, height / focalLenght, data);
			gpuContext.setParameters(narfParameters);
			cuNARF::depthToWorld(gpuContext);

			NARFContext deviceContext = NARFContext(gpuContext);

			TIME_MEASURE_START(FPFH_TIME);

			cuFPFH::cuFPFHContext gpuFPFHContext = cuFPFH::cuFPFHContext(width, height, bins, deviceContext.world);
			gpuFPFHContext.normalsRadius = 50.0f;
			gpuFPFHContext.radius = 20.0f;
			gpuFPFHContext.xzFactor = width / focalLenght;
			gpuFPFHContext.yzFactor = height / focalLenght;

			cuFPFH::initialize(gpuFPFHContext);
			cuFPFH::calculateNormals2(gpuFPFHContext);
			cuFPFH::calculateSPFHs(gpuFPFHContext);
			cuFPFH::calculateFPFHs2(gpuFPFHContext);

			TIME_MEASURE_STOP(FPFH_TIME);

			FPFH::FPFHContext gpuCopy(gpuFPFHContext);

			std::vector<std::array<float, 3 * bins> > currentPointsFPFH;
			for (NARF::Point2i p : current2DPoints) {
				std::array<float, 3*bins> hist;

				if (p.y < 0 || p.y >= height || p.x < 0 || p.x >= width) {
					for (int i = 0; i < 3 * bins; ++i) {
						hist[i] = 0.0f;
					}

				} else {
					for (int i = 0; i < 3 * bins; ++i) {
						hist[i] = gpuCopy.fpfh.data[i][p.y * width + p.x];
					}
				}

				currentPointsFPFH.push_back(hist);
			}

			int numberOfValidPoints = 0;
			double sumError = 0.0;
			for (int k = 0; k < interestPointsFPFH.size(); ++k) {

				NARF::Point2i p = current2DPoints[k];
				if (p.y < 0 || p.y >= height || p.x < 0 || p.x >= width)
					continue;

				numberOfValidPoints++;
				sumError += histogramDistance(interestPointsFPFH[k], currentPointsFPFH[k]);
			}
			sumError /= numberOfValidPoints;
			cout << "#" << i << "\t" << sumError << "\t" << numberOfValidPoints << endl;
			cerr << "#" << i << "\t" << sumError << "\t" << numberOfValidPoints << endl;

			gpuContext.dispose();
			deviceContext.dispose();

			gpuFPFHContext.dispose();
			gpuCopy.dispose();


			Mat firstImageWithIP = firstRGBFrame;
			for (InterestPoint ip : firstSet) {
				cv::circle(
					firstImageWithIP,
					cv::Point(ip.point2d.x, ip.point2d.y),
					1,
					cv::Scalar(255,0,0,255),
					-1
					);
			}

			Mat currentImageWithIP = rgbImage;
			for (auto ip : current2DPoints) {
				cv::circle(
					currentImageWithIP,
					cv::Point(ip.x, ip.y),
					1,
					cv::Scalar(255, 0, 0, 255),
					-1
					);
			}

			Mat img;
			hconcat(firstImageWithIP, currentImageWithIP, img);
			
			imshow("image", img);

		}


		/*for (auto it = deviceContext.interestPoints.begin(); it != deviceContext.interestPoints.end(); ++it) {
			cv::circle(
				rgbImage,
				cv::Point(it->point2d.x, it->point2d.y),
				1,
				cv::Scalar(255, 0, 0, 255),
				-1
				);
		}*/

		//cv::imshow("aa", rgbImage);
		cudaDeviceReset();
		//cv::waitKey();
	}

	cv::waitKey();

	return 0;
}

int mainNARFQuality(int argc, char **argv) {

	string folderName = argv[1];

	gsl_set_error_handler_off();

	std::vector<NARF::InterestPoint> firstSet;
	std::vector<Point3r> points;

	string camera = folderName + "livingRoom0.gt.freiburg";
	fstream cameraFile = fstream(camera, ios_base::in);
	bool firstFrame = true;

	int step = 4;
	for (int i = 0; i < 200; i += step) {

		stringstream ss;
		ss << i;
		
		string filename = folderName + "depth/" + ss.str() + ".png";
		string rgbFile = folderName + "rgb/" + ss.str() + ".png";

		int indx;
		float tx, ty, tz, qx, qy, qz, qw;

		cameraFile >> indx >> tx >> ty >> tz >> qx >> qy >> qz >> qw;

		for (int s = 1; s < step; ++s) {
			int sindx;
			float stx, sty, stz, sqx, sqy, sqz, sqw;
			cameraFile >> sindx >> stx >> sty >> stz >> sqx >> sqy >> sqz >> sqw;
		}

		Vector3r translation = Vector3r(tx, ty, tz) * 1000;
		Quaternion rot = Quaternion(qw, qx, qy, qz);

		cudaDeviceReset();
		cudaThreadSetLimit(cudaLimitMallocHeapSize, 512*1024*1024);

		NARFParameters narfParameters;
		narfParameters.supportingSize = 5;
		narfParameters.pointsUsedForAverage = 7;
		narfParameters.smoothingRadius = 2;
		narfParameters.reduceBorderSearchingDistance = 5;
		narfParameters.borderScoreThreshold = 0.7f;
		narfParameters.localSurfaceRadius = 5;
		narfParameters.localSurfaceNumerOfClosestNeighbors = 10;

		narfParameters.pixelRadiusBorderDirections = 3;
		narfParameters.minimumBorderProbability = 0.25f;
		narfParameters.minimumWeight = 3;
		narfParameters.mainPrincipalCurvatureRadius = 5;
		narfParameters.minimumInterestValue = 0.45;
		narfParameters.searchRadius = 75;
		narfParameters.optimalDistanceToHighSurfaceChange = 0.25f;
		//narfParameters.addPointsOnStraightEdges = true;
		narfParameters.doNonMaximumSuppression = true;
		narfParameters.numberOfPolynomialApproximations = 1;
		narfParameters.minDistanceBetweenInterestPoints = 5;
		//narfParameters.maxNumberOfInterestPoints = 1000;

		Mat image = imread(filename, cv::IMREAD_ANYCOLOR | cv::IMREAD_ANYDEPTH);
		image /= 5.0f;

		depth_t *data = (depth_t *)image.data;
		auto a = data[0];

		const int width = 640;
		const int height = 480;
		const float focalLenght = 480.0f;

		cuNARF::cuNARFContext gpuContext = cuNARF::cuNARFContext(width, height, width / focalLenght, height / focalLenght, data);

		gpuContext.setParameters(narfParameters);

		cudaFree(0);

		cudaError_t err;

		clock_t time_measure_start = clock();


		cuNARF::initialize(gpuContext);
		cuNARF::depthToWorld(gpuContext);
		cuNARF::calculateTypical3DDistance(gpuContext);
		cuNARF::calculateBorderScore(gpuContext);
		cuNARF::smoothBorderScore(gpuContext);
		cuNARF::firstClassification(gpuContext);
		cuNARF::secondClassification(gpuContext);
		cuNARF::cumulateBorderType(gpuContext);
		cuNARF::calculateLocalSurfaceParameters(gpuContext);
		cuNARF::calculateBorderDirections(gpuContext);
		cuNARF::calculateAverageBorderDirections(gpuContext);
		cuNARF::calculateSurfaceChanges(gpuContext);
		cuNARF::calculateInterestImage(gpuContext);
		cuNARF::calculateInterestPoints(gpuContext);
		cuNARF::sortAndFilterInterestPoints(gpuContext);
		
		clock_t time_measure_stop = clock();
		std::cout << "Time: " << ((double)(time_measure_stop - time_measure_start) / CLOCKS_PER_SEC) << std::endl;
		std::cerr << "Time: " << ((double)(time_measure_stop - time_measure_start) / CLOCKS_PER_SEC) << std::endl;

		cv::Mat rgbImage = cv::imread(rgbFile);

		NARFContext deviceContext = NARFContext(gpuContext);

		if (firstFrame) {
			firstFrame = false;
			firstSet = deviceContext.interestPoints;			

			points.clear();
			for (InterestPoint p : firstSet) {
				Point3r pp = p.point3d;
				pp = (rot * pp * rot.conj()).toVector();
				pp += translation;
				points.push_back(pp);
			}

		} else {

			std::vector<Point3r> ip;
			for (InterestPoint p : deviceContext.interestPoints) {
				Point3r pp = p.point3d;
				pp = (rot * pp * rot.conj()).toVector();
				pp += translation;
				ip.push_back(pp);
			}


			double sum = 0.0f;

			float r = narfParameters.searchRadius;
			for (auto p1 : points)
			{

				float minDistance = (p1 - ip[0]).squaredNorm();
				Point3r minPoint = ip[0];
				NARF::Point2i minPointPos = deviceContext.interestPoints[0].point2d;

				for (int idx = 0; idx < deviceContext.interestPoints.size(); ++idx)
				{
					Point3r p2 = ip[idx];

					float dist = (p1- p2).squaredNorm();

					if (dist < minDistance)
					{
						minDistance = dist;
						minPoint = p2;
						minPointPos = deviceContext.interestPoints[idx].point2d;
					}				
				}

				minDistance = sqrtf(minDistance);
				float s = 1.0f - 3.0f / 4.0f * minDistance / r + 1.0f / 16.0f * powf(minDistance / r, 3.0f);
				if (minDistance > 2 * r)
					s = 0.0f;
				sum += s;
			}

			double result = sum / firstSet.size();
			cout << "Result #" << i << ": " << result << " points: " << deviceContext.interestPoints.size() << endl;
			cerr << "Result #" << i << ": " << result << " points: " << deviceContext.interestPoints.size() << endl;
		}

		deviceContext.dispose();
		gpuContext.dispose();

		image.release();
		rgbImage.release();
	}

	cv::waitKey();

	return 0;
}

int mainOld(int argc, char **argv) {
	if (argc != 2) {
		cout << "No file name" << endl;
		return 1;
	}

	string filename = argv[1];
	ifstream recordFile;
	recordFile.open(filename, ifstream::in | ifstream::binary);

	if (!recordFile) {
		cout << "Could not open file: " << filename << endl;
		return 2;
	}

	Recording recording(recordFile);
	recordFile.close();

	cv::namedWindow("Image", WINDOW_AUTOSIZE);

	fs::path folderPath("Output");
	folderPath /= dateTimeString();
	fs::create_directory(folderPath);

	gsl_set_error_handler_off();

	cudaThreadSetLimit(cudaLimitMallocHeapSize, 512*1024*1024);

	NARFParameters narfParameters;
	narfParameters.supportingSize = 5;
	narfParameters.pointsUsedForAverage = 7;
	narfParameters.smoothingRadius = 2;
	narfParameters.reduceBorderSearchingDistance = 5;
	narfParameters.borderScoreThreshold = 0.7f;
	narfParameters.localSurfaceRadius = 5;
	narfParameters.localSurfaceNumerOfClosestNeighbors = 10;

	narfParameters.pixelRadiusBorderDirections = 3;
	narfParameters.minimumBorderProbability = 0.25f;
	narfParameters.minimumWeight = 3;
	narfParameters.mainPrincipalCurvatureRadius = 5;
	narfParameters.minimumInterestValue = 0.45;
	narfParameters.searchRadius = 50;
	narfParameters.optimalDistanceToHighSurfaceChange = 0.25f;
	//narfParameters.addPointsOnStraightEdges = true;
	narfParameters.doNonMaximumSuppression = true;
	narfParameters.numberOfPolynomialApproximations = 1;
	narfParameters.minDistanceBetweenInterestPoints = 5;
	//narfParameters.maxNumberOfInterestPoints = 1000;

	std::vector<NARF::InterestPoint> lastFrameInterestPoints;
	float **lastFrameFPFH = new float*[33];
	for (int i = 0; i < 33; ++i) {
		lastFrameFPFH[i] = nullptr;
	}
	depth_t *lastDepthMap = nullptr;

	for (int i = 0; i < recording.getFramesCount(); ++i) {
		auto frame = recording.getFrame(i);

		int frameSize = frame->getSize();
		int width = frame->getWidth();
		int height = frame->getHeight();
		NARF::real xzFactor = recording.getXZFactor();
		NARF::real yzFactor = recording.getYZFactor();
		depth_t *data = frame->getDepthData();
		
		NARFContext cpuContext = NARFContext(width, height, xzFactor, yzFactor, data);
		cuNARF::cuNARFContext gpuContext = cuNARF::cuNARFContext(width, height, xzFactor, yzFactor, data);

		cpuContext.setParameters(narfParameters);
		gpuContext.setParameters(narfParameters);

		cudaFree(0);

		cout << "Frame: #" << i + 1 << endl;

		cudaError_t err;

			TIME_MEASURE_START(NARF_CUDA);

			TIME_MEASURE_START(depthToWorld);
			cuNARF::depthToWorld(gpuContext);
			err = cudaDeviceSynchronize();
			TIME_MEASURE_STOP(depthToWorld);

			TIME_MEASURE_START(initialize);
			cuNARF::initialize(gpuContext);
			err = cudaDeviceSynchronize();
			TIME_MEASURE_STOP(initialize);

			TIME_MEASURE_START(calculateTypical3DDistance);
			cuNARF::calculateTypical3DDistance(gpuContext);
			err = cudaDeviceSynchronize();
			TIME_MEASURE_STOP(calculateTypical3DDistance);

			TIME_MEASURE_START(calculateBorderScore);
			cuNARF::calculateBorderScore(gpuContext);
			err = cudaDeviceSynchronize();
			TIME_MEASURE_STOP(calculateBorderScore);

			TIME_MEASURE_START(smoothBorderScore);
			cuNARF::smoothBorderScore(gpuContext);
			err = cudaDeviceSynchronize();
			TIME_MEASURE_STOP(smoothBorderScore);

			TIME_MEASURE_START(firstClassification);
			cuNARF::firstClassification(gpuContext);
			err = cudaDeviceSynchronize();
			TIME_MEASURE_STOP(firstClassification);

			TIME_MEASURE_START(secondClassification);
			cuNARF::secondClassification(gpuContext);
			err = cudaDeviceSynchronize();
			TIME_MEASURE_STOP(secondClassification);

			TIME_MEASURE_START(cumulateBorderType);
			cuNARF::cumulateBorderType(gpuContext);
			err = cudaDeviceSynchronize();
			TIME_MEASURE_STOP(cumulateBorderType);

			TIME_MEASURE_START(calculateLocalSurfaceParameters);
			cuNARF::calculateLocalSurfaceParameters(gpuContext);
			err = cudaDeviceSynchronize();
			TIME_MEASURE_STOP(calculateLocalSurfaceParameters);

			TIME_MEASURE_START(calculateBorderDirections);
			cuNARF::calculateBorderDirections(gpuContext);
			err = cudaDeviceSynchronize();
			TIME_MEASURE_STOP(calculateBorderDirections);

			TIME_MEASURE_START(calculateAverageBorderDirections);
			cuNARF::calculateAverageBorderDirections(gpuContext);
			err = cudaDeviceSynchronize();
			TIME_MEASURE_STOP(calculateAverageBorderDirections);

			TIME_MEASURE_START(calculateSurfaceChanges);
			cuNARF::calculateSurfaceChanges(gpuContext);
			err = cudaDeviceSynchronize();
			TIME_MEASURE_STOP(calculateSurfaceChanges);

			TIME_MEASURE_START(calculateInterestImage);
			cuNARF::calculateInterestImage(gpuContext);
			err = cudaDeviceSynchronize();
			TIME_MEASURE_STOP(calculateInterestImage);

			TIME_MEASURE_START(calculateInterestPoints);
			cuNARF::calculateInterestPoints(gpuContext);
			err = cudaDeviceSynchronize();
			TIME_MEASURE_STOP(calculateInterestPoints);

			TIME_MEASURE_START(sortAndFilterInterestPoints);
			cuNARF::sortAndFilterInterestPoints(gpuContext);
			err = cudaDeviceSynchronize();
			TIME_MEASURE_STOP(sortAndFilterInterestPoints);

			TIME_MEASURE_STOP(NARF_CUDA);
		
		NARFContext deviceContext = NARFContext(gpuContext);
		
		initialize(cpuContext);
		depthToWorld(cpuContext);

		int bins = 11;

		FPFH::FPFHContext fpfhContext = FPFH::FPFHContext(width, height, bins, deviceContext.world);
		fpfhContext.normalsRadius = 10.0f;
		fpfhContext.radius = 5.0f;
		fpfhContext.xzFactor = xzFactor;
		fpfhContext.yzFactor = yzFactor;

		/*fpfhContext.loadFromFile("fpfhContext.dat");*/

		cuFPFH::cuFPFHContext gpuFPFHContext = cuFPFH::cuFPFHContext(width, height, bins, deviceContext.world);
		gpuFPFHContext.normalsRadius = 50.0f;
		gpuFPFHContext.radius = 20.0f;
		gpuFPFHContext.xzFactor = xzFactor;
		gpuFPFHContext.yzFactor = yzFactor;

		//TIME_MEASURE_START(CPU_TIME);

		///*TIME_MEASURE_START(normals1);
		//FPFH::calculateNormals(fpfhContext);
		//TIME_MEASURE_STOP(normals1);*/
		//{
		//	TIME_MEASURE_START(normals2);
		//	FPFH::calculateNormals2(fpfhContext);
		//	TIME_MEASURE_STOP(normals2);


		//	TIME_MEASURE_START(calculateSPFHs);
		//	FPFH::calculateSPFHs(fpfhContext);
		//	TIME_MEASURE_STOP(calculateSPFHs);

		//	TIME_MEASURE_START(calculateFPFHs);
		//	FPFH::calculateFPFHs(fpfhContext);
		//	TIME_MEASURE_STOP(calculateFPFHs);
		//}
		//TIME_MEASURE_STOP(CPU_TIME);

		TIME_MEASURE_START(GPU_TIME);
		{
			TIME_MEASURE_START(initialize);
			cuFPFH::initialize(gpuFPFHContext);
			CUDA_SAFE_CALL(cudaDeviceSynchronize());
			TIME_MEASURE_STOP(initialize);

			TIME_MEASURE_START(calculateNormals2);
			cuFPFH::calculateNormals2(gpuFPFHContext);
			CUDA_SAFE_CALL(cudaDeviceSynchronize());
			TIME_MEASURE_STOP(calculateNormals2);

			TIME_MEASURE_START(calculateSPFHs);
			cuFPFH::calculateSPFHs(gpuFPFHContext);
			CUDA_SAFE_CALL(cudaDeviceSynchronize());
			TIME_MEASURE_STOP(calculateSPFHs);

			TIME_MEASURE_START(calculateFPFHs2);
			cuFPFH::calculateFPFHs2(gpuFPFHContext);
			CUDA_SAFE_CALL(cudaDeviceSynchronize());
			TIME_MEASURE_STOP(calculateFPFHs2);
		}
		TIME_MEASURE_STOP(GPU_TIME);

		FPFH::FPFHContext gpuCopy(gpuFPFHContext);


		/*NARF::compare(fpfhContext.normals, gpuCopy.normals, width, height);
		NARF::compareExact(fpfhContext.spfh.valid, gpuCopy.spfh.valid, width, height);
		
		for (int i = 0; i < fpfhContext.spfh.histSize; ++i) {
			NARF::compare(fpfhContext.spfh.data[i], gpuCopy.spfh.data[i], width, height);
		}*/

		//{
		//	TIME_MEASURE_START(NARF_CPU);

		//	TIME_MEASURE_START(initialize);
		//	initialize(cpuContext);
		//	TIME_MEASURE_STOP(initialize);

		//	TIME_MEASURE_START(depthToWorld);
		//	depthToWorld(cpuContext);
		//	TIME_MEASURE_STOP(depthToWorld);

		//	TIME_MEASURE_START(calculateTypical3DDistance);
		//	calculateTypical3DDistance(cpuContext);
		//	TIME_MEASURE_STOP(calculateTypical3DDistance);

		//	TIME_MEASURE_START(calculateBorderScore);
		//	calculateBorderScore(cpuContext);
		//	TIME_MEASURE_STOP(calculateBorderScore);

		//	TIME_MEASURE_START(smoothBorderScore);
		//	smoothBorderScore(cpuContext);
		//	TIME_MEASURE_STOP(smoothBorderScore);

		//	TIME_MEASURE_START(firstClassification);
		//	firstClassification(cpuContext);
		//	TIME_MEASURE_STOP(firstClassification);

		//	TIME_MEASURE_START(secondClassification);
		//	secondClassification(cpuContext);
		//	TIME_MEASURE_STOP(secondClassification);

		//	TIME_MEASURE_START(cumulateBorderType);
		//	cumulateBorderType(cpuContext);
		//	TIME_MEASURE_STOP(cumulateBorderType);

		//	TIME_MEASURE_START(calculateLocalSurfaceParameters);
		//	calculateLocalSurfaceParameters(cpuContext);
		//	TIME_MEASURE_STOP(calculateLocalSurfaceParameters);

		//	//interest points
		//	TIME_MEASURE_START(calculateBorderDirections);
		//	calculateBorderDirections(cpuContext);
		//	TIME_MEASURE_STOP(calculateBorderDirections);

		//	TIME_MEASURE_START(calculateAverageBorderDirections);
		//	calculateAverageBorderDirections(cpuContext);
		//	TIME_MEASURE_STOP(calculateAverageBorderDirections);

		//	TIME_MEASURE_START(calculateSurfaceChanges);
		//	calculateSurfaceChanges(cpuContext);
		//	TIME_MEASURE_STOP(calculateSurfaceChanges);

		//	TIME_MEASURE_START(calculateInterestImage);
		//	calculateInterestImage(cpuContext);
		//	TIME_MEASURE_STOP(calculateInterestImage);

		//	TIME_MEASURE_START(calculateInterestPoints);
		//	calculateInterestPoints(cpuContext);
		//	TIME_MEASURE_STOP(calculateInterestPoints);

		//	TIME_MEASURE_START(sortAndFilterInterestPoints);
		//	sortAndFilterInterestPoints(cpuContext);
		//	TIME_MEASURE_STOP(sortAndFilterInterestPoints);

		//	TIME_MEASURE_STOP(NARF_CPU);
		//}

		TIME_MEASURE_START(CompareResults);

		/*NARFContext deviceContext = NARFContext(gpuContext);
		NARF::compare(deviceContext.typical3dDistance, cpuContext.typical3dDistance, width, height);
		for (int i = 0; i < 4; ++i)
			NARF::compare(deviceContext.borderScore[i], cpuContext.borderScore[i], width, height);
		for (int i = 0; i < 4; ++i)
			NARF::compare(deviceContext.avgNeighborRange[i], cpuContext.avgNeighborRange[i], width, height);
		for (int i = 0; i < 4; ++i)
			NARF::compare(deviceContext.smoothBorderScore[i], cpuContext.smoothBorderScore[i], width, height);
		for (int i = 0; i < 4; ++i)
			NARF::compareExact(deviceContext.potentialBorderType[i], cpuContext.potentialBorderType[i], width, height);
		for (int i = 0; i < 4; ++i)
			NARF::compareExact(deviceContext.borderType[i], cpuContext.borderType[i], width, height);
		for (int i = 0; i < 4; ++i)
			NARF::compare(deviceContext.secondBorderScore[i], cpuContext.secondBorderScore[i], width, height);
		
		NARF::compareExact(deviceContext.borderTypeWithDirection, cpuContext.borderTypeWithDirection, width, height);
		NARF::compareLocalSurface(deviceContext.localSurface, cpuContext.localSurface, width, height, 0.1);
		NARF::compare(deviceContext.surfaceChangeDirection, cpuContext.surfaceChangeDirection, width, height);
		NARF::compare(deviceContext.surfaceChangeScore, cpuContext.surfaceChangeScore, width, height);

		NARF::compare(deviceContext.interestImage, cpuContext.interestImage, width, height);
		NARF::compareExact(cpuContext.isInterestPoint, gpuContext.hostIsInterestPoint, width, height);*/


		TIME_MEASURE_STOP(CompareResults);


		if (i > 0) {
			vector<pair<InterestPoint, InterestPoint> > bestMatches;
			vector<InterestPoint> currentInterestPoints = deviceContext.interestPoints;

			cout << currentInterestPoints.size() << " " << deviceContext.interestPoints.size() << endl;

			while (bestMatches.size() < 10) {

				vector<InterestPoint>::iterator it1;
				vector<InterestPoint>::iterator it2;
				float currentBest = FLT_MAX;

				
				for (auto ip1 = lastFrameInterestPoints.begin(); ip1 != lastFrameInterestPoints.end(); ++ip1) {
					for (auto ip2 = currentInterestPoints.begin(); ip2 != currentInterestPoints.end(); ++ip2) {

						float thisPairScore = histogramDistance(lastFrameFPFH, gpuCopy.fpfh.data, ip1->point2d, ip2->point2d, width, gpuCopy.fpfh.histSize);

						if (thisPairScore < currentBest) {
							currentBest = thisPairScore;
							it1 = ip1;
							it2 = ip2;
						}

					}
				}

				bestMatches.push_back(make_pair(*it1, *it2));
				lastFrameInterestPoints.erase(it1);
				currentInterestPoints.erase(it2);
			}


			Mat imageToSave = Mat(480, 640*2, CV_8UC3);

			depth_t min = data[0];
			depth_t max = data[0];

			for (int i = 0; i < width * height; ++i) {
				min = MIN(min, data[i]);
				min = MIN(min, lastDepthMap[i]);

				max = MAX(max, data[i]);
				max = MAX(max, lastDepthMap[i]);
			}

			//fill
			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; ++x) {
					int index = y * width + x;
					int indexL = y * (2 * width) + x;
					int indexR = y * (2 * width) + width + x;


					uchar b1 = float((data[y * width + x] - min)) / float((max - min)) * 256;
					uchar b2 = float((lastDepthMap[y * width + x] - min)) / float((max - min)) * 256;

					imageToSave.at<cv::Vec3b>(y, x) = cv::Vec3b(b1, b1, b1);
					imageToSave.at<cv::Vec3b>(y, x + width) = cv::Vec3b(b2, b2, b2);
				}
			}

			for (auto pair : bestMatches) {
				cv::Point2d p1 = cv::Point2d(pair.first.point2d.x, pair.first.point2d.y);
				cv::Point2d p2 = cv::Point2d(pair.second.point2d.x + width, pair.second.point2d.y);

				cv::line(imageToSave,
					p1, p2,
					cv::Scalar(rand()%256, rand()%256, rand()%256));
			}

			/*Stats<float> stats((float *)float_mat.data, 640 * 480);
			cout << stats;

			float_mat = (float_mat - stats.min) / (stats.max - stats.min);

			cv::imshow("Image", float_mat);*/

			//save to file
			/*Mat imageToSave;
			float_mat.convertTo(imageToSave, CV_8UC3, 256);*/

			/*Mat imageRGB;
			cv::cvtColor(imageToSave, imageRGB, CV_GRAY2RGB);*/

			//for (auto it = interestPointExtractor.interestPoints.begin(); it != interestPointExtractor.interestPoints.end(); ++it) {
			//	cv::circle(
			//		imageToSave,
			//		cv::Point(it->point2d.x, it->point2d.y),
			//		1,
			//		cv::Scalar(255,0,0, 255),
			//		-1
			//		);
			//}

			cv::imshow("Image", imageToSave);


			//fs::path filePath = folderPath;

			//ostringstream fileNameStream;
			//fileNameStream << std::setw(3) << std::setfill('0');
			//fileNameStream << i;
			//filePath /= fileNameStream.str();
			//filePath += ".png";

			//bool result = cv::imwrite(filePath.string(), imageToSave);

			cv::waitKey(0);
		}

		lastFrameInterestPoints = deviceContext.interestPoints;
		for (int i = 0; i < gpuCopy.fpfh.histSize; ++i) {
			delete[] lastFrameFPFH[i];
			lastFrameFPFH[i] = new float[width * height];
			memcpy(lastFrameFPFH[i], gpuCopy.fpfh.data[i], sizeof(float) * width * height);
		}
		delete[] lastDepthMap;
		lastDepthMap = new depth_t[width * height];
		memcpy(lastDepthMap, data, sizeof(depth_t) * width * height);
	}

	cudaDeviceReset();
	
	return 0;
}

static AppData *appData;

static std::map<ImageType, std::string> namesMapping;

std::string nameForImageType(ImageType type) {
	if (namesMapping.size() == 0) {
		namesMapping[DepthMap] = "mapa glebi";
		namesMapping[Typical3DDistance] = "odleglosc od sasiadow";
		namesMapping[BorderScore] = "border score";
		namesMapping[SmoothedBorderScore] = "wygladzony border score";
		namesMapping[FirstClassification] = "pierwsza klasyfikacja";
		namesMapping[SecondClassification] = "druga klasyfikacja";
		namesMapping[LocalSurfaceParameters] = "lokalne parametry";
		namesMapping[BorderScoreUp] = "gora";
		namesMapping[BorderScoreRight] = "prawo";
		namesMapping[BorderScoreDown] = "dol";
		namesMapping[BorderScoreLeft] = "lewo";
		namesMapping[SmoothedBorderScoreUp] = "gora";
		namesMapping[SmoothedBorderScoreRight] = "prawo";
		namesMapping[SmoothedBorderScoreDown] = "dol";
		namesMapping[SmoothedBorderScoreLeft] = "lewo";
		namesMapping[FirstClassificationUp] = "gora";
		namesMapping[FirstClassificationRight] = "prawo";
		namesMapping[FirstClassificationDown] = "dol";
		namesMapping[FirstClassificationLeft] = "lewo";
		namesMapping[SecondClassificationUp] = "gora";
		namesMapping[SecondClassificationRight] = "prawo";
		namesMapping[SecondClassificationDown] = "dol";
		namesMapping[SecondClassificationLeft] = "lewo";
		namesMapping[LocalSurfaceParametersNormal] = "normalne";
		namesMapping[LocalSurfaceParametersMeanNeighborDistance] = "sr. dystans do sasiadow";
		namesMapping[LocalSurfaceParametersEigenValues] = "wartosci wlasne";
		namesMapping[LocalSurfaceParametersNormalNoJumps] = "normalne - bez przeskokow";
		namesMapping[LocalSurfaceParametersMeanNeighborDistanceNoJumps] = "sr. dystans do sasiadow - bez przeskokow";
		namesMapping[LocalSurfaceParametersEigenValuesNoJumps] = "wartosci wlasne - bez przeskokow";
		namesMapping[LocalSurfaceParametersMaxNeighborDistance] = "maks. odleglosc do sasiadow";

		namesMapping[BorderDirections] = "Kierunek krawedzi";
		namesMapping[AvgBorderDirections] = "Wygladzony kierunek krawedzi";
		namesMapping[SurfaceChange] = "Zmiennosc powierzchni";
		namesMapping[SurfaceChangeScore] = "Zmiennosc powierzchni";
		namesMapping[SurfaceChangeDirection] = "Kierunek zmiennosci powierzchni";
		namesMapping[InterestImage] = "Interest image";
		namesMapping[InterestPoints] = "Punkty charakterystyczne";
		namesMapping[FPFHNormals] = "Normalne (FPFH)";
		namesMapping[SPFHImage] = "SPFH";
		namesMapping[FPFHImage] = "FPFH";
	}

	return namesMapping[type];
}

void addListItem(AppData *appData, GtkTreeStore *tree, const DataRow &row, GtkTreeIter *parentIter) {
	ImageType type = row.imageType;
	std::string name = nameForImageType(type);
	GtkTreeIter itemIter;
	gtk_tree_store_append(tree, &itemIter, parentIter);
	gtk_tree_store_set(tree, &itemIter, 0, row.miniature, 1, name.c_str(), -1);

	for each (const DataRow &childRow in row.children) {
		addListItem(appData, tree, childRow, &itemIter);
	}
}

void updateList(AppData *appData) {
	GtkTreeStore *tree = appData->imagesTreeStore;
	unsigned frame = appData->document->currentFrame;

	GtkTreeIter iter;
	gtk_tree_store_clear(tree);

	for (const DataRow &row : appData->document->data[frame]) {
		addListItem(appData, tree, row, NULL);
	}

	/*if (selectedRow >= 0) {
		int i = MIN(selectedRow, appData->document->data[frame].size()-1);
		GtkTreePath *path = gtk_tree_path_new();
		gtk_tree_path_append_index(path, (gint)i);
		gtk_tree_selection_select_path(gtk_tree_view_get_selection(appData->treeView), path);
		gtk_tree_path_free(path);
	}*/
}

void updatePreview(AppData *appData) {
	if (appData->document->selectedItem.size() == 0) {
		gtk_image_set_from_pixbuf(appData->previewWidget, NULL);
		return;
	}

	// image type on selected item
	unsigned frame = appData->document->currentFrame;
	vector<int> selectedItem = appData->document->selectedItem;

	auto it = selectedItem.begin();
	DataRow *row = &(appData->document->data[frame][*it]);
	++it;

	for (; it != selectedItem.end(); ++it) {
		row = &row->children[*it];
	}

	gtk_image_set_from_pixbuf(appData->previewWidget, row->pixbuf);
}

EXPORT gboolean on_preview_widget_query_tooltip(GtkWidget *widget, gint x, gint y, gboolean keyboard_mode, GtkTooltip *tooltip, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	GtkImage *image = GTK_IMAGE(widget);
	GdkPixbuf *pixbuf = gtk_image_get_pixbuf(image);

	if (!pixbuf) {
		return FALSE;
	}

	int imageWidth = gdk_pixbuf_get_width(pixbuf);
	int imageHeight = gdk_pixbuf_get_height(pixbuf);

	GtkAllocation allocation;
	gtk_widget_get_allocation(widget, &allocation);

	int imgX, imgY;

	if (imageWidth > allocation.width) {
		imgX = x;
	}
	else {
		imgX = x - (allocation.width - imageWidth) / 2;
	}

	if (imageHeight > allocation.height) {
		imgY = y;
	}
	else {
		imgY = y - (allocation.height - imageHeight) / 2;
	}

	
	string toolTipString = appData->document->toolTipForPoint(imgX, imgY);
	gtk_tooltip_set_text(tooltip, toolTipString.c_str());
	return TRUE;
}



void setCurrentFrame(AppData *appData, unsigned frame) {
	gtk_range_set_value(GTK_RANGE(appData->framesScale), frame);
	gtk_spin_button_set_value(appData->framesSpinButton, frame);
	appData->document->currentFrame = frame;
	
	updateList(appData);
	updatePreview(appData);
}

void import_file(AppData *appData, string filename) {
	appData->document->importRecording(filename);
	
	gtk_range_set_range(GTK_RANGE(appData->framesScale), 0.0f, appData->document->recording->getFramesCount()-1);
	gtk_range_set_value(GTK_RANGE(appData->framesScale), 0.0f);
	gtk_widget_set_sensitive(GTK_WIDGET(appData->framesScale), TRUE);

	gtk_spin_button_set_range(appData->framesSpinButton, 0.0, appData->document->recording->getFramesCount()-1);
	gtk_spin_button_set_value(appData->framesSpinButton, 0.0);
	gtk_widget_set_sensitive(GTK_WIDGET(appData->framesSpinButton), TRUE);

	setCurrentFrame(appData, 0);
}

EXPORT void on_frames_scale_value_changed(GtkRange *range, gpointer  user_data) {
	AppData *appData = (AppData *)user_data;
	gdouble value  = gtk_range_get_value(range);
	unsigned frame = (unsigned)(floor(value));
	setCurrentFrame(appData, frame);
}

EXPORT void on_frames_spin_button_value_changed(GtkSpinButton *spin_button, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	gdouble value = gtk_spin_button_get_value(spin_button);
	unsigned frame = (unsigned)(floor(value));
	setCurrentFrame(appData, frame);
}

EXPORT void on_treeview_images_selection_changed(GtkTreeSelection *treeselection, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	GList *list = gtk_tree_selection_get_selected_rows(treeselection, NULL);
	if (list && list->data) {
		GtkTreePath *path = (GtkTreePath *)list->data;
		int n;
		int *indices = gtk_tree_path_get_indices_with_depth(path, &n);
		std::vector<int> newSelection = std::vector<int>(indices, indices + n);
		appData->document->setSelectedItem(newSelection);
	}
	else {
		appData->document->selectedItem.clear();
	}

	g_list_free(list);

	updatePreview(appData);
}

EXPORT void on_menuitem_narf_typical3ddistance_activate(GtkMenuItem *menuitem, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	unsigned frame = appData->document->currentFrame;

	GtkDialog *typical3DDistanceDialog = load_dialog("ui/typicaldistancedialog.ui", user_data);
	gint result = gtk_dialog_run(typical3DDistanceDialog);

	GtkSpinButton *supportingSizeSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(typical3DDistanceDialog), "supporting_size_spin"));
	int supportingSize = (int)roundf(gtk_spin_button_get_value(supportingSizeSpin));

	gtk_widget_destroy(GTK_WIDGET(typical3DDistanceDialog));

	if (result != GTK_RESPONSE_OK) {
		return;
	}

	cuNARF::cuNARFContext *context = appData->document->narfContexts[frame];
	if (!context) {
		context = new cuNARF::cuNARFContext(
			appData->document->recording->getWidth(),
			appData->document->recording->getHeight(),
			appData->document->recording->getXZFactor(),
			appData->document->recording->getXZFactor(),
			appData->document->recording->getFrame(frame)->getDepthData()
			);
		appData->document->narfContexts[frame] = context;
	}

	context->supportingSize = supportingSize;

	cuNARF::initialize(*context);
	cuNARF::depthToWorld(*context);
	cuNARF::calculateTypical3DDistance(*context);
	cudaDeviceSynchronize();


	NARF::NARFContext *cpuContext = appData->document->cpuNarfContexts[frame];
	if (!cpuContext) {
		cpuContext = new NARF::NARFContext(*context);
		appData->document->cpuNarfContexts[frame] = cpuContext;
	}
	else {
		cpuContext->copyParameters(*context);
		cpuContext->copyTypical3DDistance(*context);
	}

	NARF::real *data = cpuContext->typical3dDistance;

	GdkPixbuf *typical3DDistancePixbuf;
	GdkPixbuf *typical3DDistanceMiniPixbuf;
	
	gdk_pixbuf_new_from_data_custom(data, context->width, context->height, logf, &typical3DDistancePixbuf, &typical3DDistanceMiniPixbuf);
	
	appData->document->data[frame].push_back(DataRow(ImageType::Typical3DDistance, typical3DDistancePixbuf, typical3DDistanceMiniPixbuf));
	updateList(appData);
}

EXPORT void on_menuitem_narf_borderscore_activate(GtkMenuItem *menuitem, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	unsigned frame = appData->document->currentFrame;

	GtkDialog *borderScoreDialog = load_dialog("ui/borderscoredialog.ui", user_data);
	gint result = gtk_dialog_run(borderScoreDialog);

	GtkSpinButton *pointsUsedForAverageSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(borderScoreDialog), "points_used_for_average_spin"));
	int pointsUsedForAverage = (int)roundf(gtk_spin_button_get_value(pointsUsedForAverageSpin));

	gtk_widget_destroy(GTK_WIDGET(borderScoreDialog));

	if (result != GTK_RESPONSE_OK) {
		return;
	}

	cuNARF::cuNARFContext *context = appData->document->narfContexts[frame];
	context->pointsUsedForAverage = pointsUsedForAverage;

	cuNARF::calculateBorderScore(*context);
	cudaDeviceSynchronize();

	NARF::NARFContext *cpuContext = appData->document->cpuNarfContexts[frame];
	cpuContext->copyParameters(*context);
	cpuContext->copyBorderScore(*context);
	
	DataRow borderscoreRow(ImageType::BorderScore);
	ImageType types[] = { ImageType::BorderScoreUp, ImageType::BorderScoreRight, ImageType::BorderScoreDown, ImageType::BorderScoreLeft };

	for (int i = 0; i < 4; ++i) {
		NARF::real *data = cpuContext->borderScore[i];
		
		GdkPixbuf *borderScorePixbuf;
		GdkPixbuf *borderScoreMiniPixbuf;

		gdk_pixbuf_new_from_data_custom(data, context->width, context->height, identityFunc<NARF::real>, &borderScorePixbuf, &borderScoreMiniPixbuf);

		borderscoreRow.children.push_back(DataRow(types[i], borderScorePixbuf, borderScoreMiniPixbuf));
	}

	appData->document->data[frame].push_back(borderscoreRow);

	updateList(appData);
}

EXPORT void on_menuitem_narf_smooth_border_score_activate(GtkMenuItem *menuitem, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	unsigned frame = appData->document->currentFrame;

	GtkDialog *smoothBorderScoreDialog = load_dialog("ui/smoothborderscoredialog.ui", user_data);
	gint result = gtk_dialog_run(smoothBorderScoreDialog);

	GtkSpinButton *smoothingRadiusSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(smoothBorderScoreDialog), "smoothing_radius_spin"));
	int smoothingRadius = (int)roundf(gtk_spin_button_get_value(smoothingRadiusSpin));

	gtk_widget_destroy(GTK_WIDGET(smoothBorderScoreDialog));

	if (result != GTK_RESPONSE_OK) {
		return;
	}

	cuNARF::cuNARFContext *context = appData->document->narfContexts[frame];
	context->smoothingRadius = smoothingRadius;

	cuNARF::smoothBorderScore(*context);
	cudaDeviceSynchronize();

	NARF::NARFContext *cpuContext = appData->document->cpuNarfContexts[frame];
	cpuContext->copyParameters(*context);
	cpuContext->copySmoothedBorderScore(*context);

	DataRow smoothedBorderScoreRow(ImageType::SmoothedBorderScore);
	ImageType types[] = { ImageType::SmoothedBorderScoreUp, ImageType::SmoothedBorderScoreRight, ImageType::SmoothedBorderScoreDown, ImageType::SmoothedBorderScoreLeft };

	for (int i = 0; i < 4; ++i) {
		NARF::real *data = cpuContext->smoothBorderScore[i];

		GdkPixbuf *smoothBorderScorePixbuf;
		GdkPixbuf *smoothBorderScoreMiniPixbuf;

		gdk_pixbuf_new_from_data_custom(data, context->width, context->height, identityFunc<NARF::real>, &smoothBorderScorePixbuf, &smoothBorderScoreMiniPixbuf);

		smoothedBorderScoreRow.children.push_back(DataRow(types[i], smoothBorderScorePixbuf, smoothBorderScoreMiniPixbuf));
	}

	appData->document->data[frame].push_back(smoothedBorderScoreRow);

	updateList(appData);
}

EXPORT void on_menuitem_narf_firstclassification_activate(GtkMenuItem *menuitem, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	unsigned frame = appData->document->currentFrame;

	cuNARF::cuNARFContext *context = appData->document->narfContexts[frame];

	cuNARF::firstClassification(*context);
	cudaDeviceSynchronize();

	NARF::NARFContext *cpuContext = appData->document->cpuNarfContexts[frame];
	cpuContext->copyParameters(*context);
	cpuContext->copyPotentialBorderType(*context);

	DataRow firstClassificationRow(ImageType::FirstClassification);
	ImageType types[] = { ImageType::FirstClassificationUp, ImageType::FirstClassificationRight, ImageType::FirstClassificationDown, ImageType::FirstClassificationLeft };

	for (int i = 0; i < 4; ++i) {
		NARF::BorderExtractor::PotentialBorderType *data = cpuContext->potentialBorderType[i];

		GdkPixbuf *potentialBorderTypePixbuf;
		GdkPixbuf *potentialBorderTypeMiniPixbuf;

		auto transform = [](NARF::BorderExtractor::PotentialBorderType x)->cv::Vec3b {
			switch (x)
			{
			default:
			case NARF::BorderExtractor::PotentialBorderType::Unknown:
				return cv::Vec3b(0, 0, 0);
				
			case NARF::BorderExtractor::PotentialBorderType::ObstacleBorder:
				return cv::Vec3b(255, 0, 0);
				
			case NARF::BorderExtractor::PotentialBorderType::ShadowBorder:
				return cv::Vec3b(0, 255, 0);
			}
		};

		gdk_pixbuf_new_from_data_vec3b(data, context->width, context->height, transform, &potentialBorderTypePixbuf, &potentialBorderTypeMiniPixbuf);
		firstClassificationRow.children.push_back(DataRow(types[i], potentialBorderTypePixbuf, potentialBorderTypeMiniPixbuf));
	}

	appData->document->data[frame].push_back(firstClassificationRow);

	updateList(appData);
}

EXPORT void on_menuitem_narf_second_classification_activate(GtkMenuItem *menuitem, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	unsigned frame = appData->document->currentFrame;

	GtkDialog *secondClassificationDialog = load_dialog("ui/secondclassificationdialog.ui", user_data);
	gint result = gtk_dialog_run(secondClassificationDialog);

	GtkSpinButton *searchingDistanceSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(secondClassificationDialog), "searching_distance_spin"));
	int searchingDistance = (int)roundf(gtk_spin_button_get_value(searchingDistanceSpin));

	GtkSpinButton *borderThresholdSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(secondClassificationDialog), "border_threshold_spin"));
	float borderThreshold = gtk_spin_button_get_value(borderThresholdSpin);

	gtk_widget_destroy(GTK_WIDGET(secondClassificationDialog));

	if (result != GTK_RESPONSE_OK) {
		return;
	}

	cuNARF::cuNARFContext *context = appData->document->narfContexts[frame];
	context->reduceBorderSearchingDistance = searchingDistance;
	context->borderScoreThreshold = borderThreshold;

	cuNARF::secondClassification(*context);
	cuNARF::cumulateBorderType(*context);
	cudaDeviceSynchronize();

	NARF::NARFContext *cpuContext = appData->document->cpuNarfContexts[frame];
	cpuContext->copyParameters(*context);
	cpuContext->copyBorderType(*context);

	DataRow secondClassificationRow(ImageType::SecondClassification);
	ImageType types[] = { ImageType::SecondClassificationUp, ImageType::SecondClassificationRight, ImageType::SecondClassificationDown, ImageType::SecondClassificationLeft };

	for (int i = 0; i < 4; ++i) {
		NARF::BorderExtractor::BorderType *data = cpuContext->borderType[i];

		GdkPixbuf *borderTypePixbuf;
		GdkPixbuf *borderTypeMiniPixbuf;

		auto transform = [](NARF::BorderExtractor::BorderType x)->cv::Vec3b {
			switch (x)
			{
			default:
			case NARF::BorderExtractor::BorderType::NoBorder:
				return cv::Vec3b(0, 0, 0);

			case NARF::BorderExtractor::BorderType::ObstacleBorder:
				return cv::Vec3b(255, 0, 0);

			case NARF::BorderExtractor::BorderType::ShadowBorder:
				return cv::Vec3b(0, 255, 0);
				
			case NARF::BorderExtractor::BorderType::VeilPoint:
				return cv::Vec3b(0, 0, 255);
			
			}
		};

		gdk_pixbuf_new_from_data_vec3b(data, context->width, context->height, transform, &borderTypePixbuf, &borderTypeMiniPixbuf);

		secondClassificationRow.children.push_back(DataRow(types[i], borderTypePixbuf, borderTypeMiniPixbuf));
	}

	appData->document->data[frame].push_back(secondClassificationRow);

	updateList(appData);
}

EXPORT void on_menuitem_narf_localparameters_activate(GtkMenuItem *menuitem, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	unsigned frame = appData->document->currentFrame;

	GtkDialog *localSurfaceParametersDialog = load_dialog("ui/localsurfaceparametersdialog.ui", user_data);
	gint result = gtk_dialog_run(localSurfaceParametersDialog);

	GtkSpinButton *radiusSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(localSurfaceParametersDialog), "radius_spin"));
	gdouble radius = gtk_spin_button_get_value(radiusSpin);

	GtkSpinButton *noOfNeighborsSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(localSurfaceParametersDialog), "no_neightbors_spin"));
	int numberOfNeightbors = (int)roundf(gtk_spin_button_get_value(noOfNeighborsSpin));

	gtk_widget_destroy(GTK_WIDGET(localSurfaceParametersDialog));

	if (result != GTK_RESPONSE_OK) {
		return;
	}

	cuNARF::cuNARFContext *context = appData->document->narfContexts[frame];
	context->localSurfaceRadius = radius;
	context->localSurfaceNumerOfClosestNeighbors = numberOfNeightbors;

	cuNARF::calculateLocalSurfaceParameters(*context);
	cudaDeviceSynchronize();

	NARF::NARFContext *cpuContext = appData->document->cpuNarfContexts[frame];
	cpuContext->copyParameters(*context);
	cpuContext->copyLocalSurfaceParameters(*context);

	LocalSurface min;
	LocalSurface max;

	bool hasValue = false;

	for (int i = 0; i < context->width * context->height; ++i) {
		const LocalSurface &ls = cpuContext->localSurface[i];

		Point3r p = cpuContext->world[i];
		Vector3r diff = p - ls.neighborhoodMean;
		Vector3r diffNoJumps = p - ls.nighborhoodMeanNoJumps;

		if (!ls.valid)
			continue;

		if (!hasValue) {
			min = max = ls;
			min.neighborhoodMean = max.neighborhoodMean = diff;
			min.nighborhoodMeanNoJumps = max.nighborhoodMeanNoJumps = diffNoJumps;
			hasValue = true;
			continue;
		}

		min.normal.x = MIN(min.normal.x, ls.normal.x);
		min.normal.y = MIN(min.normal.y, ls.normal.y);
		min.normal.z = MIN(min.normal.z, ls.normal.z);

		min.neighborhoodMean.x = MIN(min.neighborhoodMean.x, diff.x);
		min.neighborhoodMean.y = MIN(min.neighborhoodMean.y, diff.y);
		min.neighborhoodMean.z = MIN(min.neighborhoodMean.z, diff.z);

		min.eigenValues[0] = MIN(min.eigenValues[0], ls.eigenValues[0]);
		min.eigenValues[1] = MIN(min.eigenValues[1], ls.eigenValues[1]);
		min.eigenValues[2] = MIN(min.eigenValues[2], ls.eigenValues[2]);

		min.normalNoJumps.x = MIN(min.normalNoJumps.x, ls.normalNoJumps.x);
		min.normalNoJumps.y = MIN(min.normalNoJumps.y, ls.normalNoJumps.y);
		min.normalNoJumps.z = MIN(min.normalNoJumps.z, ls.normalNoJumps.z);

		min.eigenValuesNoJumps[0] = MIN(min.eigenValuesNoJumps[0], ls.eigenValuesNoJumps[0]);
		min.eigenValuesNoJumps[1] = MIN(min.eigenValuesNoJumps[1], ls.eigenValuesNoJumps[1]);
		min.eigenValuesNoJumps[2] = MIN(min.eigenValuesNoJumps[2], ls.eigenValuesNoJumps[2]);

		min.nighborhoodMeanNoJumps.x = MIN(min.nighborhoodMeanNoJumps.x, diffNoJumps.x);
		min.nighborhoodMeanNoJumps.y = MIN(min.nighborhoodMeanNoJumps.y, diffNoJumps.y);
		min.nighborhoodMeanNoJumps.z = MIN(min.nighborhoodMeanNoJumps.z, diffNoJumps.z);

		min.maxNeighborDistanceSquared = MIN(min.maxNeighborDistanceSquared, ls.maxNeighborDistanceSquared);


		max.normal.x = MAX(max.normal.x, ls.normal.x);
		max.normal.y = MAX(max.normal.y, ls.normal.y);
		max.normal.z = MAX(max.normal.z, ls.normal.z);

		max.neighborhoodMean.x = MAX(max.neighborhoodMean.x, diff.x);
		max.neighborhoodMean.y = MAX(max.neighborhoodMean.y, diff.y);
		max.neighborhoodMean.z = MAX(max.neighborhoodMean.z, diff.z);

		max.eigenValues[0] = MAX(max.eigenValues[0], ls.eigenValues[0]);
		max.eigenValues[1] = MAX(max.eigenValues[1], ls.eigenValues[1]);
		max.eigenValues[2] = MAX(max.eigenValues[2], ls.eigenValues[2]);

		max.normalNoJumps.x = MAX(max.normalNoJumps.x, ls.normalNoJumps.x);
		max.normalNoJumps.y = MAX(max.normalNoJumps.y, ls.normalNoJumps.y);
		max.normalNoJumps.z = MAX(max.normalNoJumps.z, ls.normalNoJumps.z);

		max.nighborhoodMeanNoJumps.x = MAX(max.nighborhoodMeanNoJumps.x, diffNoJumps.x);
		max.nighborhoodMeanNoJumps.y = MAX(max.nighborhoodMeanNoJumps.y, diffNoJumps.y);
		max.nighborhoodMeanNoJumps.z = MAX(max.nighborhoodMeanNoJumps.z, diffNoJumps.z);

		max.eigenValuesNoJumps[0] = MAX(max.eigenValuesNoJumps[0], ls.eigenValuesNoJumps[0]);
		max.eigenValuesNoJumps[1] = MAX(max.eigenValuesNoJumps[1], ls.eigenValuesNoJumps[1]);
		max.eigenValuesNoJumps[2] = MAX(max.eigenValuesNoJumps[2], ls.eigenValuesNoJumps[2]);

		max.maxNeighborDistanceSquared = MAX(max.maxNeighborDistanceSquared, ls.maxNeighborDistanceSquared);
	}

	int width = context->width;
	int height = context->height;

	DataRow localSurfaceRow = DataRow(ImageType::LocalSurfaceParameters);

	// Normals
	GdkPixbuf *normalPixbuf;
	GdkPixbuf *normalMiniPixbuf;

	gdk_pixbuf_new_from_data_vec3b(cpuContext->localSurface, width, height, [](const LocalSurface &ls)->cv::Vec3b {
		Vector3r v = (ls.normal + Vector3r(1.0f, 1.0f, 1.0f)) / 2.0f * 255.0f;
		return cv::Vec3b(v.x, v.y, v.z);
	}, &normalPixbuf, &normalMiniPixbuf);

	localSurfaceRow.children.push_back(DataRow(ImageType::LocalSurfaceParametersNormal, normalPixbuf, normalMiniPixbuf));
	
	// Neighborhood mean
	GdkPixbuf *neighborhoodMeanPixbuf;
	GdkPixbuf *neighborhoodMeanMiniPixbuf;

	NARF::real minVal = MIN(min.neighborhoodMean.x, MIN(min.neighborhoodMean.y, min.neighborhoodMean.z));
	NARF::real maxVal = MAX(max.neighborhoodMean.x, MAX(max.neighborhoodMean.y, max.neighborhoodMean.z));

	Vector3r *diffArray = new Vector3r[width * height];
	for (int i = 0; i < width * height; ++i) {
		diffArray[i] = cpuContext->world[i] - cpuContext->localSurface[i].neighborhoodMean;
	}

	gdk_pixbuf_new_from_data_vec3b(diffArray, width, height, [&min, &max, minVal, maxVal](const Vector3r &v)->cv::Vec3b {

		Vector3r neigh = v;
		neigh.x -= minVal;
		neigh.y -= minVal;
		neigh.z -= minVal;

		neigh = neigh / (maxVal - minVal) * 255.0f;

		return cv::Vec3b(neigh.x, neigh.y, neigh.z);

	}, &neighborhoodMeanPixbuf, &neighborhoodMeanMiniPixbuf);

	delete[] diffArray;

	localSurfaceRow.children.push_back(DataRow(ImageType::LocalSurfaceParametersMeanNeighborDistance, neighborhoodMeanPixbuf, neighborhoodMeanMiniPixbuf));

	// Eigen values
	GdkPixbuf *eigenValuesPixbuf;
	GdkPixbuf *eigenValuesMiniPixbuf;

	maxVal = MAX(max.eigenValues[0], MAX(max.eigenValues[1], max.eigenValues[2]));
	maxVal = logf(maxVal);

	gdk_pixbuf_new_from_data_vec3b(cpuContext->localSurface, width, height, [&maxVal](const LocalSurface &ls)->cv::Vec3b {
		
		Matrix<NARF::real, 3, 1> res = ls.eigenValues;
		res[0] = logf(res[0] + 1.0f);
		res[1] = logf(res[1] + 1.0f);
		res[2] = logf(res[2] + 1.0f);

		res = res / maxVal * 255.0f;
		return cv::Vec3b(res[0], res[1], res[2]);

	}, &eigenValuesPixbuf, &eigenValuesMiniPixbuf);

	localSurfaceRow.children.push_back(DataRow(ImageType::LocalSurfaceParametersEigenValues, eigenValuesPixbuf, eigenValuesMiniPixbuf));

	// Normals no jumps
	GdkPixbuf *normalsNoJumpsPixbuf;
	GdkPixbuf *normalsNoJumpsMiniPixbuf;

	gdk_pixbuf_new_from_data_vec3b(cpuContext->localSurface, width, height, [](const LocalSurface &ls)->cv::Vec3b {
		Vector3r v = (ls.normalNoJumps + Vector3r(1.0f, 1.0f, 1.0f)) / 2.0f * 255.0f;
		return cv::Vec3b(v.x, v.y, v.z);
	}, &normalsNoJumpsPixbuf, &normalsNoJumpsMiniPixbuf);

	localSurfaceRow.children.push_back(DataRow(ImageType::LocalSurfaceParametersNormalNoJumps, normalsNoJumpsPixbuf, normalsNoJumpsMiniPixbuf));

	// Neighborhood mean no jumps
	GdkPixbuf *neighborhoodMeanNoJumpsPixbuf;
	GdkPixbuf *neighborhoodMeanNoJumpsMiniPixbuf;

	Vector3r *diffArrayNoJumps = new Vector3r[width * height];
	for (int i = 0; i < width * height; ++i) {
		diffArrayNoJumps[i] = cpuContext->world[i] - cpuContext->localSurface[i].nighborhoodMeanNoJumps;
	}

	NARF::real minValNoJumps = MIN(min.nighborhoodMeanNoJumps.x, MIN(min.nighborhoodMeanNoJumps.y, min.nighborhoodMeanNoJumps.z));
	NARF::real maxValNoJumps = MAX(max.nighborhoodMeanNoJumps.x, MAX(max.nighborhoodMeanNoJumps.y, max.nighborhoodMeanNoJumps.z));

	gdk_pixbuf_new_from_data_vec3b(diffArrayNoJumps, width, height, [&min, &max, minValNoJumps, maxValNoJumps](const Vector3r &v)->cv::Vec3b {

		Vector3r neigh = v;
		neigh.x -= minValNoJumps;
		neigh.y -= minValNoJumps;
		neigh.z -= minValNoJumps;

		neigh = neigh / (maxValNoJumps - minValNoJumps) * 255.0f;

		return cv::Vec3b(neigh.x, neigh.y, neigh.z);

	}, &neighborhoodMeanNoJumpsPixbuf, &neighborhoodMeanNoJumpsMiniPixbuf);

	delete[] diffArrayNoJumps;

	localSurfaceRow.children.push_back(DataRow(ImageType::LocalSurfaceParametersMeanNeighborDistanceNoJumps, neighborhoodMeanNoJumpsPixbuf, neighborhoodMeanNoJumpsMiniPixbuf));

	// Eigen values no jumps
	GdkPixbuf *eigenValuesNoJumpsPixbuf;
	GdkPixbuf *eigenValuesNoJumpsMiniPixbuf;

	maxVal = MAX(max.eigenValuesNoJumps[0], MAX(max.eigenValuesNoJumps[1], max.eigenValuesNoJumps[2]));
	maxVal = logf(maxVal);

	gdk_pixbuf_new_from_data_vec3b(cpuContext->localSurface, width, height, [&maxVal](const LocalSurface &ls)->cv::Vec3b {

		Matrix<NARF::real, 3, 1> res = ls.eigenValuesNoJumps;
		res[0] = logf(res[0] + 1.0f);
		res[1] = logf(res[1] + 1.0f);
		res[2] = logf(res[2] + 1.0f);

		res = res / maxVal * 255.0f;
		return cv::Vec3b(res[0], res[1], res[2]);

	}, &eigenValuesNoJumpsPixbuf, &eigenValuesNoJumpsMiniPixbuf);

	localSurfaceRow.children.push_back(DataRow(ImageType::LocalSurfaceParametersEigenValuesNoJumps, eigenValuesNoJumpsPixbuf, eigenValuesNoJumpsMiniPixbuf));

	// Max neighbor distance
	GdkPixbuf *maxNeighborDistancePixbuf;
	GdkPixbuf *maxNeighborDistanceMiniPixbuf;

	minVal = sqrtf(min.maxNeighborDistanceSquared);
	maxVal = sqrtf(max.maxNeighborDistanceSquared);

	gdk_pixbuf_new_from_data_vec3b(cpuContext->localSurface, width, height, [minVal, maxVal](const LocalSurface &ls)->cv::Vec3b {
		NARF::real val = sqrtf(ls.maxNeighborDistanceSquared);
		val = (val - minVal) / (maxVal - minVal) * 255.0f;
		return cv::Vec3b(val, val, val);
	}, &maxNeighborDistancePixbuf, &maxNeighborDistanceMiniPixbuf);

	localSurfaceRow.children.push_back(DataRow(ImageType::LocalSurfaceParametersMaxNeighborDistance, maxNeighborDistancePixbuf, maxNeighborDistanceMiniPixbuf));

	appData->document->data[frame].push_back(localSurfaceRow);
	updateList(appData);
}

EXPORT void on_menuitem_narf_borderdirections_activate(GtkMenuItem *menuitem, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	unsigned frame = appData->document->currentFrame;

	cuNARF::cuNARFContext *context = appData->document->narfContexts[frame];

	int width = context->width;
	int height = context->height;

	cuNARF::calculateBorderDirections(*context);
	cudaDeviceSynchronize();

	NARF::NARFContext *cpuContext = appData->document->cpuNarfContexts[frame];
	cpuContext->copyParameters(*context);
	cpuContext->copyBorderDirections(*context);

	GdkPixbuf *borderDirectionsPixbuf;
	GdkPixbuf *borderDirectionsMiniPixbuf;

	gdk_pixbuf_new_from_data_vec3b(cpuContext->borderDirections, width, height, [](const Vector3r &v)->cv::Vec3b {
		Vector3r res = (v + Vector3r(1, 1, 1)) / 2.0f * 255.0f;
		return cv::Vec3b(res.x, res.y, res.z);

	}, &borderDirectionsPixbuf, &borderDirectionsMiniPixbuf);

	appData->document->data[frame].push_back(DataRow(ImageType::BorderDirections, borderDirectionsPixbuf, borderDirectionsMiniPixbuf));
	updateList(appData);
}

EXPORT void on_menuitem_narf_averageborderdirections_activate(GtkMenuItem *menuitem, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	unsigned frame = appData->document->currentFrame;

	GtkDialog *avgBorderDirectionsDialog = load_dialog("ui/avgborderdirectionsdialog.ui", user_data);
	gint result = gtk_dialog_run(avgBorderDirectionsDialog);

	GtkSpinButton *radiusSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(avgBorderDirectionsDialog), "radius_spin"));
	int radius = (int)roundf(gtk_spin_button_get_value(radiusSpin));

	GtkSpinButton *minBorderProbSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(avgBorderDirectionsDialog), "border_prob_spin"));
	gdouble minBorderProb = gtk_spin_button_get_value(minBorderProbSpin);

	gtk_widget_destroy(GTK_WIDGET(avgBorderDirectionsDialog));

	if (result != GTK_RESPONSE_OK) {
		return;
	}

	cuNARF::cuNARFContext *context = appData->document->narfContexts[frame];
	context->pixelRadiusBorderDirections = radius;
	context->minimumBorderProbability = minBorderProb;

	cuNARF::calculateAverageBorderDirections(*context);
	cudaDeviceSynchronize();


	int width = context->width;
	int height = context->height;

	NARF::NARFContext *cpuContext = appData->document->cpuNarfContexts[frame];
	cpuContext->copyParameters(*context);
	cpuContext->copyAvgBorderDirections(*context);

	GdkPixbuf *avgBorderDirectionsPixbuf;
	GdkPixbuf *avgBorderDirectionsMiniPixbuf;

	gdk_pixbuf_new_from_data_vec3b(cpuContext->averageBorderDirections, width, height, [](const Vector3r &v)->cv::Vec3b {
		Vector3r res = (v + Vector3r(1, 1, 1)) / 2.0f * 255.0f;
		return cv::Vec3b(res.x, res.y, res.z);
	}, &avgBorderDirectionsPixbuf, &avgBorderDirectionsMiniPixbuf);

	appData->document->data[frame].push_back(DataRow(ImageType::AvgBorderDirections, avgBorderDirectionsPixbuf, avgBorderDirectionsMiniPixbuf));

	updateList(appData);
}

EXPORT void on_menuitem_narf_surface_changes_activate(GtkMenuItem *menuitem, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	unsigned frame = appData->document->currentFrame;

	cuNARF::cuNARFContext *context = appData->document->narfContexts[frame];

	int width = context->width;
	int height = context->height;

	cuNARF::calculateSurfaceChanges(*context);
	cudaDeviceSynchronize();

	NARF::NARFContext *cpuContext = appData->document->cpuNarfContexts[frame];
	cpuContext->copySurfaceChanges(*context);

	DataRow surfaceChangeRow(ImageType::SurfaceChange);

	// Score
	GdkPixbuf *surfaceChangeScorePixbuf;
	GdkPixbuf *surfaceChangeScoreMiniPixbuf;

	gdk_pixbuf_new_from_data_vec3b(cpuContext->surfaceChangeScore, width, height, [](NARF::real x)->cv::Vec3b {
		x = x * 255.0f;
		return cv::Vec3b(x, x, x);

	}, &surfaceChangeScorePixbuf, &surfaceChangeScoreMiniPixbuf);

	surfaceChangeRow.children.push_back(DataRow(ImageType::SurfaceChangeScore, surfaceChangeScorePixbuf, surfaceChangeScoreMiniPixbuf));

	// Direction
	GdkPixbuf *surfaceChangeDirectionPixbuf;
	GdkPixbuf *surfaceChangeDirectionMiniPixbuf;

	gdk_pixbuf_new_from_data_vec3b(cpuContext->surfaceChangeDirection, width, height, [](const Vector3r &v)->cv::Vec3b {
		Vector3r res = (v + Vector3r(1, 1, 1)) / 2.0f * 255.0f;
		return cv::Vec3b(res.x, res.y, res.z);
	}, &surfaceChangeDirectionPixbuf, &surfaceChangeDirectionMiniPixbuf);

	surfaceChangeRow.children.push_back(DataRow(ImageType::SurfaceChangeDirection, surfaceChangeDirectionPixbuf, surfaceChangeDirectionMiniPixbuf));


	appData->document->data[frame].push_back(surfaceChangeRow);
	updateList(appData);
}

EXPORT void on_menuitem_narf_interestimage_activate(GtkMenuItem *menuitem, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	unsigned frame = appData->document->currentFrame;

	GtkDialog *interestImageDialog = load_dialog("ui/interestimagedialog.ui", user_data);
	gint result = gtk_dialog_run(interestImageDialog);

	GtkSpinButton *searchRadiusSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(interestImageDialog), "search_radius_spin"));
	gdouble searchRadius = gtk_spin_button_get_value(searchRadiusSpin);

	GtkSpinButton *minInterestValueSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(interestImageDialog), "minimal_interest_value_spin"));
	gdouble minInterestValue = gtk_spin_button_get_value(minInterestValueSpin);

	GtkSpinButton *optimalDistanceToSurfaceChangeSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(interestImageDialog), "optimal_distance_to_surface_change_spin"));
	gdouble optimalDistanceToSurfaceChange = gtk_spin_button_get_value(optimalDistanceToSurfaceChangeSpin);

	GtkCheckButton *addPointsOnEdgesCheckbox = GTK_CHECK_BUTTON(gtk_container_get_child(GTK_CONTAINER(interestImageDialog), "add_points_on_edges_checkbox"));
	gboolean addPointsOnEdges = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(addPointsOnEdgesCheckbox));

	gtk_widget_destroy(GTK_WIDGET(interestImageDialog));

	if (result != GTK_RESPONSE_OK) {
		return;
	}

	cuNARF::cuNARFContext *context = appData->document->narfContexts[frame];
	context->searchRadius = searchRadius;
	context->minimumInterestValue = minInterestValue;
	context->optimalDistanceToHighSurfaceChange = optimalDistanceToSurfaceChange;
	context->addPointsOnStraightEdges = addPointsOnEdges;

	cuNARF::calculateInterestImage(*context);
	cudaDeviceSynchronize();

	int width = context->width;
	int height = context->height;

	NARF::NARFContext *cpuContext = appData->document->cpuNarfContexts[frame];
	cpuContext->copyParameters(*context);
	cpuContext->copyInterestImage(*context);

	GdkPixbuf *interestImagePixbuf;
	GdkPixbuf *interestImageMiniPixbuf;

	gdk_pixbuf_new_from_data_vec3b(cpuContext->interestImage, width, height, [](NARF::real x)->cv::Vec3b {
		x = x * 255.0f;
		return cv::Vec3b(x, x, x);
	}, &interestImagePixbuf, &interestImageMiniPixbuf);

	appData->document->data[frame].push_back(DataRow(ImageType::InterestImage, interestImagePixbuf, interestImageMiniPixbuf));
	updateList(appData);
}

EXPORT void on_menuitem_narf_interestpoints_activate(GtkMenuItem *menuitem, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	unsigned frame = appData->document->currentFrame;

	GtkDialog *interestPointsDialog = load_dialog("ui/interestpointsdialog.ui", user_data);
	gint result = gtk_dialog_run(interestPointsDialog);

	GtkSpinButton *minInterestValueSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(interestPointsDialog), "minimum_interest_value_spin"));
	gdouble minInterestValue = gtk_spin_button_get_value(minInterestValueSpin);

	GtkSpinButton *numberOfApproxSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(interestPointsDialog), "no_poly_approx_spin"));
	int numberOfApprox = (int)(roundf(gtk_spin_button_get_value(numberOfApproxSpin)));

	GtkCheckButton *nonMaxSupprCheckBox = GTK_CHECK_BUTTON(gtk_container_get_child(GTK_CONTAINER(interestPointsDialog), "non_max_suppression_checkbox"));
	gboolean doNonMaxSuppr = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(nonMaxSupprCheckBox));

	GtkSpinButton *maxNumberOfInterestPointsSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(interestPointsDialog), "max_interest_points_spin"));
	int maxNumberOfInterestPoints = (int)(roundf(gtk_spin_button_get_value(maxNumberOfInterestPointsSpin)));

	GtkSpinButton *minDistanceSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(interestPointsDialog), "min_distance_spin"));
	gdouble minDistance = gtk_spin_button_get_value(minDistanceSpin);

	gtk_widget_destroy(GTK_WIDGET(interestPointsDialog));

	if (result != GTK_RESPONSE_OK) {
		return;
	}

	cuNARF::cuNARFContext *context = appData->document->narfContexts[frame];
	context->minimumInterestValue = minInterestValue;
	context->numberOfPolynomialApproximations = numberOfApprox;
	context->doNonMaximumSuppression = doNonMaxSuppr;
	context->maxNumberOfInterestPoints = maxNumberOfInterestPoints;
	context->minDistanceBetweenInterestPoints = minDistance;

	cuNARF::calculateInterestPoints(*context);
	cuNARF::sortAndFilterInterestPoints(*context);
	cudaDeviceSynchronize();

	int width = context->width;
	int height = context->height;

	NARF::NARFContext *cpuContext = appData->document->cpuNarfContexts[frame];
	cpuContext->copyParameters(*context);
	cpuContext->copyInterestPoints(*context);

	GdkPixbuf *interestPointsPixbuf;
	GdkPixbuf *interestPointsMiniPixbuf;

	gdk_pixbuf_new_from_data_vec3b(cpuContext->isInterestPoint, width, height, [](bool x)->cv::Vec3b {
		
		if (x) {
			return cv::Vec3b(255, 255, 255);
		}
		else {
			return cv::Vec3b(0, 0, 0);
		}

	}, &interestPointsPixbuf, &interestPointsMiniPixbuf);

	appData->document->data[frame].push_back(DataRow(ImageType::InterestPoints, interestPointsPixbuf, interestPointsMiniPixbuf));
	updateList(appData);
}

EXPORT void on_menuitem_fpfh_calculate_spfh_activate(GtkMenuItem *menuitem, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	unsigned frame = appData->document->currentFrame;

	GtkDialog *calculateSpfhDialog = load_dialog("ui/calculatespfhdialog.ui", user_data);
	gint result = gtk_dialog_run(calculateSpfhDialog);

	GtkSpinButton *normalsRadiusSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(calculateSpfhDialog), "normals_radius_spin"));
	gdouble normalsRadius = gtk_spin_button_get_value(normalsRadiusSpin);

	GtkSpinButton *binsPerFeatureSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(calculateSpfhDialog), "bins_per_feature_spin"));
	int binsPerFeature = (int)(roundf(gtk_spin_button_get_value(binsPerFeatureSpin)));

	gtk_widget_destroy(GTK_WIDGET(calculateSpfhDialog));

	if (result != GTK_RESPONSE_OK) {
		return;
	}

	cuFPFH::cuFPFHContext *context = appData->document->fpfhContexts[frame];
	NARFContext *narf = appData->document->cpuNarfContexts[frame];

	if (!context) {
		context = new cuFPFH::cuFPFHContext(appData->document->recording->getWidth(), appData->document->recording->getHeight(), binsPerFeature, narf->world);
		context->xzFactor = appData->document->recording->getXZFactor();
		context->yzFactor = appData->document->recording->getYZFactor();
		appData->document->fpfhContexts[frame] = context;
	}

	context->normalsRadius = normalsRadius;

	cuFPFH::initialize(*context);
	cuFPFH::calculateNormals2(*context);
	cuFPFH::calculateSPFHs(*context);
	cudaDeviceSynchronize();

	FPFH::FPFHContext *cpuContext = appData->document->cpuFpfhContexts[frame];
	if (!cpuContext) {
		cpuContext = new FPFH::FPFHContext(*context);
		appData->document->cpuFpfhContexts[frame] = cpuContext;
	}

	GdkPixbuf *normalsPixbuf;
	GdkPixbuf *normalsMiniPixbuf;

	gdk_pixbuf_new_from_data_vec3b(cpuContext->normals, context->width, context->height, [](const Vector3r &v)->cv::Vec3b{
		Vector3r res = (v + Vector3r(1, 1, 1)) / 2.0f * 255.0f;
		return cv::Vec3b(res.x, res.y, res.z);
	}, &normalsPixbuf, &normalsMiniPixbuf);

	appData->document->data[frame].push_back(DataRow(ImageType::FPFHNormals, normalsPixbuf, normalsMiniPixbuf));

	cv::Mat depthMapMiniature = appData->document->recording->getFrame(frame)->get_miniature_image();
	GdkPixbuf *depthMapMini = gdk_pixbuf_new_from_cvmat(depthMapMiniature);
	depthMapMiniature.release();

	cv::Mat depthMatImage = appData->document->recording->getFrame(frame)->get_depth_image();
	GdkPixbuf *depthPixbuf = gdk_pixbuf_new_from_cvmat(depthMatImage);
	depthMatImage.release();

	appData->document->data[frame].push_back(DataRow(ImageType::SPFHImage, depthPixbuf, depthMapMini));
	updateList(appData);
}

EXPORT void on_menuitem_fpfh_calculatefpfh_activate(GtkMenuItem *menuitem, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	unsigned frame = appData->document->currentFrame;

	GtkDialog *calculateFpfhDialog = load_dialog("ui/calculatefpfhdialog.ui", user_data);
	gint result = gtk_dialog_run(calculateFpfhDialog);

	GtkSpinButton *fpfhRadiusSpin = GTK_SPIN_BUTTON(gtk_container_get_child(GTK_CONTAINER(calculateFpfhDialog), "fpfh_radius_spin"));
	int fpfhRadius = (int)(roundf(gtk_spin_button_get_value(fpfhRadiusSpin)));

	gtk_widget_destroy(GTK_WIDGET(calculateFpfhDialog));

	if (result != GTK_RESPONSE_OK) {
		return;
	}

	cuFPFH::cuFPFHContext *context = appData->document->fpfhContexts[frame];
	context->radius = fpfhRadius;

	cuFPFH::calculateFPFHs2(*context);
	cudaDeviceSynchronize();

	FPFH::FPFHContext *cpuContext = appData->document->cpuFpfhContexts[frame];
	delete cpuContext;
	cpuContext = new FPFH::FPFHContext(*context);
	appData->document->cpuFpfhContexts[frame] = cpuContext;
	
	cv::Mat depthMapMiniature = appData->document->recording->getFrame(frame)->get_miniature_image();
	GdkPixbuf *depthMapMini = gdk_pixbuf_new_from_cvmat(depthMapMiniature);
	depthMapMiniature.release();

	cv::Mat depthMatImage = appData->document->recording->getFrame(frame)->get_depth_image();
	GdkPixbuf *depthPixbuf = gdk_pixbuf_new_from_cvmat(depthMatImage);
	depthMatImage.release();

	appData->document->data[frame].push_back(DataRow(ImageType::FPFHImage, depthPixbuf, depthMapMini));
	updateList(appData);
}

EXPORT void on_action_open(GtkAction *action, gpointer user_data) {

}

EXPORT void on_action_import(GtkAction *action, gpointer user_data) {
	AppData *appData = (AppData *)user_data;

	GtkWidget *dialog;
	gint res;

	dialog = gtk_file_chooser_dialog_new("Importuj plik",
		appData->mainWindow,
		GTK_FILE_CHOOSER_ACTION_OPEN,
		"Anuluj",
		GTK_RESPONSE_CANCEL,
		"Importuj",
		GTK_RESPONSE_ACCEPT,
		NULL);

	res = gtk_dialog_run(GTK_DIALOG(dialog));
	if (res == GTK_RESPONSE_ACCEPT)
	{
		char *filename;
		GtkFileChooser *chooser = GTK_FILE_CHOOSER(dialog);
		filename = gtk_file_chooser_get_filename(chooser);
		import_file(appData, filename);
		g_free(filename);
	}

	gtk_widget_destroy(dialog);
}

EXPORT void on_action_new(GtkAction *action, gpointer user_data) {
	AppData *appData = (AppData *)user_data;

	appData->set_document(new Document());
}

gboolean should_exit(AppData *appData) {
	return TRUE;
}

EXPORT void on_action_exit(GtkAction *action, gpointer user_data) {
	AppData *appData = (AppData *)user_data;
	
	if (should_exit(appData)) {
		GtkWidget *mainWindow = GTK_WIDGET(appData->mainWindow);
		gtk_widget_destroy(mainWindow);
	}
}

EXPORT gboolean on_main_window_delete_event(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	AppData *appData = (AppData *)user_data;
	return !should_exit(appData);
}

static void activate(GtkApplication *app, gpointer user_data) {
	appData = new AppData();

	appData->application = app;

	GtkBuilder *builder = gtk_builder_new();
	gtk_builder_add_from_file(builder, "ui/narf.ui", NULL);
	gtk_builder_connect_signals(builder, appData);

	GtkWidget *window = GTK_WIDGET(gtk_builder_get_object(builder, "main_window"));
	gtk_application_add_window(app, GTK_WINDOW(window));

	appData->builder = builder;

	// UI
	appData->mainWindow = GTK_WINDOW(window);
	
	// frame scale
	appData->framesScale = GTK_SCALE(gtk_builder_get_object(builder, "frames_scale"));
	gtk_widget_set_sensitive(GTK_WIDGET(appData->framesScale), FALSE);
	gtk_range_set_range(GTK_RANGE(appData->framesScale), 0.0, 1.0);
	gtk_range_set_round_digits(GTK_RANGE(appData->framesScale), 0);
	gtk_range_set_slider_size_fixed(GTK_RANGE(appData->framesScale), TRUE);
	gtk_range_set_min_slider_size(GTK_RANGE(appData->framesScale), 10);
	gtk_range_set_increments(GTK_RANGE(appData->framesScale), 1.0, 1.0);

	//frame spin button
	appData->framesSpinButton = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "frames_spin_button"));
	gtk_widget_set_sensitive(GTK_WIDGET(appData->framesSpinButton), FALSE);
	gtk_spin_button_set_range(appData->framesSpinButton, 0.0, 1.0);
	gtk_spin_button_set_value(appData->framesSpinButton, 0.0);
	gtk_spin_button_set_increments(appData->framesSpinButton, 1.0, 1.0);

	// tree view
	appData->treeView = GTK_TREE_VIEW(gtk_builder_get_object(builder, "treeview_images"));

	// image frame alignment
	appData->imageFrameAlignment = GTK_ALIGNMENT(gtk_builder_get_object(builder, "image_frame_alignment"));

	// preview
	appData->previewWidget = GTK_IMAGE(gtk_builder_get_object(builder, "preview_widget"));

	// list store
	appData->imagesTreeStore = GTK_TREE_STORE(gtk_builder_get_object(builder, "images_treestore"));

	on_action_new(nullptr, appData);

	/*GtkAction *action_exit = GTK_ACTION(gtk_builder_get_object(builder, "action_exit"));
	g_signal_connect_swapped(action_exit, "activate", G_CALLBACK(gtk_widget_destroy), window);*/

	//GtkWidget *grid;
	//GtkWidget *button;

	///* create a new window, and set its title */
	//window = gtk_application_window_new(app);
	//gtk_window_set_title(GTK_WINDOW(window), "Window");
	//gtk_container_set_border_width(GTK_CONTAINER(window), 10);

	///* Here we construct the container that is going pack our buttons */
	//grid = gtk_grid_new();

	///* Pack the container in the window */
	//gtk_container_add(GTK_CONTAINER(window), grid);

	//button = gtk_button_new_with_label("Button 1");
	//g_signal_connect(button, "clicked", G_CALLBACK(print_hello), NULL);

	///* Place the first button in the grid cell (0, 0), and make it fill
	//* just 1 cell horizontally and vertically (ie no spanning)
	//*/
	//gtk_grid_attach(GTK_GRID(grid), button, 0, 0, 1, 1);

	//button = gtk_button_new_with_label("Button 2");
	//g_signal_connect(button, "clicked", G_CALLBACK(print_hello), NULL);

	///* Place the second button in the grid cell (1, 0), and make it fill
	//* just 1 cell horizontally and vertically (ie no spanning)
	//*/
	//gtk_grid_attach(GTK_GRID(grid), button, 1, 0, 1, 1);

	//button = gtk_button_new_with_label("Quit");
	//g_signal_connect_swapped(button, "clicked", G_CALLBACK(gtk_widget_destroy), window);

	///* Place the Quit button in the grid cell (0, 1), and make it
	//* span 2 columns.
	//*/
	//gtk_grid_attach(GTK_GRID(grid), button, 0, 1, 2, 1);

	/* Now that we are done packing our widgets, we show them all
	* in one go, by calling gtk_widget_show_all() on the window.
	* This call recursively calls gtk_widget_show() on all widgets
	* that are contained in the window, directly or indirectly.
	*/
	gtk_widget_show_all(window);

}

int main(int argc, char **argv) {
	GtkApplication *app;
	int status;

	setlocale(LC_ALL, "pl_PL.UTF-8");
	cudaThreadSetLimit(cudaLimitMallocHeapSize, 512*1024*1024);

	app = gtk_application_new("pl.edu.pw.elka.doledzki.narf", G_APPLICATION_FLAGS_NONE);
	g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
	status = g_application_run(G_APPLICATION(app), argc, argv);
	g_object_unref(app);

	return status;
}

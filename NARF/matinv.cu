#include "matinv.h"
#include <math.h>
#include <assert.h>
#include <stdio.h>

namespace cuGSL
{

	CUDA_DEVICE gsl_permutation *
		gsl_permutation_alloc(const size_t n)
	{
		gsl_permutation * p;
		assert(n > 0);

		p = (gsl_permutation *)malloc(sizeof(gsl_permutation));
		assert(p != 0);

		p->data = (size_t *)malloc(n * sizeof(size_t));

		if (p->data == 0)
		{
			free(p);         /* exception in constructor, avoid memory leak */
		}

		p->size = n;

		return p;
	}

	CUDA_DEVICE void
		gsl_permutation_init(gsl_permutation * p)
	{
		const size_t n = p->size;
		size_t i;

		/* initialize permutation to identity */

		for (i = 0; i < n; i++)
		{
			p->data[i] = i;
		}
	}

	CUDA_DEVICE void
		gsl_permutation_free(gsl_permutation * p)
	{
		if (!p)
			return;

		free(p->data);
		free(p);
	}

	CUDA_DEVICE int gsl_permutation_swap(gsl_permutation * p, const size_t i, const size_t j)
	{
		const size_t size = p->size;

		if (i >= size)
		{
			return GSL_EINVAL;
		}

		if (j >= size)
		{
			return GSL_EINVAL;
		}

		if (i != j)
		{
			size_t tmp = p->data[i];
			p->data[i] = p->data[j];
			p->data[j] = tmp;
		}

		return GSL_SUCCESS;
	}

	CUDA_DEVICE int gsl_permute(const size_t * p, double * data, const size_t stride, const size_t n)
	{
		size_t i, k, pk;

		for (i = 0; i < n; i++)
		{
			k = p[i];

			while (k > i)
				k = p[k];

			if (k < i)
				continue;

			/* Now have k == i, i.e the least in its cycle */

			pk = p[k];

			if (pk == i)
				continue;

			/* shuffle the elements of the cycle */

			{
				unsigned int a;

				double t[1];

				for (a = 0; a < 1; a++)
					t[a] = data[i*stride*1 + a];

				while (pk != i)
				{
					for (a = 0; a < 1; a++)
					{
						double r1 = data[pk*stride*1 + a];
						data[k*stride*1 + a] = r1;
					}
					k = pk;
					pk = p[k];
				};

				for (a = 0; a < 1; a++)
					data[k*stride*1 + a] = t[a];
			}
		}

		return GSL_SUCCESS;
	}

	CUDA_DEVICE int gsl_permute_vector(const gsl_permutation * p, gsl_vector * v)
	{
		if (v->size != p->size)
		{
			return GSL_EBADLEN;
			//GSL_ERROR("vector and permutation must be the same length", GSL_EBADLEN);
		}

		gsl_permute(p->data, v->data, v->stride, v->size);

		return GSL_SUCCESS;
	}

	CUDA_DEVICE gsl_block * gsl_block_alloc(const size_t n)
	{
		gsl_block * b;
		assert(n > 0);

		b = (gsl_block*)malloc(sizeof(gsl_block));
		assert(b != 0);

		b->data = (double *)malloc(n * sizeof(double));
		memset(b->data, 0, n * sizeof(double));

		if (b->data == 0)
		{
			free(b);         /* exception in constructor, avoid memory leak */
		}

		b->size = n;

		return b;
	}

	CUDA_DEVICE void gsl_block_free(gsl_block * b)
	{
		if (!b)
			return;

		free(b->data);
		free(b);
	}


	CUDA_DEVICE gsl_matrix * gsl_matrix_alloc(const size_t n1, const size_t n2)
	{
		gsl_block * block;
		gsl_matrix * m;

		assert(n1 > 0);
		assert(n2 > 0);

		m = (gsl_matrix *)malloc(sizeof(gsl_matrix));
		assert(m != 0);

		block = gsl_block_alloc(n1 * n2);
		assert(block != 0);

		m->data = block->data;
		m->size1 = n1;
		m->size2 = n2;
		m->tda = n2;
		m->block = block;
		m->owner = 1;

		return m;
	}

	CUDA_DEVICE void gsl_matrix_free(gsl_matrix * m)
	{
		if (!m)
			return;

		if (m->owner)
		{
			gsl_block_free(m->block);
		}

		free(m);
	}

	CUDA_DEVICE void gsl_matrix_set_identity(gsl_matrix * m)
	{
		size_t i, j;
		double * const data = m->data;
		const size_t p = m->size1;
		const size_t q = m->size2;
		const size_t tda = m->tda;

		const double zero = 0.0;
		const double one = 1.0;

		for (i = 0; i < p; i++)
		{
			for (j = 0; j < q; j++)
			{
				*(double *)(data + (i * tda + j)) = ((i == j) ? one : zero);
			}
		}
	}

	CUDA_DEVICE __inline double gsl_matrix_get(const gsl_matrix * m, const size_t i, const size_t j)
	{
		return m->data[i * m->tda + j];
	}

	CUDA_DEVICE __inline void gsl_matrix_set(gsl_matrix * m, const size_t i, const size_t j, const double x)
	{
		m->data[i * m->tda + j] = x;
	}

	CUDA_DEVICE int gsl_matrix_swap_rows(gsl_matrix * m, const size_t i, const size_t j)
	{
		const size_t size1 = m->size1;
		const size_t size2 = m->size2;

		if (i >= size1)
		{
			return GSL_EINVAL;
		}

		if (j >= size1)
		{
			return GSL_EINVAL;
		}

		if (i != j)
		{
			double *row1 = m->data + i * m->tda;
			double *row2 = m->data + j * m->tda;

			size_t k;

			for (k = 0; k < size2; k++)
			{
				double tmp = row1[k];
				row1[k] = row2[k];
				row2[k] = tmp;
			}
		}

		return GSL_SUCCESS;
	}

	CUDA_DEVICE _gsl_vector_view gsl_matrix_column(gsl_matrix * m, const size_t j)
	{
		_gsl_vector_view view = NULL_VECTOR_VIEW;

		if (j >= m->size2)
		{
			return view;
		}

  {
	  gsl_vector v = NULL_VECTOR;

	  v.data = m->data + j;
	  v.size = m->size1;
	  v.stride = m->tda;
	  v.block = m->block;
	  v.owner = 0;

	  view.vector = v;
	  return view;
  }
	}

	CUDA_DEVICE int gsl_linalg_LU_decomp(gsl_matrix * A, gsl_permutation * p, int *signum)
	{
		assert(A->size1 == A->size2);
		assert(p->size == A->size1);

		const size_t N = A->size1;
		size_t i, j, k;

		*signum = 1;
		gsl_permutation_init(p);

		for (j = 0; j < N - 1; j++)
		{
			/* Find maximum in the j-th column */

			double ajj, max = fabs(gsl_matrix_get(A, j, j));
			size_t i_pivot = j;

			for (i = j + 1; i < N; i++)
			{
				double aij = fabs(gsl_matrix_get(A, i, j));

				if (aij > max)
				{
					max = aij;
					i_pivot = i;
				}
			}

			if (i_pivot != j)
			{
				gsl_matrix_swap_rows(A, j, i_pivot);
				gsl_permutation_swap(p, j, i_pivot);
				*signum = -(*signum);
			}

			ajj = gsl_matrix_get(A, j, j);

			if (ajj != 0.0)
			{
				for (i = j + 1; i < N; i++)
				{
					double aij = gsl_matrix_get(A, i, j) / ajj;
					gsl_matrix_set(A, i, j, aij);

					for (k = j + 1; k < N; k++)
					{
						double aik = gsl_matrix_get(A, i, k);
						double ajk = gsl_matrix_get(A, j, k);
						gsl_matrix_set(A, i, k, aik - aij * ajk);
					}
				}
			}
		}

		return GSL_SUCCESS;
	}

	CUDA_DEVICE int singular(const gsl_matrix * LU)
	{
		size_t i, n = LU->size1;

		for (i = 0; i < n; i++)
		{
			double u = gsl_matrix_get(LU, i, i);
			if (u == 0) return 1;
		}

		return 0;
	}

	/*
	* Enumerated and derived types
	*/
#define CBLAS_INDEX size_t  /* this may vary between platforms */

	enum CBLAS_ORDER { CblasRowMajor = 101, CblasColMajor = 102 };
	enum CBLAS_TRANSPOSE { CblasNoTrans = 111, CblasTrans = 112, CblasConjTrans = 113 };
	enum CBLAS_UPLO { CblasUpper = 121, CblasLower = 122 };
	enum CBLAS_DIAG { CblasNonUnit = 131, CblasUnit = 132 };
	enum CBLAS_SIDE { CblasLeft = 141, CblasRight = 142 };


	typedef  CBLAS_INDEX  CBLAS_INDEX_t;
	typedef  enum CBLAS_ORDER       CBLAS_ORDER_t;
	typedef  enum CBLAS_TRANSPOSE   CBLAS_TRANSPOSE_t;
	typedef  enum CBLAS_UPLO        CBLAS_UPLO_t;
	typedef  enum CBLAS_DIAG        CBLAS_DIAG_t;
	typedef  enum CBLAS_SIDE        CBLAS_SIDE_t;

#define INDEX int
#define OFFSET(N, incX) ((incX) > 0 ?  0 : ((N) - 1) * (-(incX)))
#define BASE double
#define INT(X) ((int)(X))

	CUDA_DEVICE void
		cblas_dtrsv(const enum CBLAS_ORDER order, const enum CBLAS_UPLO Uplo, const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag, const int N, const double *A, const int lda, double *X, const int incX)
	{
		const int nonunit = (Diag == CblasNonUnit);
		INDEX ix, jx;
		INDEX i, j;
		const int Trans = (TransA != CblasConjTrans) ? TransA : CblasTrans;

		if (N == 0)
			return;

		/* form  x := inv( A )*x */

		if ((order == CblasRowMajor && Trans == CblasNoTrans && Uplo == CblasUpper)
			|| (order == CblasColMajor && Trans == CblasTrans && Uplo == CblasLower)) {
			/* backsubstitution */
			ix = OFFSET(N, incX) + incX * (N - 1);
			if (nonunit) {
				X[ix] = X[ix] / A[lda * (N - 1) + (N - 1)];
			}
			ix -= incX;
			for (i = N - 1; i > 0 && i--;) {
				BASE tmp = X[ix];
				jx = ix + incX;
				for (j = i + 1; j < N; j++) {
					const BASE Aij = A[lda * i + j];
					tmp -= Aij * X[jx];
					jx += incX;
				}
				if (nonunit) {
					X[ix] = tmp / A[lda * i + i];
				}
				else {
					X[ix] = tmp;
				}
				ix -= incX;
			}
		}
		else if ((order == CblasRowMajor && Trans == CblasNoTrans && Uplo == CblasLower)
			|| (order == CblasColMajor && Trans == CblasTrans && Uplo == CblasUpper)) {

			/* forward substitution */
			ix = OFFSET(N, incX);
			if (nonunit) {
				X[ix] = X[ix] / A[lda * 0 + 0];
			}
			ix += incX;
			for (i = 1; i < N; i++) {
				BASE tmp = X[ix];
				jx = OFFSET(N, incX);
				for (j = 0; j < i; j++) {
					const BASE Aij = A[lda * i + j];
					tmp -= Aij * X[jx];
					jx += incX;
				}
				if (nonunit) {
					X[ix] = tmp / A[lda * i + i];
				}
				else {
					X[ix] = tmp;
				}
				ix += incX;
			}
		}
		else if ((order == CblasRowMajor && Trans == CblasTrans && Uplo == CblasUpper)
			|| (order == CblasColMajor && Trans == CblasNoTrans && Uplo == CblasLower)) {

			/* form  x := inv( A' )*x */

			/* forward substitution */
			ix = OFFSET(N, incX);
			if (nonunit) {
				X[ix] = X[ix] / A[lda * 0 + 0];
			}
			ix += incX;
			for (i = 1; i < N; i++) {
				BASE tmp = X[ix];
				jx = OFFSET(N, incX);
				for (j = 0; j < i; j++) {
					const BASE Aji = A[lda * j + i];
					tmp -= Aji * X[jx];
					jx += incX;
				}
				if (nonunit) {
					X[ix] = tmp / A[lda * i + i];
				}
				else {
					X[ix] = tmp;
				}
				ix += incX;
			}
		}
		else if ((order == CblasRowMajor && Trans == CblasTrans && Uplo == CblasLower)
			|| (order == CblasColMajor && Trans == CblasNoTrans && Uplo == CblasUpper)) {

			/* backsubstitution */
			ix = OFFSET(N, incX) + (N - 1) * incX;
			if (nonunit) {
				X[ix] = X[ix] / A[lda * (N - 1) + (N - 1)];
			}
			ix -= incX;
			for (i = N - 1; i > 0 && i--;) {
				BASE tmp = X[ix];
				jx = ix + incX;
				for (j = i + 1; j < N; j++) {
					const BASE Aji = A[lda * j + i];
					tmp -= Aji * X[jx];
					jx += incX;
				}
				if (nonunit) {
					X[ix] = tmp / A[lda * i + i];
				}
				else {
					X[ix] = tmp;
				}
				ix -= incX;
			}
		}
		else {
			printf("unrecognized operation");
		}

	}


	CUDA_DEVICE int gsl_blas_dtrsv(CBLAS_UPLO_t Uplo, CBLAS_TRANSPOSE_t TransA, CBLAS_DIAG_t Diag, const gsl_matrix * A, gsl_vector * X)
	{
		const size_t M = A->size1;
		const size_t N = A->size2;

		if (M != N)
		{
			return GSL_ENOTSQR;
			//GSL_ERROR("matrix must be square", GSL_ENOTSQR);
		}
		else if (N != X->size)
		{
			return GSL_EBADLEN;
			// GSL_ERROR("invalid length", GSL_EBADLEN);
		}

		cblas_dtrsv(CblasRowMajor, Uplo, TransA, Diag, INT(N), A->data,
			INT(A->tda), X->data, INT(X->stride));
		return GSL_SUCCESS;
	}

	CUDA_DEVICE int gsl_linalg_LU_svx(const gsl_matrix * LU, const gsl_permutation * p, gsl_vector * x)
	{
		if (LU->size1 != LU->size2)
		{
			return GSL_ENOTSQR;
			// GSL_ERROR("LU matrix must be square", GSL_ENOTSQR);
		}
		else if (LU->size1 != p->size)
		{
			return GSL_EBADLEN;
			// GSL_ERROR("permutation length must match matrix size", GSL_EBADLEN);
		}
		else if (LU->size1 != x->size)
		{
			return GSL_EBADLEN;
			// GSL_ERROR("matrix size must match solution/rhs size", GSL_EBADLEN);
		}
		else if (singular(LU))
		{
			return GSL_EDOM;
			// GSL_ERROR("matrix is singular", GSL_EDOM);
		}
		else
		{
			/* Apply permutation to RHS */

			gsl_permute_vector(p, x);

			/* Solve for c using forward-substitution, L c = P b */

			gsl_blas_dtrsv(CblasLower, CblasNoTrans, CblasUnit, LU, x);

			/* Perform back-substitution, U x = c */

			gsl_blas_dtrsv(CblasUpper, CblasNoTrans, CblasNonUnit, LU, x);

			return GSL_SUCCESS;
		}
	}

	CUDA_DEVICE int gsl_linalg_LU_invert(const gsl_matrix * LU, const gsl_permutation * p, gsl_matrix * inverse)
	{
		size_t i, n = LU->size1;

		int status = GSL_SUCCESS;

		if (singular(LU))
		{
			return GSL_EDOM;
		}

		gsl_matrix_set_identity(inverse);

		for (i = 0; i < n; i++)
		{
			gsl_vector_view c = gsl_matrix_column(inverse, i);
			int status_i = gsl_linalg_LU_svx(LU, p, &(c.vector));

			if (status_i)
				status = status_i;
		}

		return status;
	}

	CUDA_DEVICE int matInvProc(gsl_matrix *m, gsl_matrix *inverse, gsl_permutation *perm)
	{
		int s;
		int gslRes = 0;

		gslRes = gsl_linalg_LU_decomp(m, perm, &s);
		if (gslRes != 0)
			return GSL_FAILURE;			

		gslRes = gsl_linalg_LU_invert(m, perm, inverse);
		if (gslRes != 0)
			return GSL_FAILURE;

		return GSL_SUCCESS;
	}

	CUDA_DEVICE int matInv(const double *A, int n, double *invA)
	{
		gsl_matrix *m = gsl_matrix_alloc(n, n);
		gsl_matrix *inverse = gsl_matrix_alloc(n, n);
		gsl_permutation *perm = gsl_permutation_alloc(n);

		for (int i = 0; i < n * n; ++i)
			m->data[i] = A[i];

		int result = matInvProc(m, inverse, perm);
		if (result != GSL_SUCCESS) {
			gsl_permutation_free(perm);
			gsl_matrix_free(inverse);
			gsl_matrix_free(m);
			return result;
		}

		for (int i = 0; i < n * n; ++i)
			invA[i] = inverse->data[i];

		gsl_permutation_free(perm);
		gsl_matrix_free(inverse);
		gsl_matrix_free(m);

		return GSL_SUCCESS;
	}

	CUDA_DEVICE int matInv(const float *A, int n, float *invA)
	{
		gsl_matrix *m = gsl_matrix_alloc(n, n);
		gsl_matrix *inverse = gsl_matrix_alloc(n, n);
		gsl_permutation *perm = gsl_permutation_alloc(n);

		for (int i = 0; i < n * n; ++i)
			m->data[i] = A[i];

		int result = matInvProc(m, inverse, perm);
		if (result != GSL_SUCCESS) {
			gsl_permutation_free(perm);
			gsl_matrix_free(inverse);
			gsl_matrix_free(m);
			return result;
		}

		for (int i = 0; i < n * n; ++i)
			invA[i] = inverse->data[i];

		gsl_permutation_free(perm);
		gsl_matrix_free(inverse);
		gsl_matrix_free(m);

		return GSL_SUCCESS;
	}

}

#pragma once
#include "Utilities.h"

#define NULL_VECTOR_VIEW {{0, 0, 0, 0, 0}}
#define NULL_VECTOR {0, 0, 0, 0, 0}

namespace cuGSL
{
	enum {
		GSL_SUCCESS = 0,
		GSL_FAILURE = -1,
		GSL_CONTINUE = -2,  /* iteration has not converged */
		GSL_EDOM = 1,   /* input domain error, e.g sqrt(-1) */
		GSL_ERANGE = 2,   /* output range error, e.g. exp(1e100) */
		GSL_EFAULT = 3,   /* invalid pointer */
		GSL_EINVAL = 4,   /* invalid argument supplied by user */
		GSL_EFAILED = 5,   /* generic failure */
		GSL_EFACTOR = 6,   /* factorization failed */
		GSL_ESANITY = 7,   /* sanity check failed - shouldn't happen */
		GSL_ENOMEM = 8,   /* malloc failed */
		GSL_EBADFUNC = 9,   /* problem with user-supplied function */
		GSL_ERUNAWAY = 10,  /* iterative process is out of control */
		GSL_EMAXITER = 11,  /* exceeded max number of iterations */
		GSL_EZERODIV = 12,  /* tried to divide by zero */
		GSL_EBADTOL = 13,  /* user specified an invalid tolerance */
		GSL_ETOL = 14,  /* failed to reach the specified tolerance */
		GSL_EUNDRFLW = 15,  /* underflow */
		GSL_EOVRFLW = 16,  /* overflow  */
		GSL_ELOSS = 17,  /* loss of accuracy */
		GSL_EROUND = 18,  /* failed because of roundoff error */
		GSL_EBADLEN = 19,  /* matrix, vector lengths are not conformant */
		GSL_ENOTSQR = 20,  /* matrix not square */
		GSL_ESING = 21,  /* apparent singularity detected */
		GSL_EDIVERGE = 22,  /* integral or series is divergent */
		GSL_EUNSUP = 23,  /* requested feature is not supported by the hardware */
		GSL_EUNIMPL = 24,  /* requested feature not (yet) implemented */
		GSL_ECACHE = 25,  /* cache limit exceeded */
		GSL_ETABLE = 26,  /* table limit exceeded */
		GSL_ENOPROG = 27,  /* iteration is not making progress towards solution */
		GSL_ENOPROGJ = 28,  /* jacobian evaluations are not improving the solution */
		GSL_ETOLF = 29,  /* cannot reach the specified tolerance in F */
		GSL_ETOLX = 30,  /* cannot reach the specified tolerance in X */
		GSL_ETOLG = 31,  /* cannot reach the specified tolerance in gradient */
		GSL_EOF = 32   /* end of file */
	};

	struct gsl_block_struct
	{
		size_t size;
		double *data;
	};

	typedef struct gsl_block_struct gsl_block;

	typedef struct
	{
		size_t size;
		size_t stride;
		double *data;
		gsl_block *block;
		int owner;
	}
	gsl_vector;

	typedef struct
	{
		gsl_vector vector;
	} _gsl_vector_view;

	typedef _gsl_vector_view gsl_vector_view;

	typedef struct
	{
		size_t size1;
		size_t size2;
		size_t tda;
		double * data;
		gsl_block * block;
		int owner;
	} gsl_matrix;

	typedef struct
	{
		gsl_matrix matrix;
	} _gsl_matrix_view;

	typedef _gsl_matrix_view gsl_matrix_view;

	struct gsl_permutation_struct
	{
		size_t size;
		size_t *data;
	};

	typedef struct gsl_permutation_struct gsl_permutation;

	CUDA_DEVICE gsl_permutation *gsl_permutation_alloc(const size_t n);
	CUDA_DEVICE void gsl_permutation_init(gsl_permutation * p);
	CUDA_DEVICE void gsl_permutation_free(gsl_permutation * p);
	CUDA_DEVICE int gsl_permutation_swap(gsl_permutation * p, const size_t i, const size_t j);

	CUDA_DEVICE gsl_block * gsl_block_alloc(const size_t n);
	CUDA_DEVICE void gsl_block_free(gsl_block * b);

	CUDA_DEVICE gsl_matrix * gsl_matrix_alloc(const size_t n1, const size_t n2);
	CUDA_DEVICE void gsl_matrix_free(gsl_matrix * m);
	CUDA_DEVICE void gsl_matrix_set_identity(gsl_matrix * m);

	CUDA_DEVICE __inline double gsl_matrix_get(const gsl_matrix * m, const size_t i, const size_t j);
	CUDA_DEVICE __inline void gsl_matrix_set(gsl_matrix * m, const size_t i, const size_t j, const double x);
	CUDA_DEVICE int gsl_matrix_swap_rows(gsl_matrix * m, const size_t i, const size_t j);
	CUDA_DEVICE _gsl_vector_view gsl_matrix_column(gsl_matrix * m, const size_t j);

	CUDA_DEVICE int gsl_linalg_LU_decomp(gsl_matrix * A, gsl_permutation * p, int *signum);
	CUDA_DEVICE int gsl_linalg_LU_invert(const gsl_matrix * LU, const gsl_permutation * p, gsl_matrix * inverse);

	CUDA_DEVICE int matInv(const double *A, int n, double *invA);
	CUDA_DEVICE int matInv(const float *A, int n, float *invA);
}

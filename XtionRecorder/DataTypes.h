#pragma once
#include <limits>
#include <cmath>

typedef float real;

template<typename T>
struct Point3
{
	Point3(T _x = 0, T _y = 0, T _z = 0) : x(_x), y(_y), z(_z) {}

	T x;
	T y;
	T z;
};

template<typename T>
struct Rotation3
{
	T alpha;
	T beta;
	T gamma;
};

template<typename T>
struct Vector2
{
	T x;
	T y;
};

typedef Point3<float> Point3f;
typedef Rotation3<float> Rotation3f;
typedef Vector2<float> Vector2f;
typedef Vector2<int> Size2i;

typedef Point3<real> Point3r;


template<typename T>
bool isValid(const Point3<T> &p) {
	return !std::isnan<T>(p.x) && !std::isnan<T>(p.y) && !std::isnan<T>(p.z);
}

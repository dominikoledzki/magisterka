#include <OpenNI.h>
#include <iostream>
#include <memory>
#include "DataTypes.h"
#include <cstdlib>
#include <xmmintrin.h>
#include <fstream>

using namespace openni;
using namespace std;

struct WorldConversionCache
{
	float xzFactor;
	float yzFactor;
	float coeffX;
	float coeffY;
	int resolutionX;
	int resolutionY;
	int halfResX;
	int halfResY;
} m_worldConvertCache;

void refreshWorldConversionCache(const VideoStream &stream);
void convertDepthToWorld(VideoFrameRef frame, Point3f **out);
VideoMode chooseVideoMode(Device &device);

struct Header {
	int sizeX;
	int sizeY;
	float factorXZ;
	float factorYZ;
};

int main(int argc, char **argv)
{

	if (argc < 2) {
		cout << "Wrong number of arguments! Expected: 1.";
		system("PAUSE");
		return -1;
	}

	int numberOfFrames = atoi(argv[1]);
	int skipFrames = 0;
	if (argc == 3) {
		skipFrames = atoi(argv[2]);
	}

	OpenNI::initialize();

	Device device;
	Status status = device.open(ANY_DEVICE);
	if (status != Status::STATUS_OK)
	{
		std::cerr << "Cannot open device!" << std::endl;
		return -2;
	}

	VideoMode videoMode = chooseVideoMode(device);
	VideoStream videoStream;
	status = videoStream.create(device, SensorType::SENSOR_DEPTH);
	if (status != Status::STATUS_OK)
	{
		cerr << "Cannot create video stream" << endl;
	}
	else
	{
		videoStream.setVideoMode(videoMode);
		cout << "FPS: " << videoMode.getFps() << endl
			<< "Pixel format " << videoMode.getPixelFormat() << endl
			<< "Resolution: " << videoMode.getResolutionX() << " x " << videoMode.getResolutionY() << endl;
	}

	videoStream.setMirroringEnabled(false);
	videoStream.start();

	refreshWorldConversionCache(videoStream);

	int sizeX = videoMode.getResolutionX();
	int sizeY = videoMode.getResolutionY();

	float hFov = videoStream.getHorizontalFieldOfView();
	float calculatedFocalLengthX = sizeX / (2.0f * tan(hFov / 2.0f));


	FILE *file = fopen("record2.dat", "wb");
	fwrite(&sizeX, sizeof(sizeX), 1, file);
	fwrite(&sizeY, sizeof(sizeY), 1, file);
	fwrite(&m_worldConvertCache.xzFactor, sizeof(m_worldConvertCache.xzFactor), 1, file);
	fwrite(&m_worldConvertCache.yzFactor, sizeof(m_worldConvertCache.yzFactor), 1, file);


	for (int i = 0; i < skipFrames; ++i) {
		VideoFrameRef frame;
		videoStream.readFrame(&frame);
		frame.release();
	}

	for (int i = 0; i < numberOfFrames; ++i) {
		VideoFrameRef frame;
		videoStream.readFrame(&frame);

		cout << "Frame: " << frame.getFrameIndex() << endl;

		DepthPixel *data = (DepthPixel*)frame.getData();
		fwrite(data, 1, frame.getDataSize(), file);
		
		frame.release();
	}

	fclose(file);

	videoStream.stop();
	videoStream.destroy();
	device.close();
	OpenNI::shutdown();

	system("PAUSE");

	return 0;
}

VideoMode chooseVideoMode(Device &device)
{
	auto sensorInfo = device.getSensorInfo(SensorType::SENSOR_DEPTH);
	auto &supportedVideoModes = sensorInfo->getSupportedVideoModes();

	for (int i = 0; i < supportedVideoModes.getSize(); ++i) {
		auto mode = supportedVideoModes[i];
		if (mode.getFps() >= 30 && mode.getResolutionX() >= 640 && mode.getResolutionY() >= 480 && mode.getPixelFormat() == PixelFormat::PIXEL_FORMAT_DEPTH_1_MM)
			return mode;
	}

	return supportedVideoModes[0];
}

void convertDepthToWorld(VideoFrameRef frame, Point3f **out)
{
	int sizeY = frame.getHeight();
	int sizeX = frame.getWidth();
	__m256 resolutionX = _mm256_set1_ps(m_worldConvertCache.resolutionX);

	DepthPixel *data = (DepthPixel *)frame.getData();
	for (int y = 0; y < sizeY; ++y)
	{
		float depthY = float(y);
		float normalizedY = 0.5f - depthY / m_worldConvertCache.resolutionY;

		for (int x = 0; x < sizeX; x += 8)
		{
			Point3f *worldPoint = (*out) + (y*sizeX + x);
			__m256 pixel = _mm256_setr_ps(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]);
			__m256 depthX = _mm256_setr_ps(x, x + 1, x + 2, x + 3, x + 4, x + 5, x + 6, x + 7);
			__m256 normalizedX = _mm256_sub_ps(_mm256_div_ps(depthX, resolutionX), _mm256_set1_ps(0.5f));

			for (int i = 0; i < 8; ++i) {
				worldPoint[i].x = normalizedX.m256_f32[i];
				worldPoint[i].y = normalizedY;
				worldPoint[i].z = data[i];
			}

			data += 8;
		}
	}
}

OniStatus convertDepthToWorldCoordinates(float depthX, float depthY, float depthZ, float* pWorldX, float* pWorldY, float* pWorldZ)
{
	float normalizedX = depthX / m_worldConvertCache.resolutionX - .5f;
	float normalizedY = .5f - depthY / m_worldConvertCache.resolutionY;

	*pWorldX = normalizedX * depthZ * m_worldConvertCache.xzFactor;
	*pWorldY = normalizedY * depthZ * m_worldConvertCache.yzFactor;
	*pWorldZ = depthZ;
	return ONI_STATUS_OK;
}

void refreshWorldConversionCache(const VideoStream &stream)
{
	OniVideoMode videoMode;
	int size = sizeof(videoMode);
	stream.getProperty(ONI_STREAM_PROPERTY_VIDEO_MODE, &videoMode, &size);

	size = sizeof(float);
	float horizontalFov;
	float verticalFov;
	stream.getProperty(ONI_STREAM_PROPERTY_HORIZONTAL_FOV, &horizontalFov, &size);
	stream.getProperty(ONI_STREAM_PROPERTY_VERTICAL_FOV, &verticalFov, &size);

	m_worldConvertCache.xzFactor = tan(horizontalFov / 2) * 2;
	m_worldConvertCache.yzFactor = tan(verticalFov / 2) * 2;
	m_worldConvertCache.resolutionX = videoMode.resolutionX;
	m_worldConvertCache.resolutionY = videoMode.resolutionY;
	m_worldConvertCache.halfResX = m_worldConvertCache.resolutionX / 2;
	m_worldConvertCache.halfResY = m_worldConvertCache.resolutionY / 2;
	m_worldConvertCache.coeffX = m_worldConvertCache.resolutionX / m_worldConvertCache.xzFactor;
	m_worldConvertCache.coeffY = m_worldConvertCache.resolutionY / m_worldConvertCache.yzFactor;
}


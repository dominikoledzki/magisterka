Algorytmy opisywane w pracy magisterskiej zaimplementowane są w projekcie, na płycie dołączonej do pracy.
To jest dodatkowa płyta, Na której znajduje się program z interfejsem graficznym, w którym można przyjrzeć się kolejnym krokom wykonywania algorytmów i wpływu parametrów na otrzymywane wyniki.
Dane z kolejnych kroków są od siebie zależne, proszę uruchamiać kroki zgodnie z kolejnością w menu. Algorytm FPFH wymaga obliczenia pierwszego kroku z algorytmu NARF (typowa odległość sąsiadów).
Na płycie znajdują się również biblioteki potrzebne do uruchomienia lub skompilowania projektu.
W katalogu NARF znajduje się projekt Visual Studio, a w katalogu bin zbudowany program i przykładowa scena.